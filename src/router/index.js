import Vue from 'vue';
import VueRouter from 'vue-router';
import store from '../store'; // your vuex store

const ifNotAuthenticated = (to, from, next) => {
  if (!store.getters['auth/isAuthenticated']) {
    next();
    return;
  }
  next('/');
};

const ifAuthenticated = (to, from, next) => {
  if (store.getters['auth/isAuthenticated']) {
    next();
    return;
  }
  next('/auth/login');
};

Vue.use(VueRouter);

const routes = [
  {
    path: '*',
    component: () =>
      import(/* webpackChunkName: "not-found" */ '../views/NotFound.vue'),
    meta: {
      rule: 'isPublic',
      layout: 'no-sidebar'
    }
  },
  {
    path: '/',
    name: 'EntryPoint',
    component: () =>
      import(/* webpackChunkName: "entry-point" */ '../views/Index.vue'),
    meta: {
      rule: 'isGeneral',
      layout: 'no-sidebar'
    },
    beforeEnter: ifAuthenticated
  },

  // Authentication
  {
    path: '/auth',
    component: () =>
      import(/* webpackChunkName: "master" */ '../views/Wrapper.vue'),
    meta: {
      rule: 'isPublic',
      layout: 'no-sidebar'
    },
    children: [
      {
        path: 'login',
        name: 'Login',
        component: () =>
          import(
            /* webpackChunkName: "login" */ '../views/Authentication/Login.vue'
          ),
        meta: {
          rule: 'isPublic',
          layout: 'no-sidebar'
        },
        beforeEnter: ifNotAuthenticated
      },
      {
        path: 'forgot-password',
        name: 'ForgotPassword',
        component: () =>
          import(
            /* webpackChunkName: "forgot-password" */ '../views/Authentication/ForgotPassword.vue'
          ),
        meta: {
          rule: 'isPublic',
          layout: 'no-sidebar'
        },
        beforeEnter: ifNotAuthenticated
      },
      {
        path: 'reset-password/:ticket',
        name: 'ResetPassword',
        component: () =>
          import(
            /* webpackChunkName: "reset-password" */ '../views/Authentication/ResetPassword.vue'
          ),
        meta: {
          rule: 'isPublic',
          layout: 'no-sidebar'
        },
        beforeEnter: ifNotAuthenticated
      },
      {
        path: 'change-password/:ticket',
        name: 'ChangePassword',
        component: () =>
          import(
            /* webpackChunkName: "change-password" */ '../views/Authentication/ResetPassword.vue'
          ),
        meta: {
          rule: 'isGeneral',
          layout: 'no-sidebar'
        },
        beforeEnter: ifAuthenticated
      },
      {
        path: 'otp',
        name: 'Otp',
        component: () =>
          import(
            /* webpackChunkName: "otp" */ '../views/Authentication/Otp.vue'
          ),
        meta: {
          rule: 'isGeneral',
          layout: 'no-sidebar'
        },
        beforeEnter: ifNotAuthenticated
      }
    ]
  },
  // End of Authentication

  // User Login
  {
    path: '/user-login',
    name: 'UserLogin',
    component: () =>
      import(
        /* webpackChunkName: "user-login" */ '../views/UserLogin/Index.vue'
      ),
    meta: {
      rule: 'isAdmin',
      breadCrumb: 'menu.user_login',
      layout: 'default'
    }
  },

  // Master
  {
    path: '/master',
    component: () =>
      import(/* webpackChunkName: "wrapper" */ '../views/Wrapper.vue'),
    meta: {
      breadCrumb: 'menu.master_data'
    },
    children: [
      /* ========== Start Management User Internal ========== */
      {
        path: 'user-internal',
        component: () =>
          import(/* webpackChunkName: "wrapper" */ '../views/Wrapper.vue'),
        meta: {
          rule: 'isAdmin',
          breadCrumb: 'menu.manage_user_internal'
        },
        children: [
          {
            path: '',
            name: 'MasterUserInternal',
            component: () =>
              import(
                /* webpackChunkName: "master-user-internal" */ '../views/Master/UserInternal/Index.vue'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.manage_user_internal',
              layout: 'default'
            },
            beforeEnter: ifAuthenticated
          },
          {
            path: 'create',
            name: 'CreateUserInternal',
            component: () =>
              import(
                /* webpackChunkName: "master-user-internal" */ '../views/Master/UserInternal/CreateEdit.vue'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.add_user_internal',
              layout: 'default'
            },
            beforeEnter: ifAuthenticated
          },
          {
            path: 'detail/:id',
            name: 'DetailUserInternal',
            component: () =>
              import(
                /* webpackChunkName: "master-user-internal" */ '../views/Master/UserInternal/CreateEdit.vue'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.view_user_internal',
              layout: 'default',
              isDetail: true
            },
            beforeEnter: ifAuthenticated
          },
          {
            path: 'edit/:id',
            name: 'UpdateUserInternal',
            component: () =>
              import(
                /* webpackChunkName: "master-user-internal" */ '../views/Master/UserInternal/CreateEdit.vue'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.update_user_internal',
              layout: 'default'
            },
            beforeEnter: ifAuthenticated
          }
        ]
      },
      /* ========== End Management User Internal ========== */

      /* ========== Start Manajemen Assessor ========== */
      {
        path: 'assessor',
        component: () =>
          import(/* webpackChunkName: "wrapper" */ '../views/Wrapper.vue'),
        meta: {
          rule: 'isAdmin',
          breadCrumb: 'menu.manage_assessor'
        },
        children: [
          {
            path: '',
            name: 'MasterAssessor',
            component: () =>
              import(
                /* webpackChunkName: "master-assessor" */ '../views/Master/Assessor/Index.vue'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.manage_assessor',
              layout: 'default'
            },
            beforeEnter: ifAuthenticated
          },
          {
            path: 'create',
            name: 'CreateAssessor',
            component: () =>
              import(
                /* webpackChunkName: "master-assessor" */ '../views/Master/Assessor/CreateEdit.vue'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.add_assessor',
              layout: 'default'
            },
            beforeEnter: ifAuthenticated
          },
          {
            path: 'detail/:id',
            name: 'DetailAssessor',
            component: () =>
              import(
                /* webpackChunkName: "master-assessor" */ '../views/Master/Assessor/CreateEdit.vue'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.view_assessor',
              layout: 'default',
              isDetail: true
            },
            beforeEnter: ifAuthenticated
          },
          {
            path: 'edit/:id',
            name: 'UpdateAssessor',
            component: () =>
              import(
                /* webpackChunkName: "master-assessor" */ '../views/Master/Assessor/CreateEdit.vue'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.update_assessor',
              layout: 'default'
            },
            beforeEnter: ifAuthenticated
          }
        ]
      },

      /* ========== End Management Assessor ========== */

      /* ========== Start Management Peserta ========== */
      {
        path: 'peserta',
        component: () =>
          import(/* webpackChunkName: "wrapper" */ '../views/Wrapper.vue'),
        meta: {
          rule: 'isAdmin',
          breadCrumb: 'menu.manage_participant'
        },
        children: [
          {
            path: '',
            name: 'MasterPeserta',
            component: () =>
              import(
                /* webpackChunkName: "master-peserta" */ '../views/Master/Peserta/Index.vue'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.manage_participant',
              layout: 'default'
            },
            beforeEnter: ifAuthenticated
          },
          {
            path: 'create',
            name: 'CreatePeserta',
            component: () =>
              import(
                /* webpackChunkName: "master-peserta" */ '../views/Master/Peserta/CreateEdit.vue'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.add_participant',
              layout: 'default'
            },
            beforeEnter: ifAuthenticated
          },
          {
            path: 'detail/:id',
            name: 'DetailPeserta',
            component: () =>
              import(
                /* webpackChunkName: "master-peserta" */ '../views/Master/Peserta/CreateEdit.vue'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.view_participant',
              layout: 'default',
              isDetail: true
            },
            beforeEnter: ifAuthenticated
          },
          {
            path: 'edit/:id',
            name: 'UpdatePeserta',
            component: () =>
              import(
                /* webpackChunkName: "master-peserta" */ '../views/Master/Peserta/CreateEdit.vue'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.update_participant',
              layout: 'default'
            },
            beforeEnter: ifAuthenticated
          }
        ]
      },
      /* ========== End Management Peserta ========== */

      /* ========== Start Management Perusahaan ========== */
      {
        path: 'perusahaan',
        component: () =>
          import(/* webpackChunkName: "wrapper" */ '../views/Wrapper.vue'),
        meta: {
          rule: 'isAdmin',
          breadCrumb: 'menu.manage_company'
        },
        children: [
          {
            path: '',
            name: 'MasterPerusahaan',
            component: () =>
              import(
                /* webpackChunkName: "master-perusahaan" */ '../views/Master/Perusahaan/Index.vue'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.manage_company',
              layout: 'default'
            },
            beforeEnter: ifAuthenticated
          },
          {
            path: 'create',
            name: 'CreatePerusahaan',
            component: () =>
              import(
                /* webpackChunkName: "master-perusahaan" */ '../views/Master/Perusahaan/CreateEdit.vue'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.add_company',
              layout: 'default'
            },
            beforeEnter: ifAuthenticated
          },
          {
            path: 'detail/:id',
            component: () =>
              import(/* webpackChunkName: "wrapper" */ '../views/Wrapper.vue'),
            beforeEnter: ifAuthenticated,
            meta: {
              breadCrumb: 'menu.view_company'
            },
            children: [
              {
                path: '',
                name: 'DetailPerusahaan',
                component: () =>
                  import(
                    /* webpackChunkName: "master-perusahaan" */ '../views/Master/Perusahaan/CreateEdit.vue'
                  ),
                meta: {
                  rule: 'isAdmin',
                  layout: 'default',
                  breadCrumb: 'menu.view_company',
                  isDetail: true
                }
              },
              {
                path: ':jobId/kamus-kompetensi',
                name: 'KamusKompetensi',
                component: () =>
                  import(
                    /* webpackChunkName: "master-perusahaan" */ '../views/Master/Perusahaan/CompetencyDictionary.vue'
                  ),
                meta: {
                  rule: 'isAdmin',
                  breadCrumb: 'menu.add_competency_dictionary',
                  layout: 'default'
                },
                beforeEnter: ifAuthenticated
              },
              {
                path: ':jobId/detail/kamus-kompetensi',
                name: 'DetailKamusKompetensi',
                component: () =>
                  import(
                    /* webpackChunkName: "master-perusahaan" */ '../views/Master/Perusahaan/CompetencyDictionary.vue'
                  ),
                meta: {
                  rule: 'isAdmin',
                  breadCrumb: 'menu.view_competency_dictionary',
                  layout: 'default',
                  isDetail: true
                },
                beforeEnter: ifAuthenticated
              }
            ]
          },
          {
            path: ':id/update',
            name: 'UpdatePerusahaan',
            component: () =>
              import(
                /* webpackChunkName: "master-perusahaan" */ '../views/Master/Perusahaan/CreateEdit.vue'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.update_company',
              layout: 'default'
            },
            beforeEnter: ifAuthenticated
          }
        ]
      }
      /* ========== Start Management Perusahaan ========== */
    ]
  },
  // End Master

  // Materi
  {
    path: '/materi',
    component: () =>
      import(/* webpackChunkName: "wrapper" */ '../views/Wrapper.vue'),
    meta: {
      rule: 'isAdmin',
      breadCrumb: 'menu.materi'
    },
    children: [
      /* --------- Main Assessment Center ----------------- */

      /* --- simulated interactions */
      {
        path: 'sub-ordinate-interaction',
        component: () =>
          import(/* webpackChunkName: "wrapper" */ '../views/Wrapper.vue'),
        meta: {
          rule: 'isAdmin',
          breadCrumb: 'menu.manage_assessment_center'
        },
        children: [
          {
            path: '',
            name: 'ManageSubOrdinateInteraction',
            component: () =>
              import(
                /* webpackChunkName: "sub-ordinate-interaction" */ '../views/Materi/AssessmentCenter/Index'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.manage_sub_ordinate_interaction',
              layout: 'default',
              type: 'SOI'
            },
            beforeEnter: ifAuthenticated
          },

          {
            path: 'create',
            name: 'CreateSubOrdinateInteraction',
            component: () =>
              import(
                /* webpackChunkName: "sub-ordinate-interaction" */ '../views/Materi/AssessmentCenter/Interaction/SubOrdinateInteraction'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.add_sub_ordinate_interaction',
              layout: 'default',
              type: 'SOI'
            },
            beforeEnter: ifAuthenticated
          },

          {
            path: ':id/update/:version',
            name: 'UpdateSubOrdinateInteraction',
            component: () =>
              import(
                /* webpackChunkName: "sub-ordinate-interaction" */ '../views/Materi/AssessmentCenter/Interaction/SubOrdinateInteraction'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.update_sub_ordinate_interaction',
              layout: 'default',
              type: 'SOI'
            },
            beforeEnter: ifAuthenticated
          },

          {
            path: ':id/detail/:version',
            name: 'DetailSubOrdinateInteraction',
            component: () =>
              import(
                /* webpackChunkName: "sub-ordinate-interaction" */ '../views/Materi/AssessmentCenter/Interaction/SubOrdinateInteraction'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.view_sub_ordinate_interaction',
              layout: 'default',
              type: 'SOI',
              disabled: true
            },
            beforeEnter: ifAuthenticated
          },

          {
            path: ':id/approve/:version',
            name: 'ApproveSubOrdinateInteraction',
            component: () =>
              import(
                /* webpackChunkName: "sub-ordinate-interaction" */ '../views/Materi/AssessmentCenter/Interaction/SubOrdinateInteraction'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.approve_sub_ordinate_interaction',
              layout: 'default',
              type: 'SOI',
              disabled: true,
              approve: true
            },
            beforeEnter: ifAuthenticated
          }
        ]
      },

      {
        path: 'peer-interaction',
        component: () =>
          import(/* webpackChunkName: "wrapper" */ '../views/Wrapper.vue'),
        meta: {
          rule: 'isAdmin',
          breadCrumb: 'menu.manage_assessment_center'
        },
        children: [
          {
            path: '',
            name: 'ManagePeerInteraction',
            component: () =>
              import(
                /* webpackChunkName: "peer-interaction" */ '../views/Materi/AssessmentCenter/Index'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.manage_peer_interaction',
              layout: 'default',
              type: 'PI'
            },
            beforeEnter: ifAuthenticated
          },

          {
            path: 'create',
            name: 'CreatePeerInteraction',
            component: () =>
              import(
                /* webpackChunkName: "peer-interaction" */ '../views/Materi/AssessmentCenter/Interaction/PeerInteraction.vue'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.add_peer_interaction',
              layout: 'default',
              type: 'PI'
            },
            beforeEnter: ifAuthenticated
          },

          {
            path: ':id/update/:version',
            name: 'UpdatePeerInteraction',
            component: () =>
              import(
                /* webpackChunkName: "peer-interaction" */ '../views/Materi/AssessmentCenter/Interaction/PeerInteraction.vue'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.update_peer_interaction',
              layout: 'default',
              type: 'PI'
            },
            beforeEnter: ifAuthenticated
          },

          {
            path: ':id/detail/:version',
            name: 'DetailPeerInteraction',
            component: () =>
              import(
                /* webpackChunkName: "peer-interaction" */ '../views/Materi/AssessmentCenter/Interaction/PeerInteraction.vue'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.view_peer_interaction',
              layout: 'default',
              type: 'PI',
              disabled: true
            },
            beforeEnter: ifAuthenticated
          },

          {
            path: ':id/approve/:version',
            name: 'ApprovePeerInteraction',
            component: () =>
              import(
                /* webpackChunkName: "peer-interaction" */ '../views/Materi/AssessmentCenter/Interaction/PeerInteraction'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.approve_peer_interaction',
              layout: 'default',
              type: 'PI',
              disabled: true,
              approve: true
            },
            beforeEnter: ifAuthenticated
          },

          {
            path: 'flp',
            name: 'CreateFlpPeerInteraction',
            component: () =>
              import(
                /* webpackChunkName: "peer-interaction" */ '../views/Materi/AssessmentCenter/Flp'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.flp_peer_interaction',
              layout: 'default',
              type: 'PI'
            },
            beforeEnter: ifAuthenticated
          },

          {
            path: ':id/flp/:version',
            name: 'ApproveFlpPeerInteraction',
            component: () =>
              import(
                /* webpackChunkName: "peer-interaction" */ '../views/Materi/AssessmentCenter/Flp'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.flp_peer_interaction',
              layout: 'default',
              type: 'PI'
            },
            beforeEnter: ifAuthenticated
          },

          {
            path: ':id/flp/:version',
            name: 'UpdateFlpPeerInteraction',
            component: () =>
              import(
                /* webpackChunkName: "peer-interaction" */ '../views/Materi/AssessmentCenter/Flp'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.flp_peer_interaction',
              layout: 'default',
              type: 'PI'
            },
            beforeEnter: ifAuthenticated
          }
        ]
      },

      {
        path: 'customer-interaction',
        component: () =>
          import(/* webpackChunkName: "wrapper" */ '../views/Wrapper.vue'),
        meta: {
          rule: 'isAdmin',
          breadCrumb: 'menu.manage_assessment_center'
        },
        children: [
          {
            path: '',
            name: 'ManageCustomerInteraction',
            component: () =>
              import(
                /* webpackChunkName: "customer-interaction" */ '../views/Materi/AssessmentCenter/Index'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.manage_customer_interaction',
              layout: 'default',
              type: 'CI'
            },
            beforeEnter: ifAuthenticated
          },

          {
            path: 'create',
            name: 'CreateCustomerInteraction',
            component: () =>
              import(
                /* webpackChunkName: "customer-interaction" */ '../views/Materi/AssessmentCenter/Interaction/CustomerInteraction.vue'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.add_customer_interaction',
              layout: 'default',
              type: 'CI'
            },
            beforeEnter: ifAuthenticated
          },

          {
            path: ':id/update/:version',
            name: 'UpdateCustomerInteraction',
            component: () =>
              import(
                /* webpackChunkName: "customer-interaction" */ '../views/Materi/AssessmentCenter/Interaction/CustomerInteraction.vue'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.update_customer_interaction',
              layout: 'default',
              type: 'CI'
            },
            beforeEnter: ifAuthenticated
          },

          {
            path: ':id/detail/:version',
            name: 'DetailCustomerInteraction',
            component: () =>
              import(
                /* webpackChunkName: "customer-interaction" */ '../views/Materi/AssessmentCenter/Interaction/CustomerInteraction.vue'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.view_customer_interaction',
              layout: 'default',
              type: 'CI',
              disabled: true
            },
            beforeEnter: ifAuthenticated
          },

          {
            path: ':id/approve/:version',
            name: 'ApproveCustomerInteraction',
            component: () =>
              import(
                /* webpackChunkName: "customer-interaction" */ '../views/Materi/AssessmentCenter/Interaction/CustomerInteraction'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.approve_customer_interaction',
              layout: 'default',
              type: 'CI',
              disabled: true,
              approve: true
            },
            beforeEnter: ifAuthenticated
          }
        ]
      },

      /* --- simulated individual */

      {
        path: 'inbasket-intray',
        component: () =>
          import(/* webpackChunkName: "wrapper" */ '../views/Wrapper.vue'),
        meta: {
          rule: 'isAdmin',
          breadCrumb: 'menu.manage_assessment_center'
        },
        children: [
          {
            path: '',
            name: 'ManageInBasket',
            component: () =>
              import(
                /* webpackChunkName: "inbasket-intray" */ '../views/Materi/AssessmentCenter/Index'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.manage_in_basket_in_tray',
              layout: 'default',
              type: 'INB'
            },
            beforeEnter: ifAuthenticated
          },

          {
            path: 'create',
            name: 'CreateInBasket',
            component: () =>
              import(
                /* webpackChunkName: "inbasket-intray" */ '../views/Materi/AssessmentCenter/Individual/InBasket'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.add_in_basket_in_tray',
              layout: 'default',
              type: 'INB'
            },
            beforeEnter: ifAuthenticated
          },

          {
            path: ':id/update/:version',
            name: 'UpdateInBasket',
            component: () =>
              import(
                /* webpackChunkName: "inbasket-intray" */ '../views/Materi/AssessmentCenter/Individual/InBasket'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.update_in_basket_in_tray',
              layout: 'default',
              type: 'INB'
            },
            beforeEnter: ifAuthenticated
          },

          {
            path: ':id/detail/:version',
            name: 'DetailInBasket',
            component: () =>
              import(
                /* webpackChunkName: "inbasket-intray" */ '../views/Materi/AssessmentCenter/Individual/InBasket'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.view_in_basket_in_tray',
              layout: 'default',
              type: 'INB',
              disabled: true
            },
            beforeEnter: ifAuthenticated
          },

          {
            path: ':id/approve/:version',
            name: 'ApproveInBasket',
            component: () =>
              import(
                /* webpackChunkName: "inbasket-intray" */ '../views/Materi/AssessmentCenter/Individual/InBasket'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.approve_in_basket_in_tray',
              layout: 'default',
              type: 'INB',
              disabled: true,
              approve: true
            },
            beforeEnter: ifAuthenticated
          },

          {
            path: 'flp',
            name: 'CreateFlpInBasket',
            component: () =>
              import(
                /* webpackChunkName: "inbasket-intray" */ '../views/Materi/AssessmentCenter/Flp'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.flp_in_basket_in_tray',
              layout: 'default',
              type: 'INB'
            },
            beforeEnter: ifAuthenticated
          },

          {
            path: ':id/flp/:version',
            name: 'ApproveFlpInBasket',
            component: () =>
              import(
                /* webpackChunkName: "inbasket-intray" */ '../views/Materi/AssessmentCenter/Flp'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.flp_in_basket_in_tray',
              layout: 'default',
              type: 'INB'
            },
            beforeEnter: ifAuthenticated
          },

          {
            path: ':id/flp/:version',
            name: 'UpdateFlpInBasket',
            component: () =>
              import(
                /* webpackChunkName: "inbasket-intray" */ '../views/Materi/AssessmentCenter/Flp'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.flp_in_basket_in_tray',
              layout: 'default',
              type: 'INB'
            },
            beforeEnter: ifAuthenticated
          }
        ]
      },

      {
        path: 'problem-analysis',
        component: () =>
          import(/* webpackChunkName: "wrapper" */ '../views/Wrapper.vue'),
        meta: {
          rule: 'isAdmin',
          breadCrumb: 'menu.manage_assessment_center'
        },
        children: [
          {
            path: '',
            name: 'ManageProblemAnalysis',
            component: () =>
              import(
                /* webpackChunkName: "problem-analysis" */ '../views/Materi/AssessmentCenter/Index'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.manage_problem_analysis',
              layout: 'default',
              type: 'PA'
            },
            beforeEnter: ifAuthenticated
          },

          {
            path: 'create',
            name: 'CreateProblemAnalysis',
            component: () =>
              import(
                /* webpackChunkName: "problem-analysis" */ '../views/Materi/AssessmentCenter/Individual/ProblemAnalysis'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.add_problem_analysis',
              layout: 'default',
              type: 'PA'
            },
            beforeEnter: ifAuthenticated
          },

          {
            path: ':id/update/:version',
            name: 'UpdateProblemAnalysis',
            component: () =>
              import(
                /* webpackChunkName: "problem-analysis" */ '../views/Materi/AssessmentCenter/Individual/ProblemAnalysis'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.update_problem_analysis',
              layout: 'default',
              type: 'PA'
            },
            beforeEnter: ifAuthenticated
          },

          {
            path: ':id/detail/:version',
            name: 'DetailProblemAnalysis',
            component: () =>
              import(
                /* webpackChunkName: "problem-analysis" */ '../views/Materi/AssessmentCenter/Individual/ProblemAnalysis'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.view_problem_analysis',
              layout: 'default',
              type: 'PA',
              disabled: true
            },
            beforeEnter: ifAuthenticated
          },

          {
            path: ':id/approve/:version',
            name: 'ApproveProblemAnalysis',
            component: () =>
              import(
                /* webpackChunkName: "problem-analysis" */ '../views/Materi/AssessmentCenter/Individual/ProblemAnalysis'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.approve_problem_analysis',
              layout: 'default',
              type: 'PA',
              disabled: true,
              approve: true
            },
            beforeEnter: ifAuthenticated
          }
        ]
      },

      {
        path: 'improvement-enhancement',
        component: () =>
          import(/* webpackChunkName: "wrapper" */ '../views/Wrapper.vue'),
        meta: {
          rule: 'isAdmin',
          breadCrumb: 'menu.manage_assessment_center'
        },
        children: [
          {
            path: '',
            name: 'ManageImprovementEnchantment',
            component: () =>
              import(
                /* webpackChunkName: "improvement-enhancement" */ '../views/Materi/AssessmentCenter/Index'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.manage_improvement_enchantment',
              layout: 'default',
              type: 'IE'
            },
            beforeEnter: ifAuthenticated
          },

          {
            path: 'create',
            name: 'CreateImprovementEnchantment',
            component: () =>
              import(
                /* webpackChunkName: "improvement-enhancement" */ '../views/Materi/AssessmentCenter/Individual/ImprovementEnchanment'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.add_improvement_enchantment',
              layout: 'default',
              type: 'IE'
            },
            beforeEnter: ifAuthenticated
          },

          {
            path: ':id/update/:version',
            name: 'UpdateImprovementEnchantment',
            component: () =>
              import(
                /* webpackChunkName: "improvement-enhancement" */ '../views/Materi/AssessmentCenter/Individual/ImprovementEnchanment'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.update_improvement_enchantment',
              layout: 'default',
              type: 'IE'
            },
            beforeEnter: ifAuthenticated
          },

          {
            path: ':id/detail/:version',
            name: 'DetailImprovementEnchantment',
            component: () =>
              import(
                /* webpackChunkName: "improvement-enhancement" */ '../views/Materi/AssessmentCenter/Individual/ImprovementEnchanment'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.view_improvement_enchantment',
              layout: 'default',
              type: 'IE',
              disabled: true
            },
            beforeEnter: ifAuthenticated
          },

          {
            path: ':id/approve/:version',
            name: 'ApproveImprovementEnchantment',
            component: () =>
              import(
                /* webpackChunkName: "improvement-enhancement" */ '../views/Materi/AssessmentCenter/Individual/ImprovementEnchanment'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.approve_improvement_enchantment',
              layout: 'default',
              type: 'IE',
              disabled: true,
              approve: true
            },
            beforeEnter: ifAuthenticated
          }
        ]
      },

      {
        path: 'scheduling',
        component: () =>
          import(/* webpackChunkName: "wrapper" */ '../views/Wrapper.vue'),
        meta: {
          rule: 'isAdmin',
          breadCrumb: 'menu.manage_assessment_center'
        },
        children: [
          {
            path: '',
            name: 'ManageScheduling',
            component: () =>
              import(
                /* webpackChunkName: "scheduling" */ '../views/Materi/AssessmentCenter/Index'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.manage_scheduling',
              layout: 'default',
              type: 'SCH'
            },
            beforeEnter: ifAuthenticated
          },

          {
            path: 'create',
            name: 'CreateScheduling',
            component: () =>
              import(
                /* webpackChunkName: "scheduling" */ '../views/Materi/AssessmentCenter/Individual/Scheduling'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.add_scheduling',
              layout: 'default',
              type: 'SCH'
            },
            beforeEnter: ifAuthenticated
          },

          {
            path: ':id/update/:version',
            name: 'UpdateScheduling',
            component: () =>
              import(
                /* webpackChunkName: "scheduling" */ '../views/Materi/AssessmentCenter/Individual/Scheduling'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.update_scheduling',
              layout: 'default',
              type: 'SCH'
            },
            beforeEnter: ifAuthenticated
          },

          {
            path: ':id/detail/:version',
            name: 'DetailScheduling',
            component: () =>
              import(
                /* webpackChunkName: "scheduling" */ '../views/Materi/AssessmentCenter/Individual/Scheduling'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.view_scheduling',
              layout: 'default',
              type: 'SCH',
              disabled: true
            },
            beforeEnter: ifAuthenticated
          },

          {
            path: ':id/approve/:version',
            name: 'ApproveScheduling',
            component: () =>
              import(
                /* webpackChunkName: "scheduling" */ '../views/Materi/AssessmentCenter/Individual/Scheduling'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.approve_scheduling',
              layout: 'default',
              type: 'SCH',
              disabled: true,
              approve: true
            },
            beforeEnter: ifAuthenticated
          }
        ]
      },

      {
        path: 'proposal-writing',
        component: () =>
          import(/* webpackChunkName: "wrapper" */ '../views/Wrapper.vue'),
        meta: {
          rule: 'isAdmin',
          breadCrumb: 'menu.manage_assessment_center'
        },
        children: [
          {
            path: '',
            name: 'ManageProposalWriting',
            component: () =>
              import(
                /* webpackChunkName: "proposal-writing" */ '../views/Materi/AssessmentCenter/Index'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.manage_proposal_writing',
              layout: 'default',
              type: 'PW'
            },
            beforeEnter: ifAuthenticated
          },

          {
            path: 'create',
            name: 'CreateProposalWriting',
            component: () =>
              import(
                /* webpackChunkName: "proposal-writing" */ '../views/Materi/AssessmentCenter/Individual/ProposalWriting'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.add_proposal_writing',
              layout: 'default',
              type: 'PW'
            },
            beforeEnter: ifAuthenticated
          },

          {
            path: ':id/update/:version',
            name: 'UpdateProposalWriting',
            component: () =>
              import(
                /* webpackChunkName: "proposal-writing" */ '../views/Materi/AssessmentCenter/Individual/ProposalWriting'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.update_proposal_writing',
              layout: 'default',
              type: 'PW'
            },
            beforeEnter: ifAuthenticated
          },

          {
            path: ':id/detail/:version',
            name: 'DetailProposalWriting',
            component: () =>
              import(
                /* webpackChunkName: "proposal-writing" */ '../views/Materi/AssessmentCenter/Individual/ProposalWriting'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.detail_proposal_writing',
              layout: 'default',
              type: 'PW',
              disabled: true
            },
            beforeEnter: ifAuthenticated
          },

          {
            path: ':id/approve/:version',
            name: 'ApproveProposalWriting',
            component: () =>
              import(
                /* webpackChunkName: "proposal-writing" */ '../views/Materi/AssessmentCenter/Individual/ProposalWriting'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.approve_proposal_writing',
              layout: 'default',
              type: 'PW',
              disabled: true,
              approve: true
            },
            beforeEnter: ifAuthenticated
          }
        ]
      },

      /* --- simulated groups */

      {
        path: 'leaderless-group-discussion',
        component: () =>
          import(/* webpackChunkName: "wrapper" */ '../views/Wrapper.vue'),
        meta: {
          rule: 'isAdmin',
          breadCrumb: 'menu.manage_assessment_center'
        },
        children: [
          {
            path: '',
            name: 'ManageLeaderlessDiscussion',
            component: () =>
              import(
                /* webpackChunkName: "leaderless-group-discussion" */ '../views/Materi/AssessmentCenter/Index'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.manage_leaderless_discussion',
              layout: 'default',
              type: 'LGD'
            },
            beforeEnter: ifAuthenticated
          },

          {
            path: 'create',
            name: 'CreateLeaderlessDiscussion',
            component: () =>
              import(
                /* webpackChunkName: "leaderless-group-discussion" */ '../views/Materi/AssessmentCenter/Groups/LeaderlessDiscussion'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.add_leaderless_discussion',
              layout: 'default',
              type: 'LGD'
            },
            beforeEnter: ifAuthenticated
          },

          {
            path: ':id/update/:version',
            name: 'UpdateLeaderlessDiscussion',
            component: () =>
              import(
                /* webpackChunkName: "leaderless-group-discussion" */ '../views/Materi/AssessmentCenter/Groups/LeaderlessDiscussion'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.update_leaderless_discussion',
              layout: 'default',
              type: 'LGD'
            },
            beforeEnter: ifAuthenticated
          },

          {
            path: ':id/detail/:version',
            name: 'DetailLeaderlessDiscussion',
            component: () =>
              import(
                /* webpackChunkName: "leaderless-group-discussion" */ '../views/Materi/AssessmentCenter/Groups/LeaderlessDiscussion'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.view_leaderless_discussion',
              layout: 'default',
              type: 'LGD',
              disabled: true
            },
            beforeEnter: ifAuthenticated
          },

          {
            path: ':id/approve/:version',
            name: 'ApproveLeaderlessDiscussion',
            component: () =>
              import(
                /* webpackChunkName: "leaderless-group-discussion" */ '../views/Materi/AssessmentCenter/Groups/LeaderlessDiscussion'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.approve_leaderless_discussion',
              layout: 'default',
              type: 'LGD',
              disabled: true,
              approve: true
            },
            beforeEnter: ifAuthenticated
          },

          {
            path: 'flp',
            name: 'CreateFlpLeaderlessDiscussion',
            component: () =>
              import(
                /* webpackChunkName: "leaderless-group-discussion" */ '../views/Materi/AssessmentCenter/Flp'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.flp_leaderless_discussion',
              layout: 'default',
              type: 'LGD'
            },
            beforeEnter: ifAuthenticated
          },

          {
            path: ':id/flp/:version',
            name: 'ApproveFlpLeaderlessDiscussion',
            component: () =>
              import(
                /* webpackChunkName: "leaderless-group-discussion" */ '../views/Materi/AssessmentCenter/Flp'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.flp_leaderless_discussion',
              layout: 'default',
              type: 'LGD'
            },
            beforeEnter: ifAuthenticated
          },

          {
            path: ':id/flp/:version',
            name: 'UpdateFlpLeaderlessDiscussion',
            component: () =>
              import(
                /* webpackChunkName: "leaderless-group-discussion" */ '../views/Materi/AssessmentCenter/Flp'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.flp_leaderless_discussion',
              layout: 'default',
              type: 'LGD'
            },
            beforeEnter: ifAuthenticated
          }
        ]
      },

      /* --------- Main Assessment Center ----------------- */

      /* --------- Main Psychological Assessment --------- */

      {
        path: 'psychological-assessment',
        component: () =>
          import(/* webpackChunkName: "wrapper" */ '../views/Wrapper.vue'),
        meta: {
          rule: 'isAdmin'
        },
        children: [
          /* ==================== index psychological assessment ====================== */
          {
            path: '',
            name: 'ManagePsychologicalAssessment',
            component: () =>
              import(
                /* webpackChunkName: "psychological-assessment" */ '../views/Materi/PsychologicalAssessment/Index.vue'
              ),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.manage_psychological_assessment',
              layout: 'default'
            },
            beforeEnter: ifAuthenticated
          },
          /* ==================== index psychological assessment ====================== */

          {
            path: '',
            component: () =>
              import(/* webpackChunkName: "wrapper" */ '../views/Wrapper.vue'),
            meta: {
              rule: 'isAdmin',
              breadCrumb: 'menu.manage_psychological_assessment'
            },
            children: [
              /* =============== List Of Question : DISC, Papi Kostik, Value, LS, Belbin, IST, 16 PF  =============== */

              // DISC
              {
                path: 'disc/create',
                name: 'CreateDisc',
                component: () =>
                  import(
                    /* webpackChunkName: "psychological-assessment" */ '../views/Materi/PsychologicalAssessment/Question/Disc.vue'
                  ),
                meta: {
                  rule: 'isAdmin',
                  breadCrumb: 'menu.add_disc',
                  layout: 'default'
                },
                beforeEnter: ifAuthenticated
              },
              {
                path: 'disc/:id/update/:version',
                name: 'UpdateDisc',
                component: () =>
                  import(
                    /* webpackChunkName: "psychological-assessment" */ '../views/Materi/PsychologicalAssessment/Question/Disc.vue'
                  ),
                meta: {
                  rule: 'isAdmin',
                  breadCrumb: 'menu.update_disc',
                  layout: 'default'
                },
                beforeEnter: ifAuthenticated
              },
              {
                path: 'disc/:id/view/:version',
                name: 'DetailDisc',
                component: () =>
                  import(
                    /* webpackChunkName: "psychological-assessment" */ '../views/Materi/PsychologicalAssessment/Question/Disc.vue'
                  ),
                meta: {
                  rule: 'isAdmin',
                  breadCrumb: 'menu.view_disc',
                  layout: 'default',
                  disabled: true
                },
                beforeEnter: ifAuthenticated
              },
              {
                path: 'disc/:id/approve/:version',
                name: 'ApproveDisc',
                component: () =>
                  import(
                    /* webpackChunkName: "psychological-assessment" */ '../views/Materi/PsychologicalAssessment/Question/Disc.vue'
                  ),
                meta: {
                  rule: 'isAdmin',
                  breadCrumb: 'menu.view_disc',
                  layout: 'default',
                  disabled: true,
                  approve: true
                },
                beforeEnter: ifAuthenticated
              },

              // Papi Kostik
              {
                path: 'papikostik/create',
                name: 'CreatePapikostik',
                component: () =>
                  import(
                    /* webpackChunkName: "psychological-assessment" */ '../views/Materi/PsychologicalAssessment/Question/Papikostik.vue'
                  ),
                meta: {
                  rule: 'isAdmin',
                  breadCrumb: 'menu.add_papikostik',
                  layout: 'default'
                },
                beforeEnter: ifAuthenticated
              },
              {
                path: 'papikostik/:id/update/:version',
                name: 'UpdatePapikostik',
                component: () =>
                  import(
                    /* webpackChunkName: "psychological-assessment" */ '../views/Materi/PsychologicalAssessment/Question/Papikostik.vue'
                  ),
                meta: {
                  rule: 'isAdmin',
                  breadCrumb: 'menu.update_papikostik',
                  layout: 'default'
                },
                beforeEnter: ifAuthenticated
              },
              {
                path: 'papikostik/:id/view/:version',
                name: 'DetailPapikostik',
                component: () =>
                  import(
                    /* webpackChunkName: "psychological-assessment" */ '../views/Materi/PsychologicalAssessment/Question/Papikostik.vue'
                  ),
                meta: {
                  rule: 'isAdmin',
                  breadCrumb: 'menu.view_papikostik',
                  layout: 'default',
                  disabled: true
                },
                beforeEnter: ifAuthenticated
              },
              {
                path: 'papikostik/:id/approve/:version',
                name: 'ApprovePapikostik',
                component: () =>
                  import(
                    /* webpackChunkName: "psychological-assessment" */ '../views/Materi/PsychologicalAssessment/Question/Papikostik.vue'
                  ),
                meta: {
                  rule: 'isAdmin',
                  breadCrumb: 'menu.approve_papikostik',
                  layout: 'default',
                  disabled: true,
                  approve: true
                },
                beforeEnter: ifAuthenticated
              },

              // Value
              {
                path: 'value/create/:version',
                name: 'CreateValue',
                component: () =>
                  import(
                    /* webpackChunkName: "psychological-assessment" */ '../views/Materi/PsychologicalAssessment/Question/Value.vue'
                  ),
                meta: {
                  rule: 'isAdmin',
                  breadCrumb: 'menu.add_value',
                  layout: 'default'
                },
                beforeEnter: ifAuthenticated
              },
              {
                path: 'value/:id/update/:version',
                name: 'UpdateValue',
                component: () =>
                  import(
                    /* webpackChunkName: "psychological-assessment" */ '../views/Materi/PsychologicalAssessment/Question/Value.vue'
                  ),
                meta: {
                  rule: 'isAdmin',
                  breadCrumb: 'menu.update_value',
                  layout: 'default'
                },
                beforeEnter: ifAuthenticated
              },
              {
                path: 'value/:id/view/:version',
                name: 'DetailValue',
                component: () =>
                  import(
                    /* webpackChunkName: "psychological-assessment" */ '../views/Materi/PsychologicalAssessment/Question/Value.vue'
                  ),
                meta: {
                  rule: 'isAdmin',
                  breadCrumb: 'menu.view_value',
                  layout: 'default',
                  disabled: true
                },
                beforeEnter: ifAuthenticated
              },
              {
                path: 'value/:id/approve/:version',
                name: 'ApproveValue',
                component: () =>
                  import(
                    /* webpackChunkName: "psychological-assessment" */ '../views/Materi/PsychologicalAssessment/Question/Value.vue'
                  ),
                meta: {
                  rule: 'isAdmin',
                  breadCrumb: 'menu.approve_value',
                  layout: 'default',
                  disabled: true,
                  approve: true
                },
                beforeEnter: ifAuthenticated
              },

              // LS
              {
                path: 'ls/create',
                name: 'CreateLs',
                component: () =>
                  import(
                    /* webpackChunkName: "psychological-assessment" */ '../views/Materi/PsychologicalAssessment/Question/Ls.vue'
                  ),
                meta: {
                  rule: 'isAdmin',
                  breadCrumb: 'menu.add_ls',
                  layout: 'default'
                },
                beforeEnter: ifAuthenticated
              },
              {
                path: 'ls/:id/update/:version',
                name: 'UpdateLs',
                component: () =>
                  import(
                    /* webpackChunkName: "psychological-assessment" */ '../views/Materi/PsychologicalAssessment/Question/Ls.vue'
                  ),
                meta: {
                  rule: 'isAdmin',
                  breadCrumb: 'menu.update_ls',
                  layout: 'default'
                },
                beforeEnter: ifAuthenticated
              },
              {
                path: 'ls/:id/view/:version',
                name: 'DetailLs',
                component: () =>
                  import(
                    /* webpackChunkName: "psychological-assessment" */ '../views/Materi/PsychologicalAssessment/Question/Ls.vue'
                  ),
                meta: {
                  rule: 'isAdmin',
                  breadCrumb: 'menu.view_ls',
                  layout: 'default',
                  disabled: true
                },
                beforeEnter: ifAuthenticated
              },
              {
                path: 'ls/:id/approve/:version',
                name: 'ApproveLs',
                component: () =>
                  import(
                    /* webpackChunkName: "psychological-assessment" */ '../views/Materi/PsychologicalAssessment/Question/Ls.vue'
                  ),
                meta: {
                  rule: 'isAdmin',
                  breadCrumb: 'menu.approve_ls',
                  layout: 'default',
                  disabled: true,
                  approve: true
                },
                beforeEnter: ifAuthenticated
              },

              // Belbin
              {
                path: 'belbin/create',
                name: 'CreateBelbin',
                component: () =>
                  import(
                    /* webpackChunkName: "psychological-assessment" */ '../views/Materi/PsychologicalAssessment/Question/Belbin.vue'
                  ),
                meta: {
                  rule: 'isAdmin',
                  breadCrumb: 'menu.add_belbin',
                  layout: 'default'
                },
                beforeEnter: ifAuthenticated
              },
              {
                path: 'belbin/:id/update/:version',
                name: 'UpdateBelbin',
                component: () =>
                  import(
                    /* webpackChunkName: "psychological-assessment" */ '../views/Materi/PsychologicalAssessment/Question/Belbin.vue'
                  ),
                meta: {
                  rule: 'isAdmin',
                  breadCrumb: 'menu.update_belbin',
                  layout: 'default'
                },
                beforeEnter: ifAuthenticated
              },
              {
                path: 'belbin/:id/view/:version',
                name: 'DetailBelbin',
                component: () =>
                  import(
                    /* webpackChunkName: "psychological-assessment" */ '../views/Materi/PsychologicalAssessment/Question/Belbin.vue'
                  ),
                meta: {
                  rule: 'isAdmin',
                  breadCrumb: 'menu.view_belbin',
                  layout: 'default',
                  disabled: true
                },
                beforeEnter: ifAuthenticated
              },
              {
                path: 'belbin/:id/approve/:version',
                name: 'ApproveBelbin',
                component: () =>
                  import(
                    /* webpackChunkName: "psychological-assessment" */ '../views/Materi/PsychologicalAssessment/Question/Belbin.vue'
                  ),
                meta: {
                  rule: 'isAdmin',
                  breadCrumb: 'menu.approve_belbin',
                  layout: 'default',
                  disabled: true,
                  approve: true
                },
                beforeEnter: ifAuthenticated
              },

              // IST
              {
                path: 'ist/create',
                name: 'CreateIst',
                component: () =>
                  import(
                    /* webpackChunkName: "psychological-assessment" */ '../views/Materi/PsychologicalAssessment/Question/Ist.vue'
                  ),
                meta: {
                  rule: 'isAdmin',
                  breadCrumb: 'menu.add_ist',
                  layout: 'default'
                },
                beforeEnter: ifAuthenticated
              },
              {
                path: 'ist/:id/update/:version',
                name: 'UpdateIst',
                component: () =>
                  import(
                    /* webpackChunkName: "psychological-assessment" */ '../views/Materi/PsychologicalAssessment/Question/Ist.vue'
                  ),
                meta: {
                  rule: 'isAdmin',
                  breadCrumb: 'menu.update_ist',
                  layout: 'default'
                },
                beforeEnter: ifAuthenticated
              },
              {
                path: 'ist/:id/view/:version',
                name: 'DetailIst',
                component: () =>
                  import(
                    /* webpackChunkName: "psychological-assessment" */ '../views/Materi/PsychologicalAssessment/Question/Ist.vue'
                  ),
                meta: {
                  rule: 'isAdmin',
                  breadCrumb: 'menu.view_ist',
                  layout: 'default',
                  disabled: true
                },
                beforeEnter: ifAuthenticated
              },

              // 16 PF
              {
                path: '16pf/create',
                name: 'CreatePf',
                component: () =>
                  import(
                    /* webpackChunkName: "psychological-assessment" */ '../views/Materi/PsychologicalAssessment/Question/Pf.vue'
                  ),
                meta: {
                  rule: 'isAdmin',
                  breadCrumb: 'menu.add_16pf',
                  layout: 'default'
                },
                beforeEnter: ifAuthenticated
              },
              {
                path: '16pf/:id/update/:version',
                name: 'UpdatePf',
                component: () =>
                  import(
                    /* webpackChunkName: "psychological-assessment" */ '../views/Materi/PsychologicalAssessment/Question/Pf.vue'
                  ),
                meta: {
                  rule: 'isAdmin',
                  breadCrumb: 'menu.update_16pf',
                  layout: 'default'
                },
                beforeEnter: ifAuthenticated
              },
              {
                path: '16pf/:id/view/:version',
                name: 'DetailPf',
                component: () =>
                  import(
                    /* webpackChunkName: "psychological-assessment" */ '../views/Materi/PsychologicalAssessment/Question/Pf.vue'
                  ),
                meta: {
                  rule: 'isAdmin',
                  breadCrumb: 'menu.view_16pf',
                  layout: 'default',
                  disabled: true
                },
                beforeEnter: ifAuthenticated
              },
              {
                path: 'pf/:id/approve/:version',
                name: 'ApprovePf',
                component: () =>
                  import(
                    /* webpackChunkName: "psychological-assessment" */ '../views/Materi/PsychologicalAssessment/Question/Pf.vue'
                  ),
                meta: {
                  rule: 'isAdmin',
                  breadCrumb: 'menu.approve_16pf',
                  layout: 'default',
                  disabled: true,
                  approve: true
                },
                beforeEnter: ifAuthenticated
              },

              /* =============== List Of Question : DISC, Papi Kostik, Value, LS, Belbin, IST, 16 PF  =============== */

              {
                path: 'ist',
                component: () =>
                  import(
                    /* webpackChunkName: "wrapper" */ '../views/Wrapper.vue'
                  ),
                meta: {
                  rule: 'isAdmin',
                  breadCrumb: 'menu.manage_psychological_assessment'
                },
                children: [
                  {
                    path: 'subtest1',
                    name: 'CreateSubtest1',
                    component: () =>
                      import(
                        /* webpackChunkName: "ist" */ '../views/Materi/PsychologicalAssessment/Question/Ist/Subtest1.vue'
                      ),
                    meta: {
                      rule: 'isAdmin',
                      breadCrumb: 'menu.subtest1',
                      layout: 'default'
                    },
                    beforeEnter: ifAuthenticated
                  },
                  {
                    path: 'subtest2',
                    name: 'CreateSubtest2',
                    component: () =>
                      import(
                        /* webpackChunkName: "ist" */ '../views/Materi/PsychologicalAssessment/Question/Ist/Subtest2.vue'
                      ),
                    meta: {
                      rule: 'isAdmin',
                      breadCrumb: 'menu.subtest2',
                      layout: 'default'
                    },
                    beforeEnter: ifAuthenticated
                  },
                  {
                    path: 'subtest3',
                    name: 'CreateSubtest3',
                    component: () =>
                      import(
                        /* webpackChunkName: "ist" */ '../views/Materi/PsychologicalAssessment/Question/Ist/Subtest3.vue'
                      ),
                    meta: {
                      rule: 'isAdmin',
                      breadCrumb: 'menu.subtest3',
                      layout: 'default'
                    },
                    beforeEnter: ifAuthenticated
                  },
                  {
                    path: 'subtest4',
                    name: 'CreateSubtest4',
                    component: () =>
                      import(
                        /* webpackChunkName: "ist" */ '../views/Materi/PsychologicalAssessment/Question/Ist/Subtest4.vue'
                      ),
                    meta: {
                      rule: 'isAdmin',
                      breadCrumb: 'menu.subtest4',
                      layout: 'default'
                    },
                    beforeEnter: ifAuthenticated
                  },
                  {
                    path: 'subtest5',
                    name: 'CreateSubtest5',
                    component: () =>
                      import(
                        /* webpackChunkName: "ist" */ '../views/Materi/PsychologicalAssessment/Question/Ist/Subtest5.vue'
                      ),
                    meta: {
                      rule: 'isAdmin',
                      breadCrumb: 'menu.subtest5',
                      layout: 'default'
                    },
                    beforeEnter: ifAuthenticated
                  },
                  {
                    path: 'subtest6',
                    name: 'CreateSubtest6',
                    component: () =>
                      import(
                        /* webpackChunkName: "ist" */ '../views/Materi/PsychologicalAssessment/Question/Ist/Subtest6.vue'
                      ),
                    meta: {
                      rule: 'isAdmin',
                      breadCrumb: 'menu.subtest6',
                      layout: 'default'
                    },
                    beforeEnter: ifAuthenticated
                  },
                  {
                    path: 'subtest7',
                    name: 'CreateSubtest7',
                    component: () =>
                      import(
                        /* webpackChunkName: "ist" */ '../views/Materi/PsychologicalAssessment/Question/Ist/Subtest7.vue'
                      ),
                    meta: {
                      rule: 'isAdmin',
                      breadCrumb: 'menu.subtest7',
                      layout: 'default'
                    },
                    beforeEnter: ifAuthenticated
                  },
                  {
                    path: 'subtest8',
                    name: 'CreateSubtest8',
                    component: () =>
                      import(
                        /* webpackChunkName: "ist" */ '../views/Materi/PsychologicalAssessment/Question/Ist/Subtest8.vue'
                      ),
                    meta: {
                      rule: 'isAdmin',
                      breadCrumb: 'menu.subtest8',
                      layout: 'default'
                    },
                    beforeEnter: ifAuthenticated
                  },
                  {
                    path: 'subtest9',
                    name: 'CreateSubtest9',
                    component: () =>
                      import(
                        /* webpackChunkName: "ist" */ '../views/Materi/PsychologicalAssessment/Question/Ist/Subtest9.vue'
                      ),
                    meta: {
                      rule: 'isAdmin',
                      breadCrumb: 'menu.subtest9',
                      layout: 'default'
                    },
                    beforeEnter: ifAuthenticated
                  },

                  {
                    path: 'subtest1/:id/view/:version',
                    name: 'DetailSubtest1',
                    component: () =>
                      import(
                        /* webpackChunkName: "ist" */ '../views/Materi/PsychologicalAssessment/Question/Ist/Subtest1.vue'
                      ),
                    meta: {
                      rule: 'isAdmin',
                      breadCrumb: 'menu.subtest1',
                      layout: 'default',
                      disabled: true
                    },
                    beforeEnter: ifAuthenticated
                  },
                  {
                    path: 'subtest2/:id/view/:version',
                    name: 'DetailSubtest2',
                    component: () =>
                      import(
                        /* webpackChunkName: "ist" */ '../views/Materi/PsychologicalAssessment/Question/Ist/Subtest2.vue'
                      ),
                    meta: {
                      rule: 'isAdmin',
                      breadCrumb: 'menu.subtest2',
                      layout: 'default',
                      disabled: true
                    },
                    beforeEnter: ifAuthenticated
                  },
                  {
                    path: 'subtest3/:id/view/:version',
                    name: 'DetailSubtest3',
                    component: () =>
                      import(
                        /* webpackChunkName: "ist" */ '../views/Materi/PsychologicalAssessment/Question/Ist/Subtest3.vue'
                      ),
                    meta: {
                      rule: 'isAdmin',
                      breadCrumb: 'menu.subtest3',
                      layout: 'default',
                      disabled: true
                    },
                    beforeEnter: ifAuthenticated
                  },
                  {
                    path: 'subtest4/:id/view/:version',
                    name: 'DetailSubtest4',
                    component: () =>
                      import(
                        /* webpackChunkName: "ist" */ '../views/Materi/PsychologicalAssessment/Question/Ist/Subtest4.vue'
                      ),
                    meta: {
                      rule: 'isAdmin',
                      breadCrumb: 'menu.subtest4',
                      layout: 'default',
                      disabled: true
                    },
                    beforeEnter: ifAuthenticated
                  },
                  {
                    path: 'subtest5/:id/view/:version',
                    name: 'DetailSubtest5',
                    component: () =>
                      import(
                        /* webpackChunkName: "ist" */ '../views/Materi/PsychologicalAssessment/Question/Ist/Subtest5.vue'
                      ),
                    meta: {
                      rule: 'isAdmin',
                      breadCrumb: 'menu.subtest5',
                      layout: 'default',
                      disabled: true
                    },
                    beforeEnter: ifAuthenticated
                  },
                  {
                    path: ':id/subtest6/:preview/view/:version',
                    name: 'DetailSubtest6',
                    component: () =>
                      import(
                        /* webpackChunkName: "ist" */ '../views/Materi/PsychologicalAssessment/Question/Ist/Subtest6.vue'
                      ),
                    meta: {
                      rule: 'isAdmin',
                      breadCrumb: 'menu.subtest6',
                      layout: 'default',
                      disabled: true
                    },
                    beforeEnter: ifAuthenticated
                  },
                  {
                    path: 'subtest7/:id/view/:version',
                    name: 'DetailSubtest7',
                    component: () =>
                      import(
                        /* webpackChunkName: "ist" */ '../views/Materi/PsychologicalAssessment/Question/Ist/Subtest7.vue'
                      ),
                    meta: {
                      rule: 'isAdmin',
                      breadCrumb: 'menu.subtest7',
                      layout: 'default',
                      disabled: true
                    },
                    beforeEnter: ifAuthenticated
                  },
                  {
                    path: 'subtest8/:id/view//:version',
                    name: 'DetailSubtest8',
                    component: () =>
                      import(
                        /* webpackChunkName: "ist" */ '../views/Materi/PsychologicalAssessment/Question/Ist/Subtest8.vue'
                      ),
                    meta: {
                      rule: 'isAdmin',
                      breadCrumb: 'menu.subtest8',
                      layout: 'default',
                      disabled: true
                    },
                    beforeEnter: ifAuthenticated
                  },
                  {
                    path: 'subtest9/:id/view/:version',
                    name: 'DetailSubtest9',
                    component: () =>
                      import(
                        /* webpackChunkName: "ist" */ '../views/Materi/PsychologicalAssessment/Question/Ist/Subtest9.vue'
                      ),
                    meta: {
                      rule: 'isAdmin',
                      breadCrumb: 'menu.subtest9',
                      layout: 'default',
                      disabled: true
                    },
                    beforeEnter: ifAuthenticated
                  },

                  {
                    path: 'subtest1/:id/update/:version',
                    name: 'UpdateSubtest1',
                    component: () =>
                      import(
                        /* webpackChunkName: "ist" */ '../views/Materi/PsychologicalAssessment/Question/Ist/Subtest1.vue'
                      ),
                    meta: {
                      rule: 'isAdmin',
                      breadCrumb: 'menu.subtest1',
                      layout: 'default'
                    },
                    beforeEnter: ifAuthenticated
                  },
                  {
                    path: 'subtest2/:id/update/:version',
                    name: 'UpdateSubtest2',
                    component: () =>
                      import(
                        /* webpackChunkName: "ist" */ '../views/Materi/PsychologicalAssessment/Question/Ist/Subtest2.vue'
                      ),
                    meta: {
                      rule: 'isAdmin',
                      breadCrumb: 'menu.subtest2',
                      layout: 'default'
                    },
                    beforeEnter: ifAuthenticated
                  },
                  {
                    path: 'subtest3/:id/update/:version',
                    name: 'UpdateSubtest3',
                    component: () =>
                      import(
                        /* webpackChunkName: "ist" */ '../views/Materi/PsychologicalAssessment/Question/Ist/Subtest3.vue'
                      ),
                    meta: {
                      rule: 'isAdmin',
                      breadCrumb: 'menu.subtest3',
                      layout: 'default'
                    },
                    beforeEnter: ifAuthenticated
                  },
                  {
                    path: 'subtest4/:id/update/:version',
                    name: 'UpdateSubtest4',
                    component: () =>
                      import(
                        /* webpackChunkName: "ist" */ '../views/Materi/PsychologicalAssessment/Question/Ist/Subtest4.vue'
                      ),
                    meta: {
                      rule: 'isAdmin',
                      breadCrumb: 'menu.subtest4',
                      layout: 'default'
                    },
                    beforeEnter: ifAuthenticated
                  },
                  {
                    path: 'subtest5/:id/update/:version',
                    name: 'UpdateSubtest5',
                    component: () =>
                      import(
                        /* webpackChunkName: "ist" */ '../views/Materi/PsychologicalAssessment/Question/Ist/Subtest5.vue'
                      ),
                    meta: {
                      rule: 'isAdmin',
                      breadCrumb: 'menu.subtest5',
                      layout: 'default'
                    },
                    beforeEnter: ifAuthenticated
                  },
                  {
                    path: 'subtest6/:id/update/:version',
                    name: 'UpdateSubtest6',
                    component: () =>
                      import(
                        /* webpackChunkName: "ist" */ '../views/Materi/PsychologicalAssessment/Question/Ist/Subtest6.vue'
                      ),
                    meta: {
                      rule: 'isAdmin',
                      breadCrumb: 'menu.subtest6',
                      layout: 'default'
                    },
                    beforeEnter: ifAuthenticated
                  },
                  {
                    path: 'subtest7/:id/update/:version',
                    name: 'UpdateSubtest7',
                    component: () =>
                      import(
                        /* webpackChunkName: "ist" */ '../views/Materi/PsychologicalAssessment/Question/Ist/Subtest7.vue'
                      ),
                    meta: {
                      rule: 'isAdmin',
                      breadCrumb: 'menu.subtest7',
                      layout: 'default'
                    },
                    beforeEnter: ifAuthenticated
                  },
                  {
                    path: 'subtest8/:id/update/:version',
                    name: 'UpdateSubtest8',
                    component: () =>
                      import(
                        /* webpackChunkName: "ist" */ '../views/Materi/PsychologicalAssessment/Question/Ist/Subtest8.vue'
                      ),
                    meta: {
                      rule: 'isAdmin',
                      breadCrumb: 'menu.subtest8',
                      layout: 'default'
                    },
                    beforeEnter: ifAuthenticated
                  },
                  {
                    path: 'subtest9/:id/update/:version',
                    name: 'UpdateSubtest9',
                    component: () =>
                      import(
                        /* webpackChunkName: "ist" */ '../views/Materi/PsychologicalAssessment/Question/Ist/Subtest9.vue'
                      ),
                    meta: {
                      rule: 'isAdmin',
                      breadCrumb: 'menu.subtest9',
                      layout: 'default'
                    },
                    beforeEnter: ifAuthenticated
                  },

                  {
                    path: 'subtest1/:id/approve/:version',
                    name: 'ApproveSubtest1',
                    component: () =>
                      import(
                        /* webpackChunkName: "psychological-assessment" */ '../views/Materi/PsychologicalAssessment/Question/Ist/Subtest1'
                      ),
                    meta: {
                      rule: 'isAdmin',
                      breadCrumb: 'menu.subtest1',
                      layout: 'default',
                      disabled: true,
                      approve: true
                    },
                    beforeEnter: ifAuthenticated
                  },
                  {
                    path: 'subtest2/:id/approve/:version',
                    name: 'ApproveSubtest2',
                    component: () =>
                      import(
                        /* webpackChunkName: "psychological-assessment" */ '../views/Materi/PsychologicalAssessment/Question/Ist/Subtest2'
                      ),
                    meta: {
                      rule: 'isAdmin',
                      breadCrumb: 'menu.subtest2',
                      layout: 'default',
                      disabled: true,
                      approve: true
                    },
                    beforeEnter: ifAuthenticated
                  },
                  {
                    path: 'subtest3/:id/approve/:version',
                    name: 'ApproveSubtest3',
                    component: () =>
                      import(
                        /* webpackChunkName: "psychological-assessment" */ '../views/Materi/PsychologicalAssessment/Question/Ist/Subtest3'
                      ),
                    meta: {
                      rule: 'isAdmin',
                      breadCrumb: 'menu.subtest3',
                      layout: 'default',
                      disabled: true,
                      approve: true
                    },
                    beforeEnter: ifAuthenticated
                  },
                  {
                    path: 'subtest4/:id/approve/:version',
                    name: 'ApproveSubtest4',
                    component: () =>
                      import(
                        /* webpackChunkName: "psychological-assessment" */ '../views/Materi/PsychologicalAssessment/Question/Ist/Subtest4'
                      ),
                    meta: {
                      rule: 'isAdmin',
                      breadCrumb: 'menu.subtest4',
                      layout: 'default',
                      disabled: true,
                      approve: true
                    },
                    beforeEnter: ifAuthenticated
                  },
                  {
                    path: 'subtest5/:id/approve/:version',
                    name: 'ApproveSubtest5',
                    component: () =>
                      import(
                        /* webpackChunkName: "psychological-assessment" */ '../views/Materi/PsychologicalAssessment/Question/Ist/Subtest5'
                      ),
                    meta: {
                      rule: 'isAdmin',
                      breadCrumb: 'menu.subtest5',
                      layout: 'default',
                      disabled: true,
                      approve: true
                    },
                    beforeEnter: ifAuthenticated
                  },
                  {
                    path: 'subtest6/:id/approve/:version',
                    name: 'ApproveSubtest6',
                    component: () =>
                      import(
                        /* webpackChunkName: "psychological-assessment" */ '../views/Materi/PsychologicalAssessment/Question/Ist/Subtest6'
                      ),
                    meta: {
                      rule: 'isAdmin',
                      breadCrumb: 'menu.subtest6',
                      layout: 'default',
                      disabled: true,
                      approve: true
                    },
                    beforeEnter: ifAuthenticated
                  },
                  {
                    path: 'subtest7/:id/approve/:version',
                    name: 'ApproveSubtest7',
                    component: () =>
                      import(
                        /* webpackChunkName: "psychological-assessment" */ '../views/Materi/PsychologicalAssessment/Question/Ist/Subtest7'
                      ),
                    meta: {
                      rule: 'isAdmin',
                      breadCrumb: 'menu.subtest7',
                      layout: 'default',
                      disabled: true,
                      approve: true
                    },
                    beforeEnter: ifAuthenticated
                  },
                  {
                    path: 'subtest8/:id/approve/:version',
                    name: 'ApproveSubtest8',
                    component: () =>
                      import(
                        /* webpackChunkName: "psychological-assessment" */ '../views/Materi/PsychologicalAssessment/Question/Ist/Subtest8'
                      ),
                    meta: {
                      rule: 'isAdmin',
                      breadCrumb: 'menu.subtest8',
                      layout: 'default',
                      disabled: true,
                      approve: true
                    },
                    beforeEnter: ifAuthenticated
                  },
                  {
                    path: 'subtest9/:id/approve/:version',
                    name: 'ApproveSubtest9',
                    component: () =>
                      import(
                        /* webpackChunkName: "psychological-assessment" */ '../views/Materi/PsychologicalAssessment/Question/Ist/Subtest9'
                      ),
                    meta: {
                      rule: 'isAdmin',
                      breadCrumb: 'menu.subtest9',
                      layout: 'default',
                      disabled: true,
                      approve: true
                    },
                    beforeEnter: ifAuthenticated
                  }
                ]
              },

              // instruction
              {
                path: ':type/instruction',
                name: 'instruction',
                component: () =>
                  import(
                    /* webpackChunkName: "psychological-assessment" */ '../views/Materi/PsychologicalAssessment/Instruction'
                  ),
                meta: {
                  rule: 'isAdmin',
                  breadCrumb: 'menu.manage_instruction',
                  layout: 'default'
                },
                beforeEnter: ifAuthenticated
              }
            ]
          }
        ]
      }

      /* --------- Main Psychological Assessment --------- */
    ]
  },
  // End Materi

  /* ========== Start Management Project ========== */
  {
    path: '/project',
    component: () =>
      import(/* webpackChunkName: "wrapper" */ '../views/Wrapper.vue'),
    meta: {
      rule: 'isGeneral',
      breadCrumb: 'menu.project'
    },
    children: [
      {
        path: '',
        name: 'MasterProject',
        component: () =>
          import(
            /* webpackChunkName: "master-project" */ '../views/Project/Index.vue'
          ),
        meta: {
          rule: 'isGeneral',
          breadCrumb: 'menu.project',
          layout: 'default'
        },
        beforeEnter: ifAuthenticated
      },
      {
        path: 'create',
        name: 'CreateProject',
        component: () =>
          import(
            /* webpackChunkName: "master-project" */ '../views/Project/CreateEdit.vue'
          ),
        meta: {
          rule: 'isGeneral',
          breadCrumb: 'menu.add_project',
          layout: 'default'
        },
        beforeEnter: ifAuthenticated
      },
      {
        path: 'edit/:id',
        name: 'UpdateProject',
        component: () =>
          import(
            /* webpackChunkName: "master-project" */ '../views/Project/CreateEdit.vue'
          ),
        meta: {
          rule: 'isGeneral',
          breadCrumb: 'menu.edit_project',
          layout: 'default'
        },
        beforeEnter: ifAuthenticated
      },
      {
        path: 'detail/:idProyek',
        component: () =>
          import(/* webpackChunkName: "wrapper" */ '../views/Wrapper.vue'),
        meta: {
          rule: 'isGeneral',
          breadCrumb: 'menu.view_project'
        },
        children: [
          {
            path: '',
            name: 'DetailProject',
            component: () =>
              import(
                /* webpackChunkName: "master-project" */ '../views/Project/DetailProject.vue'
              ),
            meta: {
              rule: 'isGeneral',
              breadCrumb: 'menu.view_project',
              layout: 'default'
            },
            beforeEnter: ifAuthenticated
          },
          {
            path: 'asesi/:idAsesi',
            component: () =>
              import(/* webpackChunkName: "wrapper" */ '../views/Wrapper.vue'),
            meta: {
              rule: 'isGeneral',
              breadCrumb: 'menu.detail_asesi',
              layout: 'default'
            },
            children: [
              {
                path: '',
                name: 'DetailAsesi',
                component: () =>
                  import(
                    /* webpackChunkName: "master-project" */ '../views/Project/DetailAsesi.vue'
                  ),
                meta: {
                  rule: 'isGeneral',
                  breadCrumb: 'menu.detail_asesi',
                  layout: 'default'
                },
                beforeEnter: ifAuthenticated
              },
              {
                path: 'answer/:typeSoal/:proyekSoalId/:status',
                name: 'SeeAnswer',
                component: () =>
                  import(
                    /* webpackChunkName: "master-project" */ '../views/Project/SeeAnswer.vue'
                  ),
                meta: {
                  rule: 'isGeneral',
                  breadCrumb: 'menu.see_answer',
                  layout: 'default'
                },
                beforeEnter: ifAuthenticated
              },
              {
                path:
                  'report-informasi-asesi/:proyekSoalId/:proyekResultId/:assesment',
                component: () =>
                  import(
                    /* webpackChunkName: "wrapper" */ '../views/Wrapper.vue'
                  ),
                meta: {
                  rule: 'isGeneral',
                  breadCrumb: 'menu.view_project'
                },
                children: [
                  {
                    path: '',
                    name: 'ProyekReport',
                    component: () =>
                      import(
                        /* webpackChunkName: "master-project" */ '../views/Project/Report.vue'
                      ),
                    meta: {
                      rule: 'isAsesor',
                      breadCrumb: 'menu.see_answer',
                      layout: 'default'
                    },
                    beforeEnter: ifAuthenticated
                  },
                  {
                    path: 'print',
                    name: 'PrintProyekReport',
                    component: () =>
                      import(
                        /* webpackChunkName: "master-project" */ '../views/Project/Report.vue'
                      ),
                    meta: {
                      rule: 'isAsesor',
                      breadCrumb: 'menu.see_answer',
                      layout: 'print'
                    },
                    beforeEnter: ifAuthenticated
                  }
                ]
              },
              // theory in see answer
              // ASSESSMENT CENTER
              // SOI
              {
                path: ':id/detail/:version',
                name: 'SoiProyek',
                component: () =>
                  import(
                    /* webpackChunkName: "sub-ordinate-interaction" */ '../views/Materi/AssessmentCenter/Interaction/SubOrdinateInteraction'
                  ),
                meta: {
                  rule: 'isGeneral',
                  breadCrumb: 'menu.view_sub_ordinate_interaction',
                  layout: 'default',
                  type: 'SOI',
                  disabled: true,
                  proyek: true
                },
                beforeEnter: ifAuthenticated
              },
              // PI
              {
                path: ':id/detail/:version',
                name: 'PeerInteractionProyek',
                component: () =>
                  import(
                    /* webpackChunkName: "peer-interaction" */ '../views/Materi/AssessmentCenter/Interaction/PeerInteraction.vue'
                  ),
                meta: {
                  rule: 'isGeneral',
                  breadCrumb: 'menu.view_peer_interaction',
                  layout: 'default',
                  type: 'PI',
                  disabled: true,
                  proyek: true
                },
                beforeEnter: ifAuthenticated
              },
              // CI
              {
                path: ':id/detail/:version',
                name: 'CustomerInteractionProyek',
                component: () =>
                  import(
                    /* webpackChunkName: "customer-interaction" */ '../views/Materi/AssessmentCenter/Interaction/CustomerInteraction.vue'
                  ),
                meta: {
                  rule: 'isGeneral',
                  breadCrumb: 'menu.view_customer_interaction',
                  layout: 'default',
                  type: 'CI',
                  disabled: true,
                  proyek: true
                },
                beforeEnter: ifAuthenticated
              },
              // In Basket
              {
                path: ':id/detail/:version',
                name: 'InBasketProyek',
                component: () =>
                  import(
                    /* webpackChunkName: "inbasket-intray" */ '../views/Materi/AssessmentCenter/Individual/InBasket'
                  ),
                meta: {
                  rule: 'isGeneral',
                  breadCrumb: 'menu.view_in_basket_in_tray',
                  layout: 'default',
                  type: 'INB',
                  disabled: true,
                  proyek: true
                },
                beforeEnter: ifAuthenticated
              },
              // PA
              {
                path: ':id/detail/:version',
                name: 'ProblemAnalysisProyek',
                component: () =>
                  import(
                    /* webpackChunkName: "problem-analysis" */ '../views/Materi/AssessmentCenter/Individual/ProblemAnalysis'
                  ),
                meta: {
                  rule: 'isGeneral',
                  breadCrumb: 'menu.view_problem_analysis',
                  layout: 'default',
                  type: 'PA',
                  disabled: true,
                  proyek: true
                },
                beforeEnter: ifAuthenticated
              },
              // IE
              {
                path: ':id/detail/:version',
                name: 'IpEpProyek',
                component: () =>
                  import(
                    /* webpackChunkName: "improvement-enhancement" */ '../views/Materi/AssessmentCenter/Individual/ImprovementEnchanment'
                  ),
                meta: {
                  rule: 'isGeneral',
                  breadCrumb: 'menu.view_improvement_enchantment',
                  layout: 'default',
                  type: 'IE',
                  disabled: true,
                  proyek: true
                },
                beforeEnter: ifAuthenticated
              },
              // Scheduling
              {
                path: ':id/detail/:version',
                name: 'SchedulingProyek',
                component: () =>
                  import(
                    /* webpackChunkName: "scheduling" */ '../views/Materi/AssessmentCenter/Individual/Scheduling'
                  ),
                meta: {
                  rule: 'isGeneral',
                  breadCrumb: 'menu.view_scheduling',
                  layout: 'default',
                  type: 'SCH',
                  disabled: true
                },
                beforeEnter: ifAuthenticated
              },
              // Proposal Writing
              {
                path: ':id/detail/:version',
                name: 'ProposalWritingProyek',
                component: () =>
                  import(
                    /* webpackChunkName: "proposal-writing" */ '../views/Materi/AssessmentCenter/Individual/ProposalWriting'
                  ),
                meta: {
                  rule: 'isGeneral',
                  breadCrumb: 'menu.detail_proposal_writing',
                  layout: 'default',
                  type: 'PW',
                  disabled: true,
                  proyek: true
                },
                beforeEnter: ifAuthenticated
              },
              // LGD
              {
                path: ':id/detail/:version',
                name: 'LgdProyek',
                component: () =>
                  import(
                    /* webpackChunkName: "leaderless-group-discussion" */ '../views/Materi/AssessmentCenter/Groups/LeaderlessDiscussion'
                  ),
                meta: {
                  rule: 'isGeneral',
                  breadCrumb: 'menu.view_leaderless_discussion',
                  layout: 'default',
                  type: 'LGD',
                  disabled: true,
                  proyek: true
                },
                beforeEnter: ifAuthenticated
              },
              // PSYCHOLOGIVAL ASSESSMENT
              // DISC
              {
                path: 'disc/:id/view/:version',
                name: 'DiscProyek',
                component: () =>
                  import(
                    /* webpackChunkName: "psychological-assessment" */ '../views/Materi/PsychologicalAssessment/Question/Disc.vue'
                  ),
                meta: {
                  rule: 'isGeneral',
                  breadCrumb: 'menu.view_disc',
                  layout: 'default',
                  disabled: true,
                  proyek: true
                },
                beforeEnter: ifAuthenticated
              },
              // PapiKostik
              {
                path: 'papikostik/:id/view/:version',
                name: 'PapiProyek',
                component: () =>
                  import(
                    /* webpackChunkName: "psychological-assessment" */ '../views/Materi/PsychologicalAssessment/Question/Papikostik.vue'
                  ),
                meta: {
                  rule: 'isGeneral',
                  breadCrumb: 'menu.view_papikostik',
                  layout: 'default',
                  disabled: true,
                  proyek: true
                },
                beforeEnter: ifAuthenticated
              },
              // Value
              {
                path: 'value/:id/view/:version',
                name: 'ValueProyek',
                component: () =>
                  import(
                    /* webpackChunkName: "psychological-assessment" */ '../views/Materi/PsychologicalAssessment/Question/Value.vue'
                  ),
                meta: {
                  rule: 'isGeneral',
                  breadCrumb: 'menu.view_value',
                  layout: 'default',
                  disabled: true,
                  proyek: true
                },
                beforeEnter: ifAuthenticated
              },
              // LS
              {
                path: 'ls/:id/view/:version',
                name: 'LsProyek',
                component: () =>
                  import(
                    /* webpackChunkName: "psychological-assessment" */ '../views/Materi/PsychologicalAssessment/Question/Ls.vue'
                  ),
                meta: {
                  rule: 'isGeneral',
                  breadCrumb: 'menu.view_ls',
                  layout: 'default',
                  disabled: true,
                  proyek: true
                },
                beforeEnter: ifAuthenticated
              },
              // BELBIN
              {
                path: 'belbin/:id/view/:version',
                name: 'BelbinProyek',
                component: () =>
                  import(
                    /* webpackChunkName: "psychological-assessment" */ '../views/Materi/PsychologicalAssessment/Question/Belbin.vue'
                  ),
                meta: {
                  rule: 'isGeneral',
                  breadCrumb: 'menu.view_belbin',
                  layout: 'default',
                  disabled: true,
                  proyek: true
                },
                beforeEnter: ifAuthenticated
              },
              // 16PF
              {
                path: '16pf/:id/view/:version',
                name: 'Pf16Proyek',
                component: () =>
                  import(
                    /* webpackChunkName: "psychological-assessment" */ '../views/Materi/PsychologicalAssessment/Question/Pf.vue'
                  ),
                meta: {
                  rule: 'isGeneral',
                  breadCrumb: 'menu.view_16pf',
                  layout: 'default',
                  disabled: true,
                  proyek: true
                },
                beforeEnter: ifAuthenticated
              },
              // IST
              {
                path: 'ist/:id/view/:version',
                name: 'IstProyek',
                component: () =>
                  import(
                    /* webpackChunkName: "psychological-assessment" */ '../views/Materi/PsychologicalAssessment/Question/Ist.vue'
                  ),
                meta: {
                  rule: 'isGeneral',
                  breadCrumb: 'menu.view_ist',
                  layout: 'default',
                  disabled: true,
                  proyek: true
                },
                beforeEnter: ifAuthenticated
              },
              {
                path: 'subtest1/:id/view/:version',
                name: 'DetailSubtest1Proyek',
                component: () =>
                  import(
                    /* webpackChunkName: "ist" */ '../views/Materi/PsychologicalAssessment/Question/Ist/Subtest1.vue'
                  ),
                meta: {
                  rule: 'isGeneral',
                  breadCrumb: 'menu.subtest1',
                  layout: 'default',
                  disabled: true,
                  proyek: true
                },
                beforeEnter: ifAuthenticated
              },
              {
                path: 'subtest2/:id/view/:version',
                name: 'DetailSubtest2Proyek',
                component: () =>
                  import(
                    /* webpackChunkName: "ist" */ '../views/Materi/PsychologicalAssessment/Question/Ist/Subtest2.vue'
                  ),
                meta: {
                  rule: 'isGeneral',
                  breadCrumb: 'menu.subtest2',
                  layout: 'default',
                  disabled: true,
                  proyek: true
                },
                beforeEnter: ifAuthenticated
              },
              {
                path: 'subtest3/:id/view/:version',
                name: 'DetailSubtest3Proyek',
                component: () =>
                  import(
                    /* webpackChunkName: "ist" */ '../views/Materi/PsychologicalAssessment/Question/Ist/Subtest3.vue'
                  ),
                meta: {
                  rule: 'isGeneral',
                  breadCrumb: 'menu.subtest3',
                  layout: 'default',
                  disabled: true,
                  proyek: true
                },
                beforeEnter: ifAuthenticated
              },
              {
                path: 'subtest4/:id/view/:version',
                name: 'DetailSubtest4Proyek',
                component: () =>
                  import(
                    /* webpackChunkName: "ist" */ '../views/Materi/PsychologicalAssessment/Question/Ist/Subtest4.vue'
                  ),
                meta: {
                  rule: 'isGeneral',
                  breadCrumb: 'menu.subtest4',
                  layout: 'default',
                  disabled: true,
                  proyek: true
                },
                beforeEnter: ifAuthenticated
              },
              {
                path: 'subtest5/:id/view/:version',
                name: 'DetailSubtest5Proyek',
                component: () =>
                  import(
                    /* webpackChunkName: "ist" */ '../views/Materi/PsychologicalAssessment/Question/Ist/Subtest5.vue'
                  ),
                meta: {
                  rule: 'isGeneral',
                  breadCrumb: 'menu.subtest5',
                  layout: 'default',
                  disabled: true,
                  proyek: true
                },
                beforeEnter: ifAuthenticated
              },
              {
                path: ':id/subtest6/:preview/view/:version',
                name: 'DetailSubtest6Proyek',
                component: () =>
                  import(
                    /* webpackChunkName: "ist" */ '../views/Materi/PsychologicalAssessment/Question/Ist/Subtest6.vue'
                  ),
                meta: {
                  rule: 'isGeneral',
                  breadCrumb: 'menu.subtest6',
                  layout: 'default',
                  disabled: true,
                  proyek: true
                },
                beforeEnter: ifAuthenticated
              },
              {
                path: 'subtest7/:id/view/:version',
                name: 'DetailSubtest7Proyek',
                component: () =>
                  import(
                    /* webpackChunkName: "ist" */ '../views/Materi/PsychologicalAssessment/Question/Ist/Subtest7.vue'
                  ),
                meta: {
                  rule: 'isGeneral',
                  breadCrumb: 'menu.subtest7',
                  layout: 'default',
                  disabled: true,
                  proyek: true
                },
                beforeEnter: ifAuthenticated
              },
              {
                path: 'subtest8/:id/view/:version',
                name: 'DetailSubtest8Proyek',
                component: () =>
                  import(
                    /* webpackChunkName: "ist" */ '../views/Materi/PsychologicalAssessment/Question/Ist/Subtest8.vue'
                  ),
                meta: {
                  rule: 'isGeneral',
                  breadCrumb: 'menu.subtest8',
                  layout: 'default',
                  disabled: true,
                  proyek: true
                },
                beforeEnter: ifAuthenticated
              },
              {
                path: 'subtest9/:id/view/:version',
                name: 'DetailSubtest9Proyek',
                component: () =>
                  import(
                    /* webpackChunkName: "ist" */ '../views/Materi/PsychologicalAssessment/Question/Ist/Subtest9.vue'
                  ),
                meta: {
                  rule: 'isGeneral',
                  breadCrumb: 'menu.subtest9',
                  layout: 'default',
                  disabled: true,
                  proyek: true
                },
                beforeEnter: ifAuthenticated
              },

              // FLP
              {
                path: ':id/flp/:version',
                name: 'FlpInBasketProyek',
                component: () =>
                  import(
                    /* webpackChunkName: "inbasket-intray" */ '../views/Materi/AssessmentCenter/Flp'
                  ),
                meta: {
                  rule: 'isGeneral',
                  breadCrumb: 'menu.flp_in_basket_in_tray',
                  layout: 'default',
                  type: 'INB',
                  proyek: true,
                  disabled: true
                },
                beforeEnter: ifAuthenticated
              },
              {
                path: ':id/flp/:version',
                name: 'FlpPeerInteractionProyek',
                component: () =>
                  import(
                    /* webpackChunkName: "peer-interaction" */ '../views/Materi/AssessmentCenter/Flp'
                  ),
                meta: {
                  rule: 'isAdmin',
                  breadCrumb: 'menu.flp_peer_interaction',
                  layout: 'default',
                  type: 'PI',
                  proyek: true,
                  disabled: true
                },
                beforeEnter: ifAuthenticated
              },
              {
                path: ':id/flp/:version',
                name: 'FlpLeaderlessDiscussionProyek',
                component: () =>
                  import(
                    /* webpackChunkName: "leaderless-group-discussion" */ '../views/Materi/AssessmentCenter/Flp'
                  ),
                meta: {
                  rule: 'isGeneral',
                  breadCrumb: 'menu.flp_leaderless_discussion',
                  layout: 'default',
                  type: 'LGD',
                  proyek: true,
                  disabled: true
                },
                beforeEnter: ifAuthenticated
              }
            ]
          }
        ]
      }
    ]
  }
  /* ========== End Management Project ========== */
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

export default router;
