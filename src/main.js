import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import i18n from './plugins/vue-i18n';
import './plugins';
import './registerServiceWorker';

import '@/assets/css/tailwind.css'; // Tailwind styles
import '@/assets/css/custom.css'; // Tailwind styles

// Layouts
import DefaultLayout from '@/components/layouts/Default.vue';
import NoSidebarLayout from '@/components/layouts/NoSidebar.vue';
import PrintLayout from '@/components/layouts/Print.vue';

Vue.component('default-layout', DefaultLayout);
Vue.component('no-sidebar-layout', NoSidebarLayout);
Vue.component('print-layout', PrintLayout);

Vue.config.productionTip = false;

new Vue({
  i18n,
  router,
  store,
  render: (h) => h(App)
}).$mount('#app');
