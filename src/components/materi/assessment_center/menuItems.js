export default [
  {
    code: 'SOI',
    type: 'Sub Ordinate Interaction',
    name: 'Sub Ordinate Interaction',
    groupName: 'Simulasi Interaksi',
    toManage: 'ManageSubOrdinateInteraction',
    toCreate: 'CreateSubOrdinateInteraction',
    toUpdate: 'UpdateSubOrdinateInteraction',
    toDetail: 'DetailSubOrdinateInteraction',
    toApprove: 'ApproveSubOrdinateInteraction',
    key: {
      approve: 'subOrdinateInteraction/fetchApprove',
      delete: 'subOrdinateInteraction/fetchDelete'
    }
  },
  {
    code: 'PI',
    type: 'Peer Interaction',
    name: 'Peer Interaction',
    groupName: 'Simulasi Interaksi',
    toManage: 'ManagePeerInteraction',
    toCreate: 'CreatePeerInteraction',
    toUpdate: 'UpdatePeerInteraction',
    toDetail: 'DetailPeerInteraction',
    toApprove: 'ApprovePeerInteraction',
    toFLP: {
      create: 'CreateFlpPeerInteraction',
      update: 'UpdateFlpPeerInteraction',
      approve: 'ApproveFlpPeerInteraction'
    },
    key: {
      approve: 'peerInteraction/fetchApprove',
      delete: 'peerInteraction/fetchDelete'
    }
  },
  {
    code: 'CI',
    type: 'Customer Interaction',
    name: 'Customer Interaction',
    groupName: 'Simulasi Interaksi',
    toManage: 'ManageCustomerInteraction',
    toCreate: 'CreateCustomerInteraction',
    toUpdate: 'UpdateCustomerInteraction',
    toDetail: 'DetailCustomerInteraction',
    toApprove: 'ApproveCustomerInteraction',
    key: {
      approve: 'customerInteraction/fetchApprove',
      delete: 'customerInteraction/fetchDelete'
    }
  },
  {
    code: 'INB',
    type: 'In - Basket',
    name: 'IN - Basket / IN Tray',
    groupName: 'Simulasi Individual',
    toManage: 'ManageInBasket',
    toCreate: 'CreateInBasket',
    toUpdate: 'UpdateInBasket',
    toDetail: 'DetailInBasket',
    toApprove: 'ApproveInBasket',
    toFLP: {
      create: 'CreateFlpInBasket',
      update: 'UpdateFlpInBasket',
      approve: 'ApproveFlpInBasket'
    },
    key: {
      approve: 'inBasket/fetchApprove',
      delete: 'inBasket/fetchDelete'
    }
  },
  {
    code: 'PA',
    type: 'Problem Analysis',
    name: 'Problem Analysis',
    groupName: 'Simulasi Individual',
    toManage: 'ManageProblemAnalysis',
    toCreate: 'CreateProblemAnalysis',
    toUpdate: 'UpdateProblemAnalysis',
    toDetail: 'DetailProblemAnalysis',
    toApprove: 'ApproveProblemAnalysis',
    key: {
      approve: 'problemAnalysis/fetchApprove',
      delete: 'problemAnalysis/fetchDelete'
    }
  },
  {
    code: 'IE',
    type: 'Improvement-Enhancement Program',
    name: 'Improvement / Enhancement',
    groupName: 'Simulasi Individual',
    toManage: 'ManageImprovementEnchantment',
    toCreate: 'CreateImprovementEnchantment',
    toUpdate: 'UpdateImprovementEnchantment',
    toDetail: 'DetailImprovementEnchantment',
    toApprove: 'ApproveImprovementEnchantment',
    key: {
      approve: 'improvementEnchantment/fetchApprove',
      delete: 'improvementEnchantment/fetchDelete'
    }
  },
  {
    code: 'SCH',
    type: 'Scheduling',
    name: 'Scheduling',
    groupName: 'Simulasi Individual',
    toManage: 'ManageScheduling',
    toCreate: 'CreateScheduling',
    toUpdate: 'UpdateScheduling',
    toDetail: 'DetailScheduling',
    toApprove: 'ApproveScheduling',
    key: {
      approve: 'scheduling/fetchApprove',
      delete: 'scheduling/fetchDelete'
    }
  },
  {
    code: 'PW',
    type: 'Proposal Writing',
    name: 'Proposal Writing',
    groupName: 'Simulasi Individual',
    toManage: 'ManageProposalWriting',
    toCreate: 'CreateProposalWriting',
    toUpdate: 'UpdateProposalWriting',
    toDetail: 'DetailProposalWriting',
    toApprove: 'ApproveProposalWriting',
    key: {
      approve: 'proposalWriting/fetchApprove',
      delete: 'proposalWriting/fetchDelete'
    }
  },
  {
    code: 'LGD',
    groupName: 'Simulasi Kelompok',
    type: 'Leaderless Group Discussion',
    name: 'Leaderless Group Discussion',
    toManage: 'ManageLeaderlessDiscussion',
    toCreate: 'CreateLeaderlessDiscussion',
    toUpdate: 'UpdateLeaderlessDiscussion',
    toDetail: 'DetailLeaderlessDiscussion',
    toApprove: 'ApproveLeaderlessDiscussion',
    toFLP: {
      create: 'CreateFlpLeaderlessDiscussion',
      update: 'UpdateFlpLeaderlessDiscussion',
      approve: 'ApproveFlpLeaderlessDiscussion'
    },
    key: {
      approve: 'leaderlessDiscussion/fetchApprove',
      delete: 'leaderlessDiscussion/fetchDelete'
    }
  }
];
