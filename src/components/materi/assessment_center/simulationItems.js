import { cloneDeep } from 'lodash';

// --- simulation type filter ---
export const simulationType = {
  umum: {
    code: 'INSTRUKSI_UMUM',
    name: 'Instruksi Umum'
  },
  assessor: {
    code: 'INSTRUKSI_ASSESSOR',
    name: 'Instruksi Assessor'
  },
  asesi: {
    code: 'INSTRUKSI_ASESI',
    name: 'Instruksi Asesi'
  },
  peran: {
    code: 'INSTRUKSI_PERAN',
    name: 'Instruksi Peran'
  },
  lampiran: { code: 'LAMPIRAN', name: 'Lampiran' },
  inbox: { code: 'INBOX', name: 'Inbox' },
  lembarJadwal: {
    code: 'LEMBAR_JADWAL',
    name: 'Lembar Jawab'
  },
  lembarTugas: {
    code: 'LEMBAR_TUGAS',
    name: 'Daftar Tugas'
  },
  lembarTambahan: {
    code: 'LEMBAR_TUGAS_TAMBAHAN',
    name: 'Info Tambahan'
  }
};

// --- simulation initial empty object
export const simulationSection = {
  namaInstruksi: '',
  contentInstruksi: ''
};

export const simulationQuestion = {
  nomor: 1,
  pertanyaan: ''
};

// --- simulation type inbox ---
export const emptyInbox = {
  subject: '',
  from: '',
  to: '',
  cc: '',
  duration: {
    time: '',
    date: ''
  },
  body: '\0'
};
export const simulationInbox = {
  jenisInstruksi: simulationType.inbox.code,
  durasi: null,
  sectionInstruksi: []
};

export const emptyAssignment = {
  date: '',
  total: '',
  body: '\0',
  additional: false,
  bodyAdditional: '\0'
};
export const emptyScheduling = {
  namaInstruksi: '',
  contentInstruksi: '',
  isiInstruksi: {
    periodeAwal: null,
    periodeAkhir: null,
    tanggal: '',
    jumlahOrang: '',
    durasiTambahan: null
  }
};

// --- simulation type content ---
const _base = {
  soalName: '',
  targetJobName: '',
  durasiPengerjaan: null,
  soal: {
    instruksi: []
  }
};

_base.soal.instruksi = [
  {
    jenisInstruksi: simulationType.umum.code,
    order: 1,
    sectionInstruksi: [
      {
        namaInstruksi: '',
        contentInstruksi: '',
        order: 1
      }
    ]
  }
];
export const subOrdinateInteraction = cloneDeep(_base);

_base.soal.instruksi = [
  {
    jenisInstruksi: simulationType.umum.code,
    order: 1,
    sectionInstruksi: [
      {
        namaInstruksi: '',
        contentInstruksi: '',
        order: 1
      }
    ]
  },
  {
    jenisInstruksi: simulationType.peran.code,
    order: 2,
    sectionInstruksi: [
      {
        namaInstruksi: '',
        contentInstruksi: '',
        order: 1
      }
    ]
  }
];
export const peerInteraction = cloneDeep(_base);

_base.soal.instruksi = [
  {
    jenisInstruksi: simulationType.umum.code,
    order: 1,
    sectionInstruksi: [
      {
        namaInstruksi: '',
        contentInstruksi: '',
        order: 1
      }
    ]
  },
  {
    jenisInstruksi: simulationType.peran.code,
    order: 2,
    sectionInstruksi: [
      {
        namaInstruksi: '',
        contentInstruksi: '',
        order: 1
      }
    ]
  }
];
export const customerInteraction = cloneDeep(_base);

_base.soal.instruksi = [
  {
    jenisInstruksi: simulationType.umum.code,
    order: 1,
    sectionInstruksi: [
      {
        namaInstruksi: '',
        contentInstruksi: '',
        order: 1
      }
    ]
  }
];
export const inBasket = cloneDeep(_base);

_base.soal.instruksi = [
  {
    jenisInstruksi: simulationType.umum.code,
    order: 1,
    sectionInstruksi: [
      {
        namaInstruksi: '',
        contentInstruksi: '',
        order: 1
      }
    ]
  }
];
export const problemAnalysis = cloneDeep(_base);

_base.soal.instruksi = [
  {
    jenisInstruksi: simulationType.umum.code,
    order: 1,
    sectionInstruksi: [
      {
        namaInstruksi: '',
        contentInstruksi: '',
        order: 1
      }
    ]
  },
  {
    jenisInstruksi: simulationType.asesi.code,
    order: 2,
    sectionInstruksi: [
      {
        namaInstruksi: '',
        contentInstruksi: '',
        order: 1
      }
    ]
  }
];
export const improvementEnhancement = cloneDeep(_base);

_base.soal.instruksi = [
  {
    jenisInstruksi: simulationType.umum.code,
    order: 1,
    sectionInstruksi: [
      {
        namaInstruksi: '',
        contentInstruksi: '',
        order: 1
      }
    ]
  }
];
export const proposalWriting = cloneDeep(_base);

_base.soal.instruksi = [
  {
    jenisInstruksi: simulationType.umum.code,
    order: 1,
    sectionInstruksi: [
      {
        namaInstruksi: '',
        contentInstruksi: '',
        order: 1
      }
    ]
  }
];
export const scheduling = cloneDeep(_base);

_base.soal.instruksi = [
  {
    jenisInstruksi: simulationType.umum.code,
    order: 1,
    sectionInstruksi: [
      {
        namaInstruksi: '',
        contentInstruksi: '',
        order: 1
      }
    ]
  },
  {
    jenisInstruksi: simulationType.peran.code,
    order: 2,
    sectionInstruksi: [
      {
        namaInstruksi: '',
        contentInstruksi: '',
        order: 1
      }
    ]
  }
];
export const leaderLessGroupDiscussion = cloneDeep(_base);
