/*=========================================================================================
  File Name: sidebarItems.js
  Description: Sidebar Items list. Add / Remove menu items from here.
  Strucutre:
          url     => router path
          name    => name to display in sidebar
          slug    => router path name
          icon    => Feather Icon component/icon name
          tag     => text to display on badge
          tagColor  => class to apply on badge element
          i18n    => Internationalization
          submenu   => submenu of current item (current item will become dropdown )
                NOTE: Submenu don't have any icon(you can add icon if u want to display)
          isDisabled  => disable sidebar item/group
==========================================================================================*/

export default [
  {
    url: 'UserLogin',
    icon: 'person_outline',
    i18n: 'menu.user_login',
    rule: 'isAdmin'
  },
  {
    icon: 'inbox_outline',
    i18n: 'menu.master_data',
    rule: 'isAdmin',
    subMenu: [
      {
        url: 'MasterUserInternal',
        icon: 'radio_button_unchecked',
        i18n: 'menu.manage_user_internal',
        rule: 'isAdmin'
      },
      {
        url: 'MasterPerusahaan',
        icon: 'radio_button_unchecked',
        i18n: 'menu.manage_company',
        rule: 'isAdmin'
      },
      {
        url: 'MasterPeserta',
        icon: 'radio_button_unchecked',
        i18n: 'menu.manage_participant',
        rule: 'isAdmin'
      },
      {
        url: 'MasterAssessor',
        icon: 'radio_button_unchecked',
        i18n: 'menu.manage_assessor',
        rule: 'isAdmin'
      }
    ]
  },
  {
    icon: 'work_outline',
    i18n: 'menu.materi',
    rule: 'isAdmin',
    subMenu: [
      {
        url: 'ManagePsychologicalAssessment',
        icon: 'library_books',
        i18n: 'menu.manage_psychological_assessment',
        rule: 'isAdmin'
      },
      {
        icon: 'assessment',
        i18n: 'menu.manage_assessment_center',
        rule: 'isAdmin',
        subMenu: [
          {
            url: 'ManageSubOrdinateInteraction',
            icon: 'radio_button_unchecked',
            i18n: 'menu.manage_sub_ordinate_interaction',
            rule: 'isAdmin'
          },
          {
            url: 'ManagePeerInteraction',
            icon: 'radio_button_unchecked',
            i18n: 'menu.manage_peer_interaction',
            rule: 'isAdmin'
          },
          {
            url: 'ManageCustomerInteraction',
            icon: 'radio_button_unchecked',
            i18n: 'menu.manage_customer_interaction',
            rule: 'isAdmin'
          },
          {
            url: 'ManageInBasket',
            icon: 'radio_button_unchecked',
            i18n: 'menu.manage_in_basket_in_tray',
            rule: 'isAdmin'
          },
          {
            url: 'ManageProblemAnalysis',
            icon: 'radio_button_unchecked',
            i18n: 'menu.manage_problem_analysis',
            rule: 'isAdmin'
          },
          {
            url: 'ManageImprovementEnchantment',
            icon: 'radio_button_unchecked',
            i18n: 'menu.manage_improvement_enchantment',
            rule: 'isAdmin'
          },
          {
            url: 'ManageScheduling',
            icon: 'radio_button_unchecked',
            i18n: 'menu.manage_scheduling',
            rule: 'isAdmin'
          },
          {
            url: 'ManageProposalWriting',
            icon: 'radio_button_unchecked',
            i18n: 'menu.manage_proposal_writing',
            rule: 'isAdmin'
          },
          {
            url: 'ManageLeaderlessDiscussion',
            icon: 'radio_button_unchecked',
            i18n: 'menu.manage_leaderless_discussion',
            rule: 'isAdmin'
          }
        ]
      }
    ]
  },
  {
    url: 'MasterProject',
    icon: 'folder_open',
    i18n: 'menu.project',
    rule: 'isGeneral'
  }
];
