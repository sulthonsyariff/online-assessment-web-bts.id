export const loadingMixin = {
  methods: {
    closeLoading(el) {
      setTimeout(() => {
        this.$vs.loading.close(el);
      }, 1000);
    },

    loadingButton(el) {
      this.$vs.loading({
        background: 'primary',
        color: 'white',
        container: el,
        scale: 0.45
      });
    },

    loadingContent(el) {
      this.$vs.loading({
        container: el,
        scale: 0.45
      });
    },

    loadingPage(el, access) {
      if (access === 'login') {
        this.$vs.loading({
          background: 'primary',
          color: 'white',
          type: 'radius',
          container: el,
          scale: 2
        });
      } else {
        this.$vs.loading({
          background: 'transparent',
          type: 'border',
          color: '#1F74FF',
          container: el,
          scale: 1.5
        });
      }
    }
  }
};
