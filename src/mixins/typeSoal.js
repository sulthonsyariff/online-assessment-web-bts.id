export const setTypeSoalMixin = {
  methods: {
    setTypeSoal(soalType, flag) {
      let result;
      let stream;
      let template;
      let detail;
      let router;
      let answer;
      switch (soalType) {
        // Psycological Assessment
        case 'DISC':
          stream = 'disc';
          template = 'Disc';
          detail = 'disc';
          router = 'DiscProyek';
          answer = 'jawaban-disc';
          break;
        case 'PAPIKOSTIK':
          stream = 'papi';
          template = 'Papikostik';
          detail = 'papikostik';
          router = 'PapiProyek';
          answer = 'jawaban-papikostik';
          break;
        case 'VALUE':
          stream = 'value';
          template = 'Value';
          detail = 'value';
          router = 'ValueProyek';
          answer = 'jawaban-value';
          break;
        case 'LS':
          stream = 'ls';
          template = 'Leadership Style';
          detail = 'ls';
          router = 'LsProyek';
          answer = 'jawaban-ls';
          break;
        case 'BELBIN':
          stream = 'teamstyle';
          template = 'Team Style';
          detail = 'belbin';
          router = 'BelbinProyek';
          answer = 'jawaban-teamstyle';
          break;
        case 'IST':
          stream = 'ist';
          template = 'Intelligence Structure Test';
          detail = 'ist';
          router = 'IstProyek';
          answer = 'jawaban-ist';
          break;
        case 'PF16':
          stream = '16pf';
          template = '16 PF';
          detail = '16pf';
          router = 'Pf16Proyek';
          answer = 'jawaban-16pf';
          break;
        // Assessment Center
        case 'IN_BASKET':
          stream = 'in-basket';
          template = 'IN - Basket / IN Tray';
          detail = 'inbasket-intray';
          router = 'InBasketProyek';
          answer = 'jawaban-ib';
          break;
        case 'IP_EP':
          stream = 'ep';
          template = 'Improvement / Enhancement';
          detail = 'improvement-enhancement';
          router = 'IpEpProyek';
          answer = 'jawaban-ep';
          break;
        case 'PROBLEM_ANALYSIS':
          stream = 'problem-analysis';
          template = 'Problem Analysis';
          detail = 'problem-analysis';
          router = 'ProblemAnalysisProyek';
          answer = 'jawaban-pa';
          break;
        case 'PROPOSAL_WRITING':
          stream = 'proposal-writing';
          template = 'Proposal Writing';
          detail = 'proposal-writing';
          router = 'ProposalWritingProyek';
          answer = 'jawaban-pw';
          break;
        case 'SCHEDULING':
          stream = 'scheduling';
          template = 'Scheduling';
          detail = 'scheduling';
          router = 'SchedulingProyek';
          answer = 'jawaban-schedulling';
          break;
        case 'CUSTOMER_INTERACTION':
          stream = 'customer-interaction';
          template = 'Customer Interaction';
          detail = 'customer-interaction';
          router = 'CustomerInteractionProyek';
          answer = 'jawaban-ci';
          break;
        case 'PEER_INTERACTION':
          stream = 'peer-interaction';
          template = 'Peer Interaction';
          detail = 'peer-interaction';
          router = 'PeerInteractionProyek';
          answer = 'jawaban-pi';
          break;
        case 'SOI':
          stream = 'soi';
          template = 'Sub Ordinate Interaction';
          detail = 'sub-ordinate-interaction';
          router = 'SoiProyek';
          answer = 'jawaban-soi';
          break;
        case 'LEADERLESS_GROUP_DISCUSSION':
          stream = 'lgd';
          template = 'Leaderless Group Discussion';
          detail = 'leaderless-group-discussion';
          router = 'LgdProyek';
          answer = 'jawaban-lgd';
          break;
      }

      switch (flag) {
        case 'stream':
          result = stream;
          break;
        case 'template':
          result = template;
          break;
        case 'detail':
          result = detail;
          break;
        case 'router':
          result = router;
          break;
        case 'answer':
          result = answer;
          break;
      }

      return result;
    }
  }
};
