export const filtersMixin = {
  filters: {
    timer: function(value) {
      let secs = value ? parseInt(value) : 0;

      let sec_num = parseInt(secs, 10);
      // let hours = Math.floor(sec_num / 3600);
      let minutes = Math.floor(sec_num / 60) % 60;
      let seconds = sec_num % 60;

      return [minutes, seconds].map((v) => (v < 10 ? '0' + v : v)).join(':');
    },

    converDatetime: function(value) {
      return value
        .substr(0, 10)
        .split('/')
        .reverse()
        .join('/')
        .replaceAll('/', '-');
    }
  }
};
