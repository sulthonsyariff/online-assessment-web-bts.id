export const stylingStatusMixin = {
  methods: {
    stylingStatus(status) {
      let result;
      switch (status) {
        case 'Disetujui':
          result = 'text-status-approved';
          break;
        case 'Menunggu':
          result = 'text-status-pending';
          break;
        default:
          break;
      }

      return result;
    }
  }
};
