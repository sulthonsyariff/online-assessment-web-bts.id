import constant from '../constants';

export const statusMixin = {
  methods: {
    status(param) {
      if (constant.STATUS[param]) {
        return constant.STATUS[param].name;
      }

      return '-';
    },
    colorStatus(param) {
      if (constant.STATUS[param]) {
        return constant.STATUS[param].color;
      }

      return 'text-black';
    }
  }
};
