export function alphabetic(value) {
  if (!value) return true;
  var regex = /^[a-zA-Z ]*$/;
  if (value.match(regex)) {
    return true;
  }
  return false;
}
