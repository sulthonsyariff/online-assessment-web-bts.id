export const inputNumberMixin = {
  methods: {
    keypressmonitor(event) {
      if (
        event.key == 'e' ||
        event.key == 'E' ||
        event.key == '-' ||
        event.key == '+'
      ) {
        event.preventDefault();
      }
    },

    onPaste(event) {
      event.preventDefault();
    }
  }
};
