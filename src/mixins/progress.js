export const progressMixin = {
  methods: {
    // count progress bar percent
    progress(total, limit) {
      let percent = limit / total;
      return percent * 100;
    }
  }
};
