import { mapGetters } from 'vuex';
import { find } from 'lodash';

import menuItems from '@/components/materi/assessment_center/menuItems';
import questionItems from '@/views/Materi/PsychologicalAssessment/questionItems';

export const navigateMixin = {
  computed: {
    ...mapGetters({
      role: 'auth/currentRoles'
    })
  },
  methods: {
    // in progress refactor
    navigateSelected(params, key) {
      let role = this.role[0].toLowerCase();

      if (role == 'super_admin' && params.status == 1) {
        this.$router.push({
          name: key.approve,
          params: {
            id:
              params.userInternalId ||
              params.perusahaanId ||
              params.pesertaId ||
              params.assessorId ||
              params.targetJobId
          }
        });
      } else {
        this.$router.push({
          name: key.detail,
          params: {
            id:
              params.userInternalId ||
              params.perusahaanId ||
              params.pesertaId ||
              params.assessorId ||
              params.targetJobId
          }
        });
      }
    },

    navigateSelectedByVersion(type, params, item) {
      this.$store.commit(
        'assessmentCenter/SET_APPROVED_STATUS',
        params.approved
      );

      let page = null;

      if (item === 'PSIKOTEST') {
        // type === code in psikotest
        page = find(questionItems, { code: type });
      }

      if (item === 'SIMULATION') {
        // type === type in simulation
        page = find(menuItems, { type: type });
      }

      if (
        params.approved === 0 ||
        params.approved === 1 ||
        params.approved === 2
      ) {
        this.$router.push({
          name: page.toApprove,
          params: {
            id: params.soalId,
            version: params.version
          }
        });
      } else {
        this.$router.push({
          name: page.toDetail,
          params: {
            id: params.soalId,
            version: params.version
          }
        });
      }
    }
  }
};
