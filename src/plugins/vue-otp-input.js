import Vue from 'vue';
import OTPInput from '@8bu/vue-otp-input';
import '@8bu/vue-otp-input/dist/vue-otp-input.css';

Vue.use(OTPInput);
