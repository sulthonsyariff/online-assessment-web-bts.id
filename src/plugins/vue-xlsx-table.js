import Vue from 'vue';
import vueXlsxTable from 'vue-xlsx-table';

Vue.use(vueXlsxTable, { rABS: false });
