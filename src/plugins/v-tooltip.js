import Vue from 'vue';
import VTooltip from 'v-tooltip';

Vue.use(VTooltip);

import '@/assets/css/tooltip.scss'; // Tailwind styles
