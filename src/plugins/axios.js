import Vue from 'vue';
import store from '../store';
import router from '../router';
import axios from 'axios';

let baseConfig = {
  baseURL:
    process.env.VUE_APP_BASE_URL !== undefined
      ? process.env.VUE_APP_BASE_URL
      : '//api.local/'
};

const _axios = axios.create(baseConfig);

_axios.interceptors.request.use(
  function(config) {
    if (store.getters['auth/isAuthenticated']) {
      config.headers = {
        Authorization: `Bearer ${store.getters['auth/token']}`
      };
    }

    return config;
  },
  function(error) {
    console.log(error);
    return Promise.reject(error);
  }
);

_axios.interceptors.response.use(
  (response) => {
    // Return a successful response back to the calling service
    return response;
  },
  (error) => {
    // Try request again with new token
    if (
      error.response.status === 401 &&
      ['4000', '4001'].includes(error.response.data.statusCode.toString()) &&
      !['/auth/refresh-token', '/auth/login'].includes(
        error.response.config.url
      )
    ) {
      return store
        .dispatch('auth/refreshToken')
        .then((data) => {
          // New request with new token
          const config = error.config;
          config.headers['Authorization'] = `Bearer ${data.accessToken}`;

          return new Promise((resolve, reject) => {
            axios
              .request(config)
              .then((response) => {
                resolve(response);
              })
              .catch((error) => {
                reject(error);
              });
          });
        })
        .catch((error) => {
          store.dispatch('auth/clearAppSession');
          router.push({ name: 'Login' });
          return new Promise((resolve, reject) => {
            reject(error);
          });
        });
    } else {
      // Return any error which is not due to authentication back to the calling service
      return new Promise((resolve, reject) => {
        reject(error);
      });
    }
  }
);

Plugin.install = function(Vue) {
  Vue.axios = _axios;
  window.axios = _axios;
  Object.defineProperties(Vue.prototype, {
    axios: {
      get() {
        return _axios;
      }
    },
    $axios: {
      get() {
        return _axios;
      }
    }
  });
};

Vue.use(Plugin);

export default Plugin;
