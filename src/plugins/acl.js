import Vue from 'vue';
import { AclInstaller, AclCreate, AclRule } from 'vue-acl';
import router from '../router';
import store from '../store'; // your vuex store

Vue.use(AclInstaller);

export default new AclCreate({
  initial: 'public',
  notfound: {
    path: '/error',
    forwardQueryParams: true
  },
  router,
  acceptLocalRules: true,
  globalRules: {
    isPublic: new AclRule('public').generate(),
    isSuperAdmin: new AclRule('super_admin').generate(),
    isAdminOnly: new AclRule('admin').generate(),
    isAdmin: new AclRule('admin').or('super_admin').generate(),
    isPeserta: new AclRule('peserta').generate(),
    isAsesor: new AclRule('asesor')
      .or('super_admin')
      .or('admin')
      .generate(),
    isAsesorOnly: new AclRule('asesor').generate(),
    isGeneral: new AclRule('public')
      .or('super_admin')
      .or('admin')
      .or('asesor')
      .or('peserta')
      .generate()
  },
  middleware: async (acl) => {
    if (store.getters['auth/currentRoles']) {
      if (store.getters['auth/currentRoles'].includes('SUPER_ADMIN')) {
        acl.change('super_admin');
        return;
      }

      if (store.getters['auth/currentRoles'].includes('ADMIN')) {
        acl.change('admin');
        return;
      }

      if (store.getters['auth/currentRoles'].includes('ASESOR')) {
        acl.change('asesor');
        return;
      }

      if (store.getters['auth/currentRoles'].includes('PESERTA')) {
        acl.change('peserta');
        return;
      }
    }

    acl.change('public');
  }
});
