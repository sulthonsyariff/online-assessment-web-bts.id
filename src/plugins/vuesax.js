import Vue from 'vue';
import Vuesax from 'vuesax';

import 'vuesax/dist/vuesax.css'; //Vuesax styles
import 'material-icons/iconfont/material-icons.css';

Vue.use(Vuesax, {
  theme: {
    colors: {
      alternative: '#ede4d4',
      primary: '#f1b102',
      secondary: '#44c4f1',
      'shades-black': '#232323',
      danger: '#e53e3e'
    }
  }
});
