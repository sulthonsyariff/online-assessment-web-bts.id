import Vue from 'vue';

import VuelidateErrorExtractor, { templates } from 'vuelidate-error-extractor';

Vue.use(VuelidateErrorExtractor, {
  template: templates.singleErrorExtractor.foundaton6,
  i18n: 'validations'
});
