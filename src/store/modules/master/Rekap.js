const namespaced = true;

const state = {
  items: []
};
// eslint-disable-next-line no-unused-vars
const getters = {
  items: (state) => state.items
};
// eslint-disable-next-line no-unused-vars
const mutations = {
  SET_ITEM: (state, payload) => {
    state.items = payload;
  }
};

const actions = {
  // eslint-disable-next-line no-unused-vars
  fetchProyek: ({ commit }, payload) => {
    return window.axios
      .get(`/proyek-rekap`)
      .then((response) => {
        commit('SET_ITEM', response.data.data);
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  // eslint-disable-next-line no-unused-vars
  fetchProyekDetail: ({ commit }, payload) => {
    return window.axios
      .get(`/proyek-rekap/proyek/${payload.id}`)
      .then((response) => {
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  // eslint-disable-next-line no-unused-vars
  saveRekap: ({ commit }, payload) => {
    return window.axios
      .post('/proyek-rekap', payload.data)
      .then((response) => {
        return response;
      })
      .catch((error) => {
        throw error;
      });
  },
  // eslint-disable-next-line no-unused-vars
  updateRekap: ({ commit }, payload) => {
    return window.axios
      .put(`/proyek-rekap/${payload.proyekRekapId}`, payload.data)
      .then((response) => {
        return response;
      })
      .catch((error) => {
        throw error;
      });
  },
  // eslint-disable-next-line no-unused-vars
  fetchFiles: ({ commit }, payload) => {
    return window.axios
      .get(`/proyek-rekap/file/download/${payload.fileUrl}`, {
        params: {
          type: payload.type
        },
        responseType: 'arraybuffer'
      })
      .then((response) => {
        return response;
      })
      .catch((error) => {
        throw error;
      });
  },
  // eslint-disable-next-line no-unused-vars
  downloadRekap: ({ commit }, payload) => {
    return window.axios
      .get(`/proyek-rekap/download/${payload.proyekId}`, {
        responseType: 'arraybuffer'
      })
      .then((response) => {
        return response;
      })
      .catch((error) => {
        throw error;
      });
  }
};

export { namespaced, state, getters, mutations, actions };
