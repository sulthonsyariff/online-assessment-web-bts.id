import qs from 'querystring';

const namespaced = true;

const state = {
  items: []
};

const getters = {
  items: (state) => state.items
};

const mutations = {
  SET_ITEM: (state, payload) => {
    state.items = payload;
  }
};

const actions = {
  fetchList: ({ commit, rootGetters }, payload) => {
    let defaultPageable = rootGetters['table/defaultPageable'];
    let queryParams = {
      limit: defaultPageable.limit,
      page: defaultPageable.currentPage - 1,
      search: defaultPageable.search,
      roleId: payload.roleId,
      orderBy: payload.orderBy
    };

    return window.axios
      .get('/user-internal', {
        params: queryParams,
        paramsSerializer: function(params) {
          return qs.stringify(params);
        }
      })
      .then((response) => {
        let data = response.data.data;

        commit('SET_ITEM', data.content);
        commit('table/SET_DEFAULT_TOTAL_PAGES', data.totalPages, {
          root: true
        });
        commit('table/SET_DEFAULT_TOTAL_ELEMENTS', data.totalElements, {
          root: true
        });

        return data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchSave: ({ commit }, payload) => {
    let data = payload.data;

    return window
      .axios({
        method: 'post',
        url: '/user-internal',
        data: data
      })
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchUpdate: ({ commit }, payload) => {
    let data = payload.data;

    return window
      .axios({
        method: 'put',
        url: `/user-internal/${payload.id}`,
        data: data
      })
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchApprove: ({ commit }, payload) => {
    let data = payload.data;

    return window
      .axios({
        method: 'put',
        url: `/user-internal/approve/${payload.id}`,
        data: data
      })
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchDetail: ({ commit }, payload) => {
    return window.axios
      .get(`/user-internal/${payload.id}`)
      .then((response) => {
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchReview: ({ commit }, payload) => {
    return window.axios
      .get(`/user-internal/edit-preview/${payload.id}`)
      .then((response) => {
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchDelete: ({ commit }, payload) => {
    return window.axios
      .delete(`/user-internal/${payload.id}`)
      .then((response) => {
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  }
};

export { namespaced, state, getters, mutations, actions };
