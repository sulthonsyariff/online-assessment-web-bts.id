const namespaced = true;

const state = {
  items: []
};

const getters = {
  items: (state) => state.items,
  itemsApproved: (state) =>
    state.items.filter((item) => {
      if (item.status == 3) return item;
    })
};

const mutations = {
  SET_ITEM: (state, payload) => {
    state.items = payload;
  }
};

const actions = {
  fetchList: ({ commit }) => {
    return window.axios
      .get('/targetjob')
      .then((response) => {
        let data = response.data.data;

        commit('SET_ITEM', data);
        return data;
      })
      .catch((error) => {
        throw error;
      });
  },

  fetchListApproved: ({ commit }) => {
    return window.axios
      .get('/targetjob/approved-targetjob')
      .then((response) => {
        let data = response.data.data;

        commit('SET_ITEM', data);
        return data;
      })
      .catch((error) => {
        throw error;
      });
  },

  fetchListLevel: ({ commit }, payload) => {
    return window.axios
      .get(`/targetjob/target-level/${payload.targetJobId}`)
      .then((response) => {
        let data = response.data.data;

        commit('SET_ITEM', data);
        return data;
      })
      .catch((error) => {
        throw error;
      });
  },

  fetchListCompany: ({ commit }, payload) => {
    return window.axios
      .get(`/targetjob/targetjobperusahaan/${payload.perusahaanId}`)
      .then((response) => {
        let data = response.data.data;

        commit('SET_ITEM', data);
        return data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchSave: ({ commit }, payload) => {
    let parsingData = payload.data;
    delete parsingData.targetJobId;

    let data = [];
    data.push(parsingData);

    return window
      .axios({
        method: 'post',
        url: `/targetjob/${payload.perusahaanId}`,
        data: data
      })
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchUpdate: ({ commit }, payload) => {
    let data = payload.data;

    return window
      .axios({
        method: 'put',
        url: `/targetjob/${payload.id}`,
        data: data
      })
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchApprove: ({ commit }, payload) => {
    let data = payload.data;

    return window
      .axios({
        method: 'put',
        url: `/targetjob/approved/${payload.id}`,
        data: data
      })
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchDetail: ({ commit }, payload) => {
    return window.axios
      .get(`/targetjob/${payload.id}`)
      .then((response) => {
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchDelete: ({ commit }, payload) => {
    return window.axios
      .delete(`/targetjob/${payload.id}`)
      .then((response) => {
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  }
};

export { namespaced, state, getters, mutations, actions };
