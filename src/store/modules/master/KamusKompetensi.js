const namespaced = true;

const state = {
  items: []
};

const getters = {
  items: (state) => state.items
};

const mutations = {
  SET_ITEM: (state, payload) => {
    state.items = payload;
  }
};

const actions = {
  // eslint-disable-next-line no-unused-vars
  fetchSaveKamus: ({ commit }, payload) => {
    let data = payload.data;

    return window
      .axios({
        method: 'post',
        url: `/kamuskompetensi/${payload.targetJobId}`,
        data: data
      })
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchDeleteKamusKompetensi: ({ commit }, payload) => {
    return window
      .axios({
        method: 'delete',
        url: `/kamuskompetensi/${payload.kamusKompetensiId}`
      })
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchSaveKompetensi: ({ commit }, payload) => {
    let data = payload.data;

    return window
      .axios({
        method: 'post',
        url: `/kompetensi/${payload.targetJobId}/${payload.kamusKompetensiId}`,
        data: data
      })
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchDeleteKompetensi: ({ commit }, payload) => {
    return window
      .axios({
        method: 'delete',
        url: `/kompetensi/${payload.kamusKompetensiId}`
      })
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchSaveKriteriaPenilaian: ({ commit }, payload) => {
    let data = payload.data;

    return window
      .axios({
        method: 'post',
        url: `/kriteriapenilaian/${payload.targetJobId}/${payload.kamusKompetensiId}`,
        data: data
      })
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchDeleteKriteriaPenilaian: ({ commit }, payload) => {
    return window
      .axios({
        method: 'delete',
        url: `/kriteriapenilaian/${payload.kamusKompetensiId}`
      })
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchSaveKriteriaRekomendasi: ({ commit }, payload) => {
    let data = payload.data;

    return window
      .axios({
        method: 'post',
        url: `/kriteriarekomendasi/${payload.targetJobId}/${payload.kamusKompetensiId}`,
        data: data
      })
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchDeleteKriteriaRekomendasi: ({ commit }, payload) => {
    return window
      .axios({
        method: 'delete',
        url: `/kriteriarekomendasi/${payload.kamusKompetensiId}`
      })
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchUpdateStatus: ({ commit }, payload) => {
    let data = payload.data;

    return window
      .axios({
        method: 'put',
        url: `/kamuskompetensi/update/${payload.targetJobId}`,
        data: data
      })
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        throw error;
      });
  }
};

export { namespaced, state, getters, mutations, actions };
