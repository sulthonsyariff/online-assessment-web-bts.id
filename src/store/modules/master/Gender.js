const namespaced = true;

const state = {
  items: []
};

const getters = {
  items: (state) => state.items
};

const mutations = {
  SET_ITEM: (state, payload) => {
    state.items = payload;
  }
};

const actions = {
  fetchList: ({ commit }) => {
    let data = [
      {
        id: 1,
        name: 'Pria'
      },
      {
        id: 2,
        name: 'Wanita'
      }
    ];
    commit('SET_ITEM', data);
  }
};

export { namespaced, state, getters, mutations, actions };
