import qs from 'querystring';

const namespaced = true;

const state = {
  items: [],
  detailAssessment: [],
  asesi: null,
  jawaban: [],
  usia: null,
  normaPendidikan: null,
  istReport: false,
  pfType: 'stenine',
  normaPekerjaan: null,
  detailProyek: null,
  infoAsesi: {},
  destroyFunc: false,
  destroySubtestProgress: false
};

const getters = {
  items: (state) => state.items,
  detailAssessment: (state) => state.detailAssessment,
  jawaban: (state) => state.jawaban,
  usia: (state) => state.usia,
  normaPendidikan: (state) => state.normaPendidikan,
  asesi: (state) => state.asesi,
  istReport: (state) => state.istReport,
  pfType: (state) => state.pfType,
  normaPekerjaan: (state) => state.normaPekerjaan,
  detailProyek: (state) => state.detailProyek,
  infoAsesi: (state) => state.infoAsesi,
  destroyFunc: (state) => state.destroyFunc,
  destroySubtestProgress: (state) => state.destroySubtestProgress
};

const mutations = {
  SET_DESTROY_SUBTEST_PROGRESS: (state, payload) => {
    state.destroySubtestProgress = payload.flag;
  },

  SET_DESTROY_FLAG: (state, payload) => {
    state.destroyFunc = payload.flag;
  },

  SET_TYPE_PF: (state, payload) => {
    state.pfType = payload;
  },

  SET_ITEM: (state, payload) => {
    state.items = payload;
  },

  SET_DATA_ASSESSMENT: (state, payload) => {
    state.detailAssessment = payload;
  },

  SET_JAWABAN: (state, payload) => {
    state.jawaban = payload;
  },

  SET_ASESI: (state, payload) => {
    state.asesi = payload;
  },

  SET_IST_REPORT: (state, payload) => {
    state.istReport = payload;
  },

  UPDATE_USIA: (state, payload) => {
    state.usia = payload;
  },

  UPDATE_NORMA_PENDIDIKAN: (state, payload) => {
    state.normaPendidikan = payload;
  },

  DETAIL_PROYEK: (state, payload) => {
    state.detailProyek = payload;
  },

  SET_INFO_ASESI: (state, payload) => {
    state.infoAsesi = payload;
  },

  SET_NORMA_PEKERJAAN: (state, payload) => {
    state.normaPekerjaan = payload;
  }
};

const actions = {
  fetchList: ({ commit, rootGetters }, payload) => {
    let defaultPageable = rootGetters['table/defaultPageable'];
    let queryParams = {
      ...payload,
      limit: defaultPageable.limit,
      page: defaultPageable.currentPage - 1,
      search: defaultPageable.search
    };

    let endPoint = payload.role === 'ASESOR' ? '/proyekAssessor' : '/proyek';

    return window.axios
      .get(endPoint, {
        params: queryParams,
        paramsSerializer: function(params) {
          return qs.stringify(params);
        }
      })
      .then((response) => {
        let data = response.data.data;

        commit('SET_ITEM', data.content);
        commit('table/SET_DEFAULT_TOTAL_PAGES', data.totalPages, {
          root: true
        });
        commit('table/SET_DEFAULT_TOTAL_ELEMENTS', data.totalElements, {
          root: true
        });

        return data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchSave: ({ commit }, payload) => {
    let data = payload.data;

    return window
      .axios({
        method: 'post',
        url: '/proyek',
        data: data
      })
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchReview: ({ commit }, payload) => {
    return window.axios
      .get(`/proyek/edit-preview/${payload.id}`)
      .then((response) => {
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchUpdate: ({ commit }, payload) => {
    let data = payload.data;

    return window
      .axios({
        method: 'put',
        url: `/proyek/${payload.id}`,
        data: data
      })
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchApprove: ({ commit }, payload) => {
    let data = payload.data;

    return window
      .axios({
        method: 'put',
        url: `/proyek/approve/${payload.id}`,
        data: data
      })
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchDetail: ({ commit }, payload) => {
    return window.axios
      .get(`/proyek/${payload.id}`)
      .then((response) => {
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchDelete: ({ commit }, payload) => {
    return window.axios
      .delete(`/proyek/${payload.id}`)
      .then((response) => {
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchTemplate: ({ commit }, payload) => {
    return window.axios
      .get('/proyek/soal-type', {
        params: payload,
        paramsSerializer: function(params) {
          return qs.stringify(params);
        }
      })
      .then((response) => {
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchNamaMateri: ({ commit }, payload) => {
    return window.axios
      .get('/proyek/soal', {
        params: payload,
        paramsSerializer: function(params) {
          return qs.stringify(params);
        }
      })
      .then((response) => {
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchProyekAsesi: ({ commit }, payload) => {
    return window.axios
      .get(`/proyek-asesi/${payload.proyekPesertaId}`, {
        paramsSerializer: function(params) {
          return qs.stringify(params);
        }
      })
      .then((response) => {
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchProyekAssessment: ({ commit }, payload) => {
    return window.axios
      .get(`/proyek-asesi/assement/${payload.proyekPesertaId}`, {
        paramsSerializer: function(params) {
          return qs.stringify(params);
        }
      })
      .then((response) => {
        commit('SET_DATA_ASSESSMENT', response.data.data);
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchProyekAssessor: ({ commit }, payload) => {
    return window.axios
      .get('/assessor/approved-assessor', {
        params: payload,
        paramsSerializer: function(params) {
          return qs.stringify(params);
        }
      })
      .then((response) => {
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchBeritaAcara: ({ commit }, payload) => {
    return window.axios
      .get(`/proyek/${payload.proyekId}/berita-acara`, {
        paramsSerializer: function(params) {
          return qs.stringify(params);
        }
      })
      .then((response) => {
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  deletePesertaKelompok: ({ commit }, payload) => {
    return window.axios
      .delete(`/proyek/${payload.id}/berita-acara`)
      .then((response) => {
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchPerusahaanApprove: ({ commit }, payload) => {
    return window.axios
      .get('/perusahaan/approve-perusahaan', {})
      .then((response) => {
        let data = response.data.data;

        return data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  approvedTargetJob: ({ commit }, payload) => {
    return window.axios
      .get(`/targetjob/approved-targetjob/${payload.id}`)
      .then((response) => {
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  tambahWaktu: ({ commit }, payload) => {
    let data = payload.data;

    return window
      .axios({
        method: 'put',
        url: `/proyek-asesi/mongo/tambah-waktu/${payload.proyekPesertaId}/proyek-soal/${payload.proyekSoalId}`,
        data: data
      })
      .then((response) => {
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  klasifikasiAssessor: ({ commit }, payload) => {
    let data = payload.data;

    return window
      .axios({
        method: 'put',
        url: `/proyek-asesi/report-simulasi/${payload.proyekPesertaId}/proyek-result/${payload.proyekResultId}`,
        data: data
      })
      .then((response) => {
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchLaporan: ({ commit }, payload) => {
    return window
      .axios({
        method: 'get',
        url: `/proyek/laporan-proyek/${payload.id}`
      })
      .then((response) => {
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  updateLaporan: ({ commit }, payload) => {
    let data = payload.data;

    return window
      .axios({
        method: 'put',
        url: `/proyek/laporan-proyek/${payload.id}`,
        data: data
      })
      .then((response) => {
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  detailProjectForUpdate: ({ commit }, payload) => {
    return window
      .axios({
        method: 'get',
        url: `/proyek/detail-proyek/${payload.id}`
      })
      .then((response) => {
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchReportInformasiAsesi: ({ commit }, payload) => {
    return window.axios
      .get(`/proyek-asesi/report-informasi-asesi/${payload.id}`)
      .then((response) => {
        commit('SET_ASESI', response.data.data);
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchReportBelbin: ({ commit }, payload) => {
    return window.axios
      .get(`/proyek-asesi/report-belbin/${payload.id}`)
      .then((response) => {
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchReportLs: ({ commit }, payload) => {
    return window.axios
      .get(`/proyek-asesi/report-ls/${payload.id}`)
      .then((response) => {
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchReportValue: ({ commit }, payload) => {
    return window.axios
      .get(`/proyek-asesi/report-value/${payload.id}`)
      .then((response) => {
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchReportPapikostik: ({ commit }, payload) => {
    return window.axios
      .get(`/proyek-asesi/report-papikostik/${payload.id}`)
      .then((response) => {
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchReportDisc: ({ commit }, payload) => {
    return window.axios
      .get(`/proyek-asesi/report-disc/${payload.id}`)
      .then((response) => {
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchReportIst: ({ commit }, payload) => {
    return window.axios
      .get(`/ist/raw-score-ist/${payload.id}`)
      .then((response) => {
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  fetchSaveReportIst: ({ commit }, payload) => {
    return window
      .axios({
        method: 'post',
        url: `/ist/raw-score-ist/${payload.id}`,
        data: payload.payload
      })
      .then((response) => {
        commit('UPDATE_USIA', null);
        commit('SET_IST_REPORT', true);
        return response.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchReportIstResult: ({ commit, state }, payload) => {
    let filter = {
      proyekResultId: payload.id,
      normaPekerjaan: state.normaPekerjaan
    };

    return window.axios
      .get(`/ist/raw-score-ist/result`, {
        params: filter,
        paramsSerializer: function(params) {
          return qs.stringify(params);
        }
      })
      .then((response) => {
        if (response.data.data) {
          commit('SET_IST_REPORT', true);
        } else {
          commit('SET_IST_REPORT', false);
        }

        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchReport16pf: ({ commit }, payload) => {
    return window.axios
      .get('/proyek-asesi/report-16pf', {
        params: payload,
        paramsSerializer: function(params) {
          return qs.stringify(params);
        }
      })
      .then((response) => {
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  reportDownload: ({ commit }, payload) => {
    return window.axios
      .get(
        `/proyekreport/${payload.proyekResultId}/download/${payload.assessment}`
      )
      .then((response) => {
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchFilesJawaban: ({ commit }, payload) => {
    return window.axios
      .get(`/proyekresult/download/${payload.fileUrl}`, {
        params: {
          type: payload.type
        },
        responseType: 'arraybuffer'
      })
      .then((response) => {
        return response;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  downloadBeritaAcara: ({ commit }, payload) => {
    return window.axios
      .get(`/proyekreport/${payload.id}/download/beritaacara`, {
        responseType: 'arraybuffer'
      })
      .then((response) => {
        return response;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  downloadFilesInBasket: ({ commit }, payload) => {
    return window.axios
      .get(`/proyekresult/download/fileInbasket/${payload.fileUrl}`, {
        params: {
          type: payload.type
        },
        responseType: 'arraybuffer'
      })
      .then((response) => {
        return response;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  currentTestAsesi: ({ commit }, payload) => {
    return window.axios
      .get(
        `/proyek/peserta-current-test/${payload.proyekId}/${payload.proyekPesertaId}`
      )
      .then((response) => {
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchProgress: ({ commit }, payload) => {
    return window.axios
      .get(`/proyek-asesi/stream/${payload.soalType}`, {
        params: payload.params,
        paramsSerializer: function(params) {
          return qs.stringify(params);
        }
      })
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchPictures: ({ commit }, payload) => {
    return window.axios
      .get(`/proyek-asesi/stream/picture`, {
        params: payload,
        paramsSerializer: function(params) {
          return qs.stringify(params);
        }
      })
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  getPicturesCompleted: ({ commit }, payload) => {
    return window.axios
      .get(`/proyek-asesi/picture`, {
        params: payload,
        paramsSerializer: function(params) {
          return qs.stringify(params);
        }
      })
      .then((response) => {
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchPicturesCompleted: ({ commit }, payload) => {
    return window.axios
      .get(`/proyek-asesi/download/${payload.imageUrl}`, {
        responseType: 'arraybuffer'
      })
      .then((response) => {
        if (payload.type === 'get') {
          return (
            `data:${response.headers['content-type']};base64,` +
            Buffer.from(response.data, 'binary').toString('base64')
          );
        } else {
          return response;
        }
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  getAnswer: ({ commit }, payload) => {
    return window.axios
      .get(
        `/proyek-asesi/${payload.typeSoal}/${payload.proyekSoalId}/${payload.proyekPesertaId}`
      )
      .then((response) => {
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  }
};

export { namespaced, state, getters, mutations, actions };
