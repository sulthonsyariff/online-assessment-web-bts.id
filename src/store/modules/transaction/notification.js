const namespaced = true;

const state = {};

const getters = {};

const mutations = {};

const actions = {
  // eslint-disable-next-line no-unused-vars
  readNotification: ({ commit }, payload) => {
    return window
      .axios({
        method: 'put',
        url: `/notification/${payload.id}`,
        data: {}
      })
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        throw error;
      });
  }
};

export { namespaced, state, getters, mutations, actions };
