const namespaced = true;

const state = {};

const getters = {};

const mutations = {};

const actions = {
  // eslint-disable-next-line no-unused-vars
  saveImage: ({ commit }, payload) => {
    let formData = new FormData();
    formData.append('image', payload.file);

    return window
      .axios({
        method: 'post',
        url: '/image-store/image',
        data: formData
      })
      .then((response) => {
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  }
};

export { namespaced, state, getters, mutations, actions };
