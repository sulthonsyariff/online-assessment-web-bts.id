import qs from 'querystring';

const namespaced = true;

const state = {
  channelId: []
};

const getters = {
  items: (state) => state.items
};

const mutations = {
  SET_CHANNEL_ID: (state, payload) => {
    state.channelId = payload;
  }
};

const actions = {
  fetchChatChannel: ({ commit }, payload) => {
    return window.axios
      .get('/chat/channel', {
        params: payload,
        paramsSerializer: function(params) {
          return qs.stringify(params);
        }
      })
      .then((response) => {
        let data = response.data.data;

        commit('SET_CHANNEL_ID', data);
        return data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  sendMessage: ({ commit }, payload) => {
    return window
      .axios({
        method: 'post',
        url: '/chat',
        data: payload
      })
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        throw error;
      });
  }
};

export { namespaced, state, getters, mutations, actions };
