const jwtDecode = require('jwt-decode');
import { includes } from 'lodash';

const namespaced = true;

const state = {
  refreshToken: '',
  stepFirsTime: false,
  emailOtp: '',
  stepOtp: false,
  otpTime: 0,
  otpDuration: 0,
  token: '',
  ticket: ''
};

const getters = {
  currentEmail: (state) => {
    if (!state.token) {
      return;
    }

    let decoded = jwtDecode(state.token);
    return decoded.sub;
  },
  currentRoles: (state) => {
    if (!state.token) {
      return;
    }

    let decoded = jwtDecode(state.token);
    return decoded.roles;
  },
  isAuthenticated: (state) => !!state.token,
  stepFirsTime: (state) => state.stepFirsTime,
  stepOtp: (state) => state.stepOtp,
  emailOtp: (state) => state.emailOtp,
  otpTime: (state) => state.otpTime,
  otpDuration: (state) => state.otpDuration,
  ticket: (state) => state.ticket,
  token: (state) => state.token
};

const mutations = {
  SET_TOKEN: (state, payload) => {
    state.token = payload;
  },

  SET_REFRESH_TOKEN: (state, payload) => {
    state.refreshToken = payload;
  },

  SET_TICKET: (state, payload) => {
    state.ticket = payload;
  },

  SET_STEP_FIRST_TIME: (state, payload) => {
    state.stepFirsTime = payload;
  },

  SET_STEP_OTP: (state, payload) => {
    state.stepOtp = payload;
  },

  SET_EMAIL: (state, payload) => {
    state.emailOtp = payload;
  },

  SET_OTP_TIME: (state, payload) => {
    state.otpTime = payload;
  },

  SET_OTP_DURATION: (state, payload) => {
    state.otpDuration = payload;
  }
};

const actions = {
  login: ({ commit }, payload) => {
    return window.axios
      .post('/auth/login', payload)
      .then((response) => {
        let data = response.data.data;
        if (response.data.statusCode === 2002) {
          commit('SET_STEP_OTP', true);
          commit('SET_OTP_TIME', new Date());
          commit('SET_OTP_DURATION', data.durasi);
          commit('SET_EMAIL', payload.email);
        } else if (response.data.statusCode === 2003) {
          commit('SET_TOKEN', data.accessToken);
          commit('SET_REFRESH_TOKEN', data.refreshToken);
          commit('SET_TICKET', data.ticket);
          commit('SET_STEP_FIRST_TIME', true);
        } else {
          commit('SET_TOKEN', data.accessToken);
          commit('SET_REFRESH_TOKEN', data.refreshToken);
          commit('SET_TICKET', '');
          commit('SET_STEP_FIRST_TIME', false);
          commit('SET_STEP_OTP', false);
          commit('SET_EMAIL', '');
          commit('SET_OTP_TIME', 0);
          commit('SET_OTP_DURATION', 0);
        }

        return data;
      })
      .catch((error) => {
        throw error;
      });
  },

  otpVerification: ({ commit }, payload) => {
    return window.axios
      .post('/auth/otp', payload)
      .then((response) => {
        let data = response.data.data;
        if (response.data.statusCode === 2003) {
          commit('SET_TICKET', data.ticket);
          commit('SET_STEP_FIRST_TIME', true);
        }

        commit('SET_TOKEN', data.accessToken);
        commit('SET_REFRESH_TOKEN', data.refreshToken);
        commit('SET_STEP_OTP', false);
        commit('SET_EMAIL', '');
        commit('SET_OTP_TIME', 0);
        commit('SET_OTP_DURATION', 0);

        return data;
      })
      .catch((error) => {
        throw error;
      });
  },

  logout: ({ dispatch }) => {
    return window.axios
      .post('/auth/logout')
      .then(() => {
        return dispatch('clearAppSession');
      })
      .catch((error) => {
        throw error;
      });
  },

  clearAppSession: ({ commit }) => {
    commit('SET_TOKEN', '');
    commit('SET_REFRESH_TOKEN', '');
    commit('SET_TICKET', '');
    commit('SET_STEP_FIRST_TIME', false);
    commit('SET_STEP_OTP', false);
    commit('SET_EMAIL', '');
    commit('SET_OTP_TIME', 0);
    commit('SET_OTP_DURATION', 0);
  },

  refreshToken: ({ state, commit }) => {
    if (!state.refreshToken) {
      return;
    }

    let payload = {
      refreshToken: state.refreshToken
    };

    return window.axios
      .post('/auth/refresh-token', payload)
      .then((response) => {
        let data = response.data.data;

        commit('SET_TOKEN', data.accessToken);
        return data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  forgotPassword: ({ commit }, payload) => {
    return window.axios
      .post('/auth/reset-password', payload)
      .then((response) => {
        let data = response.data.data;

        return data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  resetPassword: ({ commit }, payload) => {
    return window.axios
      .put('/auth/reset-password', payload)
      .then((response) => {
        let data = response.data.data;

        commit('SET_TICKET', '');
        commit('SET_STEP_FIRST_TIME', false);
        return data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  checkPermission: ({ commit }, payload) => {
    if (includes(payload.roles, 'SUPER_ADMIN')) {
      return 1;
    } else if (includes(payload.roles, 'ADMIN')) {
      return 2;
    } else {
      return 3;
    }
  }
};

export { namespaced, state, getters, mutations, actions };
