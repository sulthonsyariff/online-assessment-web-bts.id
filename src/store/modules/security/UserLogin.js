import qs from 'querystring';

const namespaced = true;

const state = {
  items: []
};

const getters = {
  items: (state) => state.items
};

const mutations = {
  SET_ITEM: (state, payload) => {
    state.items = payload;
  }
};

const actions = {
  fetchList: ({ commit, rootGetters }, payload) => {
    let defaultPageable = rootGetters['table/defaultPageable'];
    let queryParams = {
      limit: defaultPageable.limit,
      page: defaultPageable.currentPage - 1,
      search: defaultPageable.search,
      roleId: payload.roleId,
      perusahaanId: payload.perusahaanId,
      orderBy: payload.orderBy
    };

    return window.axios
      .get('/user-login', {
        params: queryParams,
        paramsSerializer: function(params) {
          return qs.stringify(params);
        }
      })
      .then((response) => {
        let data = response.data.data;

        commit('table/SET_DEFAULT_TOTAL_PAGES', data.totalPages, {
          root: true
        });
        commit('table/SET_DEFAULT_TOTAL_ELEMENTS', data.totalElements, {
          root: true
        });

        commit('SET_ITEM', data.content);
        return data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchLogout: ({ commit }, payload) => {
    return window
      .axios({
        method: 'put',
        url: `/user-login/force-logout/${payload.id}`,
        data: {}
      })
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        throw error;
      });
  }
};

export { namespaced, state, getters, mutations, actions };
