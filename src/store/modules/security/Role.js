const namespaced = true;

const state = {
  items: []
};

const getters = {
  items: (state) => state.items
};

const mutations = {
  SET_ITEM: (state, payload) => {
    state.items = payload;
  }
};

const actions = {
  fetchSearchList: ({ commit }, payload) => {
    let action = '/role';

    if (payload) action = payload;

    return window.axios
      .get(action)
      .then((response) => {
        let data = response.data.data;

        commit('SET_ITEM', data);
        return data;
      })
      .catch((error) => {
        throw error;
      });
  }
};

export { namespaced, state, getters, mutations, actions };
