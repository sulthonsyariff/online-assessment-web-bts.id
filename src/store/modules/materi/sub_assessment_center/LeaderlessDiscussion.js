const namespaced = true;

const state = {
  items: []
};

const getters = {
  items: (state) => state.items
};

const mutations = {
  SET_ITEM: (state, payload) => {
    state.items = payload;
  }
};

const actions = {
  // eslint-disable-next-line no-unused-vars
  fetchSave: ({ commit }, payload) => {
    let data = payload.data;

    return window
      .axios({
        method: 'post',
        url: '/leaderless-group-discussion',
        data: data
      })
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchReview: ({ commit }, payload) => {
    return window.axios
      .get(
        `/leaderless-group-discussion/edit-preview/${payload.id}/version/${payload.version}`
      )
      .then((response) => {
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchUpdate: ({ commit }, payload) => {
    let data = payload.data;

    return window
      .axios({
        method: 'put',
        url: `/leaderless-group-discussion/${payload.id}/version/${payload.version}`,
        data: data
      })
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchApprove: ({ commit }, payload) => {
    let data = payload.data;

    return window
      .axios({
        method: 'put',
        url: `/leaderless-group-discussion/approve/${payload.id}/version/${payload.version}`,
        data: data
      })
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchDetail: ({ commit }, payload) => {
    return window.axios
      .get(
        `/leaderless-group-discussion/${payload.id}/version/${payload.version}`
      )
      .then((response) => {
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchDetailProyek: ({ commit }, payload) => {
    return window.axios
      .get(
        `/leaderless-group-discussion/instruksi-peran/${payload.id}/version/${payload.version}`
      )
      .then((response) => {
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchDelete: ({ commit }, payload) => {
    return window.axios
      .delete(
        `/leaderless-group-discussion/${payload.id}/version/${payload.version}`
      )
      .then((response) => {
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  }
};

export { namespaced, state, getters, mutations, actions };
