const namespaced = true;

const state = {
  items: []
};

const getters = {
  items: (state) => state.items
};

const mutations = {
  SET_ITEM: (state, payload) => {
    state.items = payload;
  }
};

const actions = {
  // eslint-disable-next-line no-unused-vars
  fetchSave: ({ commit }, payload) => {
    let data = payload.data;

    return window
      .axios({
        method: 'post',
        url: '/proposal-writing',
        data: data
      })
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchReview: ({ commit }, payload) => {
    return window.axios
      .get(
        `/proposal-writing/edit-preview/${payload.id}/version/${payload.version}`
      )
      .then((response) => {
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchUpdate: ({ commit }, payload) => {
    let data = payload.data;

    return window
      .axios({
        method: 'put',
        url: `/proposal-writing/${payload.id}/version/${payload.version}`,
        data: data
      })
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchApprove: ({ commit }, payload) => {
    let data = payload.data;

    return window
      .axios({
        method: 'put',
        url: `/proposal-writing/approve/${payload.id}/version/${payload.version}`,
        data: data
      })
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchDetail: ({ commit }, payload) => {
    return window.axios
      .get(`/proposal-writing/${payload.id}/version/${payload.version}`)
      .then((response) => {
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchDelete: ({ commit }, payload) => {
    return window.axios
      .delete(`/proposal-writing/${payload.id}/version/${payload.version}`)
      .then((response) => {
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  }
};

export { namespaced, state, getters, mutations, actions };
