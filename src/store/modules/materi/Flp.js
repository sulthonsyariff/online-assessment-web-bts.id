// eslint-disable-next-line no-unused-vars
import qs from 'querystring';

const namespaced = true;

const state = {
  item: null
};

const getters = {
  item: (state) => state.item
};

const mutations = {
  SET_ITEM: (state, payload) => {
    state.item = payload;
  }
};

const actions = {
  // eslint-disable-next-line no-unused-vars
  fetchList: ({ commit, rootGetters }, payload) => {
    let queryParams = {
      soalType: payload.soalType
    };

    return window.axios
      .get('/flp', {
        params: queryParams,
        paramsSerializer: function(params) {
          return qs.stringify(params);
        }
      })
      .then((response) => {
        let data = response.data.data;
        commit('SET_ITEM', data);
        return data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchSave: ({ commit }, payload) => {
    let data = payload.data;

    return window
      .axios({
        method: 'post',
        url: '/flp',
        data: data
      })
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchReview: ({ commit }, payload) => {
    return window.axios
      .get(`/flp/edit-preview/${payload.id}/version/${payload.version}`)
      .then((response) => {
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchUpdate: ({ commit }, payload) => {
    let data = payload.data;

    return window
      .axios({
        method: 'put',
        url: `/flp/${payload.id}/version/${payload.version}`,
        data: data
      })
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchApprove: ({ commit }, payload) => {
    let data = payload.data;

    return window
      .axios({
        method: 'put',
        url: `/flp/approve/${payload.id}/version/${payload.version}`,
        data: data
      })
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchDetail: ({ commit }, payload) => {
    return window.axios
      .get(`/flp/${payload.id}/version/${payload.version}`)
      .then((response) => {
        let data = response.data.data;

        commit('SET_ITEM', data);
        return data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchDelete: ({ commit }, payload) => {
    return window.axios
      .delete(`/flp/${payload.id}/version/${payload.version}`)
      .then((response) => {
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  }
};

export { namespaced, state, getters, mutations, actions };
