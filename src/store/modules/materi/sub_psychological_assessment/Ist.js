import constant from '../../../../constants.js';

const namespaced = true;

const state = {
  items: null,
  isInitialized: false,
  isExisted: false,
  id: null
};

const getters = {
  items: (state) => state.items,
  isInitialized: (state) => state.isInitialized,
  isExisted: (state) => state.isExisted,
  id: (state) => state.id
};

const mutations = {
  SET_ITEM: (state, payload) => {
    state.items = payload;
  },

  UPDATE_SUBTEST: (state, payload) => {
    state.items.soal[payload.id] = payload.data;
  },

  UPDATE_NAME: (state, payload) => {
    state.items.soalName = payload.soalName;
  },

  UPDATE_DURATION: (state, payload) => {
    state.items.durasiPengerjaan = payload.durasiPengerjaan;
  },

  UPDATE_INITIALIZED: (state, payload) => {
    state.isInitialized = payload.status;
  },

  UPDATE_EXISTED: (state, payload) => {
    state.isExisted = payload.status;
  },

  UPDATE_ID: (state, payload) => {
    state.id = payload.id;
  }
};

const actions = {
  fetchSubs8({ commit }) {
    const data = constant.SUBTEST8;
    commit('SET_ITEM', data);
    return data;
  },

  // eslint-disable-next-line no-unused-vars
  fetchInitial: ({ commit }) => {
    commit('SET_ITEM', null);

    let initial = {
      soalName: '',
      durasiPengerjaan: 0,
      soal: {
        subtest1: {},
        subtest2: {},
        subtest3: {},
        subtest4: {},
        subtest5: {},
        subtest6: {},
        subtest7: {},
        subtest8: {},
        subtest9: {}
      }
    };

    commit('SET_ITEM', initial);
    return initial;
  },

  // eslint-disable-next-line no-unused-vars
  fetchImage: ({ commit }, payload) => {
    return window.axios
      .get(`/image-store/${payload.id}`, {
        responseType: 'arraybuffer'
      })
      .then((response) => {
        return (
          `data:${response.headers['content-type']};base64,` +
          Buffer.from(response.data, 'binary').toString('base64')
        );
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchSave: ({ commit }, payload) => {
    let data = payload.data;

    return window
      .axios({
        method: 'post',
        url: '/ist',
        data: data
      })
      .then((response) => {
        commit('SET_ITEM', null);
        return response.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchUpdate: ({ commit }, payload) => {
    let data = payload.data;

    return window
      .axios({
        method: 'put',
        url: `/ist/${payload.id}/version/${payload.version}`,
        data: data
      })
      .then((response) => {
        commit('SET_ITEM', null);
        return response.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchApprove: ({ commit }, payload) => {
    let data = payload.data;

    return window
      .axios({
        method: 'put',
        url: `/ist/approve/${payload.id}/version/${payload.version}`,
        data: data
      })
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchDetail: ({ commit }, payload) => {
    return window.axios
      .get(`/ist/${payload.id}/version/${payload.version}`)
      .then((response) => {
        commit('SET_ITEM', null);

        // set item
        commit('SET_ITEM', response.data.data);
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchReview: ({ commit }, payload) => {
    return window.axios
      .get(`/ist/edit-preview/${payload.id}/version/${payload.version}`)
      .then((response) => {
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchDelete: ({ commit }, payload) => {
    return window.axios
      .delete(`/ist/${payload.id}/version/${payload.version}`)
      .then((response) => {
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  }
};

export { namespaced, state, getters, mutations, actions };
