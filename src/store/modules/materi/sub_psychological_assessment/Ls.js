// eslint-disable-next-line no-unused-vars
import qs from 'querystring';
import ls from '@/views/Materi/PsychologicalAssessment/Question/Lookup/ls';

const namespaced = true;

const state = {
  items: null
};

const getters = {
  items: (state) => state.items
};

const mutations = {
  SET_ITEM: (state, payload) => {
    state.items = payload;
  }
};

const actions = {
  // eslint-disable-next-line no-unused-vars
  fetchAnswerKeys({ commit }, payload) {
    // reset item
    commit('SET_ITEM', null);

    let data = {
      soalName: '',
      durasiPengerjaan: '',
      soal: ls
    };

    commit('SET_ITEM', data);
    return data;
  },

  // eslint-disable-next-line no-unused-vars
  fetchSave: ({ commit }, payload) => {
    let data = payload.data;

    return window
      .axios({
        method: 'post',
        url: '/ls',
        data: data
      })
      .then((response) => {
        commit('SET_ITEM', null);
        return response.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchUpdate: ({ commit }, payload) => {
    let data = payload.data;

    return window
      .axios({
        method: 'put',
        url: `/ls/${payload.id}/version/${payload.version}`,
        data: data
      })
      .then((response) => {
        commit('SET_ITEM', null);
        return response.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchApprove: ({ commit }, payload) => {
    let data = payload.data;

    return window
      .axios({
        method: 'put',
        url: `/ls/approve/${payload.id}/version/${payload.version}`,
        data: data
      })
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchDetail: ({ commit }, payload) => {
    return window.axios
      .get(`/ls/${payload.id}/version/${payload.version}`)
      .then((response) => {
        // reset item
        commit('SET_ITEM', null);

        // set item
        commit('SET_ITEM', response.data.data);
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchReview: ({ commit }, payload) => {
    return window.axios
      .get(`/ls/edit-preview/${payload.id}/version/${payload.version}`)
      .then((response) => {
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchDelete: ({ commit }, payload) => {
    return window.axios
      .delete(`/ls/${payload.id}/version/${payload.version}`)
      .then((response) => {
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  }
};

export { namespaced, state, getters, mutations, actions };
