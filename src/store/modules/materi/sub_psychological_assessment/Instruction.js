const namespaced = true;

const state = {
  item: null
};

const getters = {
  items: (state) => state.items
};

const mutations = {
  SET_ITEM: (state, payload) => {
    state.items = payload;
  }
};

const actions = {
  // eslint-disable-next-line no-unused-vars
  fetchDetail: ({ commit }, payload) => {
    return window.axios
      .get(`/petunjuk/${payload.type}`)
      .then((response) => {
        return response.data.data;
      })
      .catch((e) => {
        throw e;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchSave: ({ commit }, payload) => {
    return window
      .axios({
        method: 'post',
        url: 'petunjuk/',
        data: payload.data
      })
      .then((response) => {
        return response.data;
      })
      .catch((e) => {
        throw e;
      });
  }
};

export { namespaced, state, getters, mutations, actions };
