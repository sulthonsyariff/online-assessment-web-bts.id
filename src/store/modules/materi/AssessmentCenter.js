// eslint-disable-next-line no-unused-vars
import qs from 'querystring';

const namespaced = true;

const state = {
  items: [],
  approvedStatus: null
};

const getters = {
  items: (state) => state.items,
  approvedStatus: (state) => state.approvedStatus
};

const mutations = {
  SET_ITEM: (state, payload) => {
    state.items = payload;
  },
  SET_APPROVED_STATUS: (state, payload) => {
    state.approvedStatus = payload;
  }
};

const actions = {
  fetchList: ({ commit, rootGetters }, payload) => {
    let defaultPageable = rootGetters['table/defaultPageable'];

    let queryParams = {
      limit: defaultPageable.limit,
      page: defaultPageable.currentPage - 1,
      search: defaultPageable.search,
      soalType: payload.soalType,
      targetJobName: payload.targetJobName
    };

    return window.axios
      .get('/soal-simulasi', {
        params: queryParams,
        paramsSerializer: function(params) {
          return qs.stringify(params);
        }
      })
      .then((response) => {
        let data = response.data.data;

        commit('SET_ITEM', data.content);
        commit('table/SET_DEFAULT_TOTAL_PAGES', data.totalPages, {
          root: true
        });
        commit('table/SET_DEFAULT_TOTAL_ELEMENTS', data.totalElements, {
          root: true
        });

        return data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchInstruksiPeran: ({ commit }, payload) => {
    return window.axios
      .get(`/instruksi-peran`, {
        params: payload,
        paramsSerializer: function(params) {
          return qs.stringify(params);
        }
      })
      .then((response) => {
        return response.data.data;
      })
      .catch((error) => {
        throw error;
      });
  }
};

export { namespaced, state, getters, mutations, actions };
