import camelCase from 'lodash/camelCase';

const modules = {};

// master
const requireMaster = require.context('./master', false, /\.js$/);
requireMaster.keys().forEach((fileName) => {
  if (fileName === './index.js') return;
  const moduleName = camelCase(fileName.replace(/(\.\/|\.js)/g, ''));
  modules[moduleName] = {
    namespaced: true,
    ...requireMaster(fileName)
  };
});

// materi
const requireMateri = require.context('./materi', false, /\.js$/);
requireMateri.keys().forEach((fileName) => {
  if (fileName === './index.js') return;
  const moduleName = camelCase(fileName.replace(/(\.\/|\.js)/g, ''));
  modules[moduleName] = {
    namespaced: true,
    ...requireMateri(fileName)
  };
});

// sub materi (psychological)
const requirePsychological = require.context(
  './materi/sub_psychological_assessment',
  false,
  /\.js$/
);
requirePsychological.keys().forEach((fileName) => {
  if (fileName === './index.js') return;
  const moduleName = camelCase(fileName.replace(/(\.\/|\.js)/g, ''));
  modules[moduleName] = {
    namespaced: true,
    ...requirePsychological(fileName)
  };
});

// sub materi (assessment)
const requireAssessment = require.context(
  './materi/sub_assessment_center',
  false,
  /\.js$/
);
requireAssessment.keys().forEach((fileName) => {
  if (fileName === './index.js') return;
  const moduleName = camelCase(fileName.replace(/(\.\/|\.js)/g, ''));
  modules[moduleName] = {
    namespaced: true,
    ...requireAssessment(fileName)
  };
});

const requireSecutiry = require.context('./security', false, /\.js$/);
requireSecutiry.keys().forEach((fileName) => {
  if (fileName === './index.js') return;
  const moduleName = camelCase(fileName.replace(/(\.\/|\.js)/g, ''));
  modules[moduleName] = {
    namespaced: true,
    ...requireSecutiry(fileName)
  };
});

const requireTransaction = require.context('./transaction', false, /\.js$/);
requireTransaction.keys().forEach((fileName) => {
  if (fileName === './index.js') return;
  const moduleName = camelCase(fileName.replace(/(\.\/|\.js)/g, ''));
  modules[moduleName] = {
    namespaced: true,
    ...requireTransaction(fileName)
  };
});

const requireOthers = require.context('./others', false, /\.js$/);
requireOthers.keys().forEach((fileName) => {
  if (fileName === './index.js') return;
  const moduleName = camelCase(fileName.replace(/(\.\/|\.js)/g, ''));
  modules[moduleName] = {
    namespaced: true,
    ...requireOthers(fileName)
  };
});

export default modules;
