const namespaced = true;

const state = {
  pageable: [
    {
      search: '',
      limit: 10,
      totalPages: 0,
      totalElements: 0,
      currentPage: 1
    }
  ],
  optionList: [
    { text: '10', value: 10 },
    { text: '50', value: 50 },
    { text: '100', value: 100 }
  ]
};

const getters = {
  defaultPageable: (state) => {
    return state.pageable[0];
  },
  pageableById: (state) => (id) => {
    return state.pageable[id];
  },
  optionList: (state) => state.optionList
};

const mutations = {
  SET_DEFAULT_SEARCH: (state, payload) => {
    state.pageable[0].search = payload;
    state.pageable[0].currentPage = 1;
  },

  SET_DEFAULT_LIMIT: (state, payload) => {
    state.pageable[0].limit = payload;
    state.pageable[0].currentPage = 1;
  },

  SET_DEFAULT_TOTAL_PAGES: (state, payload) => {
    state.pageable[0].totalPages = payload;
  },

  SET_DEFAULT_TOTAL_ELEMENTS: (state, payload) => {
    state.pageable[0].totalElements = payload;
  },

  SET_DEFAULT_CURRENT_PAGE: (state, payload) => {
    state.pageable[0].currentPage = payload;
  },

  RESET_PAGEABLE: (state) => {
    state.pageable[0].search = '';
    state.pageable[0].limit = 10;
    state.pageable[0].totalPages = 0;
    state.pageable[0].totalElements = 0;
    state.pageable[0].currentPage = 1;
  },

  ADD_PAGEABLE: (state, payload) => {
    state.pageable.push(payload);
  },

  SET_SEARCH: (state, payload) => {
    state.pageable[payload.index].search = payload.data;
    state.pageable[payload.index].currentPage = 1;
  },

  SET_LIMIT: (state, payload) => {
    state.pageable[payload.index].limit = payload.data;
    state.pageable[payload.index].currentPage = 1;
  },

  SET_TOTAL_PAGES: (state, payload) => {
    state.pageable[payload.index].totalPages = payload.data;
  },

  SET_TOTAL_ELEMENTS: (state, payload) => {
    state.pageable[payload.index].totalElements = payload.data;
  },

  SET_CURRENT_PAGE: (state, payload) => {
    state.pageable[payload.index].currentPage = payload.data;
  },

  RESET_CUSTOM_PAGEABLE: (state) => {
    state.pageable = [
      {
        search: '',
        limit: 10,
        totalPages: 0,
        totalElements: 0,
        currentPage: 1
      }
    ];
  }
};

const actions = {};

export { namespaced, state, getters, mutations, actions };
