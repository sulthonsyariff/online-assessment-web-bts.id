const namespaced = true;

const state = {
  message: '',
  textMessage: '',
  typeNotification: 'success',
  showNotification: false,
  serverTime: ''
};

const getters = {
  message: (state) => state.message,
  textMessage: (state) => state.textMessage,
  typeNotification: (state) => state.typeNotification,
  showNotification: (state) => state.showNotification,
  getTimeServer: (state) => state.serverTime
};

const mutations = {
  SET_MESSAGE: (state, payload) => {
    state.message = payload;
  },
  SET_TEXT_MESSAGE: (state, payload) => {
    state.textMessage = payload;
  },
  SET_NOTIFICATION: (state, payload) => {
    state.showNotification = payload;
  },
  SET_TYPE_NOTIFICATION: (state, payload) => {
    state.typeNotification = payload;
  },
  SET_TIME_SERVER: (state, payload) => {
    state.serverTime = payload;
  }
};

const actions = {
  showSuccessNotification({ commit }, payload) {
    let message = payload.message ? payload.message : 'Success';
    let textMessage = payload.textMessage ? payload.textMessage : '';
    let timestamp = new Date().getUTCMilliseconds();

    commit('SET_MESSAGE', message);
    commit('SET_TEXT_MESSAGE', textMessage);
    commit('SET_TYPE_NOTIFICATION', 'success');
    commit('SET_NOTIFICATION', timestamp);
  },

  showErrorNotification({ commit }, payload) {
    let message = payload.message ? payload.message : 'Error';
    let textMessage = payload.textMessage ? payload.textMessage : '';
    let timestamp = new Date().getUTCMilliseconds();

    commit('SET_MESSAGE', message);
    commit('SET_TEXT_MESSAGE', textMessage);
    commit('SET_TYPE_NOTIFICATION', 'danger');
    commit('SET_NOTIFICATION', timestamp);
  },

  showErrorReqNotification({ commit }, payload) {
    let message = 'Error';
    let textMessage = '';
    let timestamp = new Date().getUTCMilliseconds();

    if (payload.response && payload.response.data) {
      message = payload.response.data.message;
      textMessage = payload.response.data.errorMessage;
    } else {
      textMessage = payload.message;
    }

    commit('SET_MESSAGE', message);
    commit('SET_TEXT_MESSAGE', textMessage);
    commit('SET_TYPE_NOTIFICATION', 'danger');
    commit('SET_NOTIFICATION', timestamp);
  },

  refreshNotification({ commit }) {
    commit('SET_MESSAGE', '');
    commit('SET_TEXT_MESSAGE', '');
    commit('SET_TYPE_NOTIFICATION', 'success');
    commit('SET_NOTIFICATION', false);
  },

  // eslint-disable-next-line no-unused-vars
  getTimeServer: ({ commit }) => {
    return window.axios
      .get('/time', {})
      .then(() => {
        commit('SET_TIME_SERVER', new Date().getTime());
      })
      .catch((error) => {
        throw error;
      });
  }
};

export { namespaced, state, getters, mutations, actions };
