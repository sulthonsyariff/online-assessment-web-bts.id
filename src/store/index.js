import Vue from 'vue';
import Vuex from 'vuex';
import VuexPersistence from 'vuex-persist';
import modules from './modules';

import * as Cookies from 'js-cookie';

Vue.use(Vuex);

const vuexCookie = new VuexPersistence({
  key: 'bpi-session',
  restoreState: (key) => Cookies.getJSON(key),
  saveState: (key, state) =>
    Cookies.set(key, state, {
      expires: 3
    }),
  modules: ['auth']
});

const vuexLocal = new VuexPersistence({
  key: 'bpi-materi',
  storage: window.localStorage,
  modules: ['ist', 'project']
});

export default new Vuex.Store({
  modules,
  plugins: [vuexCookie.plugin, vuexLocal.plugin]
});
