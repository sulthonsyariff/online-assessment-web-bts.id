/* STATUS PERSETUJUAN */
const STATUS = [
  {
    id: 0,
    name: 'Menunggu Persetujuan Create',
    color: 'text-blue-600'
  },
  {
    id: 1,
    name: 'Menunggu Persetujuan Edit',
    color: 'text-blue-600'
  },
  {
    id: 2,
    name: 'Menunggu Persetujuan Delete',
    color: 'text-orange-600'
  },
  {
    id: 3,
    name: 'OK',
    color: 'text-green-600'
  },
  {
    id: 4,
    name: 'Ditolak',
    color: 'text-red-500'
  },
  {
    id: 5,
    name: 'Tidak lengkap',
    color: 'text-grey-600'
  },
  {
    id: 6,
    name: 'Draft',
    color: 'text-grey-600'
  }
];

const LEVEL_TARGET_JOB = [
  {
    value: 'STAFF',
    text: 'STAFF'
  },
  {
    value: 'FIRST_LINE_MANAGER',
    text: 'FIRST LINE MANAGER'
  },
  {
    value: 'MIDDLE_MANAGER',
    text: 'MIDDLE MANAGER'
  },
  {
    value: 'GENERAL_MANAGER',
    text: 'GENERAL MANAGER'
  },
  {
    value: 'DIREKTUR',
    text: 'DIREKTUR'
  }
];

const APPLY_ROLES = [
  'CUSTOMER_INTERACTION',
  'PEER_INTERACTION',
  'LEADERLESS_GROUP_DISCUSSION'
];

const NORMA_PENDIDIKAN = [
  {
    value: 'NORMA_EKON',
    text: 'EKONOM'
  },
  {
    value: 'NORMA_PSI',
    text: 'PSIKOLOG'
  },
  {
    value: 'NORMA_DOSEN',
    text: 'DOSEN'
  },
  {
    value: 'NORMA_MAT',
    text: 'MATEMATIK'
  },
  {
    value: 'NORMA_HUK',
    text: 'HUKUM'
  },
  {
    value: 'NORMA_IR',
    text: 'INSINYUR'
  },
  {
    value: 'NORMA_ARS',
    text: 'ARSITEK'
  },
  {
    value: 'NORMA_KIMIA',
    text: 'KIMIA'
  },
  {
    value: 'NORMA_D3TEK',
    text: 'D3-TEKNIK'
  },
  {
    value: 'NORMA_LAIN_LAIN',
    text: 'LAIN-LAIN'
  }
];

export default {
  STATUS,
  LEVEL_TARGET_JOB,
  APPLY_ROLES,
  NORMA_PENDIDIKAN
};
