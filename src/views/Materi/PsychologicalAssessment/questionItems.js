export default [
  {
    code: 'DISC',
    name: 'Disc',
    toCreate: 'CreateDisc',
    toUpdate: 'UpdateDisc',
    toDetail: 'DetailDisc',
    toApprove: 'ApproveDisc',
    key: {
      approve: 'disc/fetchApprove',
      delete: 'disc/fetchDelete'
    }
  },
  {
    code: 'PAPIKOSTIK',
    name: 'Papi Kostik',
    toCreate: 'CreatePapikostik',
    toUpdate: 'UpdatePapikostik',
    toDetail: 'DetailPapikostik',
    toApprove: 'ApprovePapikostik',
    key: {
      approve: 'papikostik/fetchApprove',
      delete: 'papikostik/fetchDelete'
    }
  },
  {
    code: 'VALUE',
    name: 'Value',
    toCreate: 'CreateValue',
    toUpdate: 'UpdateValue',
    toDetail: 'DetailValue',
    toApprove: 'ApproveValue',
    key: {
      approve: 'value/fetchApprove',
      delete: 'value/fetchDelete'
    }
  },
  {
    code: 'LS',
    name: 'Leadership Style',
    toCreate: 'CreateLs',
    toUpdate: 'UpdateLs',
    toDetail: 'DetailLs',
    toApprove: 'ApproveLs',
    key: {
      approve: 'ls/fetchApprove',
      delete: 'ls/fetchDelete'
    }
  },
  {
    code: 'BELBIN',
    name: 'Team Style',
    toCreate: 'CreateBelbin',
    toUpdate: 'UpdateBelbin',
    toDetail: 'DetailBelbin',
    toApprove: 'ApproveBelbin',
    key: {
      approve: 'belbin/fetchApprove',
      delete: 'belbin/fetchDelete'
    }
  },
  {
    code: 'IST',
    name: 'Intelligence Structure Test',
    toCreate: 'CreateIst',
    toUpdate: 'UpdateIst',
    toDetail: 'DetailIst',
    toApprove: 'ApproveIst',
    key: {
      delete: 'ist/fetchDelete',
      approve: 'ist/fetchApprove'
    }
  },
  {
    code: 'PF16',
    name: '16 PF',
    toCreate: 'CreatePf',
    toUpdate: 'UpdatePf',
    toDetail: 'DetailPf',
    toApprove: 'ApprovePf',
    key: {
      approve: 'pf/fetchApprove',
      delete: 'pf/fetchDelete'
    }
  }
];
