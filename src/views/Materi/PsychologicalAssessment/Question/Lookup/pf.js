export default [
  [
    {
      nomor: 1,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '2', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 2,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '2', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 3,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 4,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '2', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 5,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '2', description: 'm' }
      ],
      answer: ''
    }
  ],
  [
    {
      nomor: 6,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '2', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 7,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '2', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 8,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '2', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 9,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '2', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 10,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '2', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    }
  ],
  [
    {
      nomor: 11,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '2', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 12,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '2', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 13,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '2', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 14,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '2', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 15,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '2', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    }
  ],
  [
    {
      nomor: 16,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '2', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 17,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '2', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 18,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '2', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 19,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '2', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 20,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '0', description: 'm' },
        { notasi: 'c', key: '1', description: 'm' }
      ],
      answer: ''
    }
  ],
  [
    {
      nomor: 21,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '2', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 22,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '2', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 23,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '2', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 24,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '2', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 25,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '2', description: 'm' }
      ],
      answer: ''
    }
  ],
  [
    {
      nomor: 26,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '2', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 27,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '2', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 28,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '2', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 29,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '2', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 30,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '2', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    }
  ],
  [
    {
      nomor: 31,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '2', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 32,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '2', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 33,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '2', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 34,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '2', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 35,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '2', description: 'm' }
      ],
      answer: ''
    }
  ],
  [
    {
      nomor: 36,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '2', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 37,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 38,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '2', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 39,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '2', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 40,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '2', description: 'm' }
      ],
      answer: ''
    }
  ],
  [
    {
      nomor: 41,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '2', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 42,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '2', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 43,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '2', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 44,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '2', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 45,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '2', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    }
  ],
  [
    {
      nomor: 46,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '2', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 47,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '2', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 48,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '2', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 49,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '2', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 50,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '2', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    }
  ],
  [
    {
      nomor: 51,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '2', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 52,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '2', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 53,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '2', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 54,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '0', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 55,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '2', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    }
  ],
  [
    {
      nomor: 56,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '2', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 57,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '2', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 58,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '2', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 59,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '2', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 60,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '2', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    }
  ],
  [
    {
      nomor: 61,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '2', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 62,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '2', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 63,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '2', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 64,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '2', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 65,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '2', description: 'm' }
      ],
      answer: ''
    }
  ],
  [
    {
      nomor: 66,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '2', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 67,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '2', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 68,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '2', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 69,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 70,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '2', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    }
  ],
  [
    {
      nomor: 71,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '1', description: 'm' },
        { notasi: 'b', key: '0', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 72,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '2', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 73,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '2', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 74,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '2', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 75,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '2', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    }
  ],
  [
    {
      nomor: 76,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '2', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 77,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '2', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 78,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '2', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 79,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '2', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 80,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '2', description: 'm' }
      ],
      answer: ''
    }
  ],
  [
    {
      nomor: 81,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '2', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 82,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '2', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 83,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '2', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 84,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '2', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 85,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '2', description: 'm' }
      ],
      answer: ''
    }
  ],
  [
    {
      nomor: 86,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '2', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 87,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '2', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 88,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '0', description: 'm' },
        { notasi: 'c', key: '1', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 89,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '2', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 90,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '2', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    }
  ],
  [
    {
      nomor: 91,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '2', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 92,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '2', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 93,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '2', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 94,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '2', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 95,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '2', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    }
  ],
  [
    {
      nomor: 96,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '2', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 97,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '2', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 98,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '2', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 99,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '2', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 100,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '2', description: 'm' }
      ],
      answer: ''
    }
  ],
  [
    {
      nomor: 101,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '2', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 102,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '2', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 103,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '2', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 104,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '1', description: 'm' },
        { notasi: 'b', key: '0', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    },
    {
      nomor: 105,
      pernyataan: 'a',
      opsi: [
        { notasi: 'a', key: '0', description: 'm' },
        { notasi: 'b', key: '1', description: 'm' },
        { notasi: 'c', key: '0', description: 'm' }
      ],
      answer: ''
    }
  ]
];
