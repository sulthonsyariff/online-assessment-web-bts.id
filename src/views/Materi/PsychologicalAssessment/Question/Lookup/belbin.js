export default [
  [
    {
      nomor: 1,
      judul: 'Kontribusi saya dalam tim',
      opsi: [
        {
          pernyataan: '1',
          description: '',
          key: { self: 'Resource Investigator', other: 'B' }
        },
        {
          pernyataan: '2',
          description: '',
          key: { self: 'Coordinator', other: 'D' }
        },
        {
          pernyataan: '3',
          description: '',
          key: { self: 'Team Worker', other: 'C' }
        },
        {
          pernyataan: '4',
          description: '',
          key: { self: 'Plant', other: 'A' }
        },
        {
          pernyataan: '5',
          description: '',
          key: { self: 'Coordinator', other: 'D' }
        },
        {
          pernyataan: '6',
          description: '',
          key: { self: 'Implementer', other: 'E' }
        },
        {
          pernyataan: '7',
          description: '',
          key: { self: 'Specialist', other: 'F' }
        },
        {
          pernyataan: '8',
          description: '',
          key: { self: 'Monitor Evaluator', other: 'G' }
        },
        {
          pernyataan: '9',
          description: '',
          key: { self: 'Completer Finisher', other: 'H' }
        },
        {
          pernyataan: '10',
          description: '',
          key: { self: 'Sharper', other: 'I' }
        }
      ]
    }
  ],
  [
    {
      nomor: 2,
      judul: 'Saat bergabung dalam suatu tim',
      opsi: [
        {
          pernyataan: '1',
          description: '',
          key: { self: 'Coordinator', other: 'D' }
        },
        {
          pernyataan: '2',
          description: '',
          key: { self: 'Team Worker', other: 'C' }
        },
        {
          pernyataan: '3',
          description: '',
          key: { self: 'Sharper', other: 'I' }
        },
        {
          pernyataan: '4',
          description: '',
          key: { self: 'Plant', other: 'A' }
        },
        {
          pernyataan: '5',
          description: '',
          key: { self: 'Implementer', other: 'E' }
        },
        {
          pernyataan: '6',
          description: '',
          key: { self: 'Resource Investigator', other: 'B' }
        },
        {
          pernyataan: '7',
          description: '',
          key: { self: 'Plant', other: 'A' }
        },
        {
          pernyataan: '8',
          description: '',
          key: { self: 'Specialist', other: 'F' }
        },
        {
          pernyataan: '9',
          description: '',
          key: { self: 'Monitor Evaluator', other: 'G' }
        },
        {
          pernyataan: '10',
          description: '',
          key: { self: 'Completer Finisher', other: 'H' }
        }
      ]
    }
  ],
  [
    {
      nomor: 3,
      judul: 'Pendekatan yang sering saya lakukan dalam tim',
      opsi: [
        {
          pernyataan: '1',
          description: '',
          key: { self: 'Team Worker', other: 'C' }
        },
        {
          pernyataan: '2',
          description: '',
          key: { self: 'Specialist', other: 'F' }
        },
        {
          pernyataan: '3',
          description: '',
          key: { self: 'Sharper', other: 'I' }
        },
        {
          pernyataan: '4',
          description: '',
          key: { self: 'Coordinator', other: 'D' }
        },
        {
          pernyataan: '5',
          description: '',
          key: { self: 'Implementer', other: 'E' }
        },
        {
          pernyataan: '6',
          description: '',
          key: { self: 'Plant', other: 'A' }
        },
        {
          pernyataan: '7',
          description: '',
          key: { self: 'Completer Finisher', other: 'H' }
        },
        {
          pernyataan: '8',
          description: '',
          key: { self: 'Resource Investigator', other: 'B' }
        },
        {
          pernyataan: '9',
          description: '',
          key: { self: 'Team Worker', other: 'C' }
        },
        {
          pernyataan: '10',
          description: '',
          key: { self: 'Monitor Evaluator', other: 'G' }
        }
      ]
    }
  ],
  [
    {
      nomor: 4,
      judul:
        'Saat tiba-tiba saya diberi tugas yang sulit dengan waktu terbatas dan orang-orang yang tidak saya kenal sebelumnya',
      opsi: [
        {
          pernyataan: '1',
          description: '',
          key: { self: 'Implementer', other: 'E' }
        },
        {
          pernyataan: '2',
          description: '',
          key: { self: 'Specialist', other: 'F' }
        },
        {
          pernyataan: '3',
          description: '',
          key: { self: 'Plant', other: 'A' }
        },
        {
          pernyataan: '4',
          description: '',
          key: { self: 'Team Worker', other: 'C' }
        },
        {
          pernyataan: '5',
          description: '',
          key: { self: 'Coordinator', other: 'D' }
        },
        {
          pernyataan: '6',
          description: '',
          key: { self: 'Monitor Evaluator', other: 'G' }
        },
        {
          pernyataan: '7',
          description: '',
          key: { self: 'Completer Finishser', other: 'H' }
        },
        {
          pernyataan: '8',
          description: '',
          key: { self: 'Sharper', other: 'I' }
        },
        {
          pernyataan: '9',
          description: '',
          key: { self: 'Sharper', other: 'I' }
        },
        {
          pernyataan: '10',
          description: '',
          key: { self: 'Resource Investigator', other: 'B' }
        }
      ]
    }
  ],
  [
    {
      nomor: 5,
      judul: 'Masalah yang sering saya hadapi saat bekerja dalam tim',
      opsi: [
        {
          pernyataan: '1',
          description: '',
          key: { self: 'Completer Finisher', other: 'H' }
        },
        {
          pernyataan: '2',
          description: '',
          key: { self: 'Sharper', other: 'I' }
        },
        {
          pernyataan: '3',
          description: '',
          key: { self: 'Monitor Evaluator', other: 'G' }
        },
        {
          pernyataan: '4',
          description: '',
          key: { self: 'Resource Investigator', other: 'B' }
        },
        {
          pernyataan: '5',
          description: '',
          key: { self: 'Implementer', other: 'E' }
        },
        {
          pernyataan: '6',
          description: '',
          key: { self: 'Team Worker', other: 'C' }
        },
        {
          pernyataan: '7',
          description: '',
          key: { self: 'Coordinator', other: 'D' }
        },
        {
          pernyataan: '8',
          description: '',
          key: { self: 'Plant', other: 'A' }
        },
        {
          pernyataan: '9',
          description: '',
          key: { self: 'Specialist', other: 'F' }
        },
        {
          pernyataan: '10',
          description: '',
          key: { self: 'Specialist', other: 'F' }
        }
      ]
    }
  ],
  [
    {
      nomor: 6,
      judul: 'Saya mendapatkan kepuasan kerja karena',
      opsi: [
        {
          pernyataan: '1',
          description: '',
          key: { self: 'Monitor Evaluator', other: 'G' }
        },
        {
          pernyataan: '2',
          description: '',
          key: { self: 'Implementer', other: 'E' }
        },
        {
          pernyataan: '3',
          description: '',
          key: { self: 'Team Worker', other: 'C' }
        },
        {
          pernyataan: '4',
          description: '',
          key: { self: 'Completer Finisher', other: 'H' }
        },
        {
          pernyataan: '5',
          description: '',
          key: { self: 'Resource Investigator', other: 'B' }
        },
        {
          pernyataan: '6',
          description: '',
          key: { self: 'Coordinator', other: 'D' }
        },
        {
          pernyataan: '7',
          description: '',
          key: { self: 'Implementer', other: 'E' }
        },
        {
          pernyataan: '8',
          description: '',
          key: { self: 'Plant', other: 'A' }
        },
        {
          pernyataan: '9',
          description: '',
          key: { self: 'Specialist', other: 'F' }
        },
        {
          pernyataan: '10',
          description: '',
          key: { self: 'Sharper', other: 'I' }
        }
      ]
    }
  ]
];
