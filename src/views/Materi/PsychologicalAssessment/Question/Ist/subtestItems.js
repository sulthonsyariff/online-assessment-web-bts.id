export default [
  {
    code: 'SE',
    title: 'Subtes 1',
    subtitle: 'Melengkapi Kalimat',
    toCreate: 'CreateSubtest1',
    toDetail: 'DetailSubtest1',
    toUpdate: 'UpdateSubtest1',
    status: null
  },
  {
    code: 'WA',
    title: 'Subtes 2',
    subtitle: 'Melengkapi Kalimat',
    toCreate: 'CreateSubtest2',
    toDetail: 'DetailSubtest2',
    toUpdate: 'UpdateSubtest2',
    status: null
  },
  {
    code: 'AN',
    title: 'Subtes 3',
    subtitle: 'Persamaan Kata',
    toCreate: 'CreateSubtest3',
    toDetail: 'DetailSubtest3',
    toUpdate: 'UpdateSubtest3',
    status: null
  },
  {
    code: 'GE',
    title: 'Subtes 4',
    subtitle: 'Sifat Yang Dimiliki Bersama',
    toCreate: 'CreateSubtest4',
    toDetail: 'DetailSubtest4',
    toUpdate: 'UpdateSubtest4',
    status: null
  },
  {
    code: 'RA',
    title: 'Subtes 5',
    subtitle: 'Berhitung',
    toCreate: 'CreateSubtest5',
    toDetail: 'DetailSubtest5',
    toUpdate: 'UpdateSubtest5',
    status: null
  },
  {
    code: 'ZR',
    title: 'Subtes 6',
    subtitle: 'Deret Angka',
    toCreate: 'CreateSubtest6',
    toDetail: 'DetailSubtest6',
    toUpdate: 'UpdateSubtest6',
    status: null
  },
  {
    code: 'FA',
    title: 'Subtes 7',
    subtitle: 'Memilih Bentuk',
    toCreate: 'CreateSubtest7',
    toDetail: 'DetailSubtest7',
    toUpdate: 'UpdateSubtest7',
    status: null
  },
  {
    code: 'WU',
    title: 'Subtes 8',
    subtitle: 'Latihan Balok',
    toCreate: 'CreateSubtest8',
    toDetail: 'DetailSubtest8',
    toUpdate: 'UpdateSubtest8',
    status: null
  },
  {
    code: 'ME',
    title: 'Subtes 9',
    subtitle: 'Latihan Simbol',
    toCreate: 'CreateSubtest9',
    toDetail: 'DetailSubtest9',
    toUpdate: 'UpdateSubtest9',
    status: null
  }
];
