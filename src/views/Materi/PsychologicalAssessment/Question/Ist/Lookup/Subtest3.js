export default {
  durasi: null,
  soalName: '',
  soal: [
    [
      {
        nomor: 41,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: '',
            key: ''
          },
          {
            notasi: 'b',
            description: '',
            key: ''
          },
          {
            notasi: 'c',
            description: '',
            key: ''
          },
          {
            notasi: 'd',
            description: '',
            key: ''
          },
          {
            notasi: 'e',
            description: '',
            key: ''
          }
        ],
        key: ''
      },
      {
        nomor: 42,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: '',
            key: ''
          },
          {
            notasi: 'b',
            description: '',
            key: ''
          },
          {
            notasi: 'c',
            description: '',
            key: ''
          },
          {
            notasi: 'd',
            description: '',
            key: ''
          },
          {
            notasi: 'e',
            description: '',
            key: ''
          }
        ],
        key: ''
      },
      {
        nomor: 43,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: '',
            key: ''
          },
          {
            notasi: 'b',
            description: '',
            key: ''
          },
          {
            notasi: 'c',
            description: '',
            key: ''
          },
          {
            notasi: 'd',
            description: '',
            key: ''
          },
          {
            notasi: 'e',
            description: '',
            key: ''
          }
        ],
        key: ''
      },
      {
        nomor: 44,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: '',
            key: ''
          },
          {
            notasi: 'b',
            description: '',
            key: ''
          },
          {
            notasi: 'c',
            description: '',
            key: ''
          },
          {
            notasi: 'd',
            description: '',
            key: ''
          },
          {
            notasi: 'e',
            description: '',
            key: ''
          }
        ],
        key: ''
      },
      {
        nomor: 45,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: '',
            key: ''
          },
          {
            notasi: 'b',
            description: '',
            key: ''
          },
          {
            notasi: 'c',
            description: '',
            key: ''
          },
          {
            notasi: 'd',
            description: '',
            key: ''
          },
          {
            notasi: 'e',
            description: '',
            key: ''
          }
        ],
        key: ''
      }
    ],
    [
      {
        nomor: 46,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: '',
            key: ''
          },
          {
            notasi: 'b',
            description: '',
            key: ''
          },
          {
            notasi: 'c',
            description: '',
            key: ''
          },
          {
            notasi: 'd',
            description: '',
            key: ''
          },
          {
            notasi: 'e',
            description: '',
            key: ''
          }
        ],
        key: ''
      },
      {
        nomor: 47,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: '',
            key: ''
          },
          {
            notasi: 'b',
            description: '',
            key: ''
          },
          {
            notasi: 'c',
            description: '',
            key: ''
          },
          {
            notasi: 'd',
            description: '',
            key: ''
          },
          {
            notasi: 'e',
            description: '',
            key: ''
          }
        ],
        key: ''
      },
      {
        nomor: 48,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: '',
            key: ''
          },
          {
            notasi: 'b',
            description: '',
            key: ''
          },
          {
            notasi: 'c',
            description: '',
            key: ''
          },
          {
            notasi: 'd',
            description: '',
            key: ''
          },
          {
            notasi: 'e',
            description: '',
            key: ''
          }
        ],
        key: ''
      },
      {
        nomor: 49,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: '',
            key: ''
          },
          {
            notasi: 'b',
            description: '',
            key: ''
          },
          {
            notasi: 'c',
            description: '',
            key: ''
          },
          {
            notasi: 'd',
            description: '',
            key: ''
          },
          {
            notasi: 'e',
            description: '',
            key: ''
          }
        ],
        key: ''
      },
      {
        nomor: 50,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: '',
            key: ''
          },
          {
            notasi: 'b',
            description: '',
            key: ''
          },
          {
            notasi: 'c',
            description: '',
            key: ''
          },
          {
            notasi: 'd',
            description: '',
            key: ''
          },
          {
            notasi: 'e',
            description: '',
            key: ''
          }
        ],
        key: ''
      }
    ],
    [
      {
        nomor: 51,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: '',
            key: ''
          },
          {
            notasi: 'b',
            description: '',
            key: ''
          },
          {
            notasi: 'c',
            description: '',
            key: ''
          },
          {
            notasi: 'd',
            description: '',
            key: ''
          },
          {
            notasi: 'e',
            description: '',
            key: ''
          }
        ],
        key: ''
      },
      {
        nomor: 52,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: '',
            key: ''
          },
          {
            notasi: 'b',
            description: '',
            key: ''
          },
          {
            notasi: 'c',
            description: '',
            key: ''
          },
          {
            notasi: 'd',
            description: '',
            key: ''
          },
          {
            notasi: 'e',
            description: '',
            key: ''
          }
        ],
        key: ''
      },
      {
        nomor: 53,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: '',
            key: ''
          },
          {
            notasi: 'b',
            description: '',
            key: ''
          },
          {
            notasi: 'c',
            description: '',
            key: ''
          },
          {
            notasi: 'd',
            description: '',
            key: ''
          },
          {
            notasi: 'e',
            description: '',
            key: ''
          }
        ],
        key: ''
      },
      {
        nomor: 54,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: '',
            key: ''
          },
          {
            notasi: 'b',
            description: '',
            key: ''
          },
          {
            notasi: 'c',
            description: '',
            key: ''
          },
          {
            notasi: 'd',
            description: '',
            key: ''
          },
          {
            notasi: 'e',
            description: '',
            key: ''
          }
        ],
        key: ''
      },
      {
        nomor: 55,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: '',
            key: ''
          },
          {
            notasi: 'b',
            description: '',
            key: ''
          },
          {
            notasi: 'c',
            description: '',
            key: ''
          },
          {
            notasi: 'd',
            description: '',
            key: ''
          },
          {
            notasi: 'e',
            description: '',
            key: ''
          }
        ],
        key: ''
      }
    ],
    [
      {
        nomor: 56,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: '',
            key: ''
          },
          {
            notasi: 'b',
            description: '',
            key: ''
          },
          {
            notasi: 'c',
            description: '',
            key: ''
          },
          {
            notasi: 'd',
            description: '',
            key: ''
          },
          {
            notasi: 'e',
            description: '',
            key: ''
          }
        ],
        key: ''
      },
      {
        nomor: 57,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: '',
            key: ''
          },
          {
            notasi: 'b',
            description: '',
            key: ''
          },
          {
            notasi: 'c',
            description: '',
            key: ''
          },
          {
            notasi: 'd',
            description: '',
            key: ''
          },
          {
            notasi: 'e',
            description: '',
            key: ''
          }
        ],
        key: ''
      },
      {
        nomor: 58,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: '',
            key: ''
          },
          {
            notasi: 'b',
            description: '',
            key: ''
          },
          {
            notasi: 'c',
            description: '',
            key: ''
          },
          {
            notasi: 'd',
            description: '',
            key: ''
          },
          {
            notasi: 'e',
            description: '',
            key: ''
          }
        ],
        key: ''
      },
      {
        nomor: 59,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: '',
            key: ''
          },
          {
            notasi: 'b',
            description: '',
            key: ''
          },
          {
            notasi: 'c',
            description: '',
            key: ''
          },
          {
            notasi: 'd',
            description: '',
            key: ''
          },
          {
            notasi: 'e',
            description: '',
            key: ''
          }
        ],
        key: ''
      },
      {
        nomor: 60,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: '',
            key: ''
          },
          {
            notasi: 'b',
            description: '',
            key: ''
          },
          {
            notasi: 'c',
            description: '',
            key: ''
          },
          {
            notasi: 'd',
            description: '',
            key: ''
          },
          {
            notasi: 'e',
            description: '',
            key: ''
          }
        ],
        key: ''
      }
    ]
  ]
};
