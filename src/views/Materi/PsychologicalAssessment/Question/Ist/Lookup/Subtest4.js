export default {
  durasi: null,
  soalName: '',
  soal: [
    [
      {
        nomor: 61,
        pertanyaan: '',
        key: [
          {
            nilai: 2,
            value: ''
          },
          {
            nilai: 1,
            value: ''
          },
          {
            nilai: 0,
            value: ''
          }
        ]
      },
      {
        nomor: 62,
        pertanyaan: '',
        key: [
          {
            nilai: 2,
            value: ''
          },
          {
            nilai: 1,
            value: ''
          },
          {
            nilai: 0,
            value: ''
          }
        ]
      },
      {
        nomor: 63,
        pertanyaan: '',
        key: [
          {
            nilai: 2,
            value: ''
          },
          {
            nilai: 1,
            value: ''
          },
          {
            nilai: 0,
            value: ''
          }
        ]
      },
      {
        nomor: 64,
        pertanyaan: '',
        key: [
          {
            nilai: 2,
            value: ''
          },
          {
            nilai: 1,
            value: ''
          },
          {
            nilai: 0,
            value: ''
          }
        ]
      },
      {
        nomor: 65,
        pertanyaan: '',
        key: [
          {
            nilai: 2,
            value: ''
          },
          {
            nilai: 1,
            value: ''
          },
          {
            nilai: 0,
            value: ''
          }
        ]
      }
    ],
    [
      {
        nomor: 66,
        pertanyaan: '',
        key: [
          {
            nilai: 2,
            value: ''
          },
          {
            nilai: 1,
            value: ''
          },
          {
            nilai: 0,
            value: ''
          }
        ]
      },
      {
        nomor: 67,
        pertanyaan: '',
        key: [
          {
            nilai: 2,
            value: ''
          },
          {
            nilai: 1,
            value: ''
          },
          {
            nilai: 0,
            value: ''
          }
        ]
      },
      {
        nomor: 68,
        pertanyaan: '',
        key: [
          {
            nilai: 2,
            value: ''
          },
          {
            nilai: 1,
            value: ''
          },
          {
            nilai: 0,
            value: ''
          }
        ]
      },
      {
        nomor: 69,
        pertanyaan: '',
        key: [
          {
            nilai: 2,
            value: ''
          },
          {
            nilai: 1,
            value: ''
          },
          {
            nilai: 0,
            value: ''
          }
        ]
      },
      {
        nomor: 70,
        pertanyaan: '',
        key: [
          {
            nilai: 2,
            value: ''
          },
          {
            nilai: 1,
            value: ''
          },
          {
            nilai: 0,
            value: ''
          }
        ]
      }
    ],
    [
      {
        nomor: 71,
        pertanyaan: '',
        key: [
          {
            nilai: 2,
            value: ''
          },
          {
            nilai: 1,
            value: ''
          },
          {
            nilai: 0,
            value: ''
          }
        ]
      },
      {
        nomor: 72,
        pertanyaan: '',
        key: [
          {
            nilai: 2,
            value: ''
          },
          {
            nilai: 1,
            value: ''
          },
          {
            nilai: 0,
            value: ''
          }
        ]
      },
      {
        nomor: 73,
        pertanyaan: '',
        key: [
          {
            nilai: 2,
            value: ''
          },
          {
            nilai: 1,
            value: ''
          },
          {
            nilai: 0,
            value: ''
          }
        ]
      },
      {
        nomor: 74,
        pertanyaan: '',
        key: [
          {
            nilai: 2,
            value: ''
          },
          {
            nilai: 1,
            value: ''
          },
          {
            nilai: 0,
            value: ''
          }
        ]
      },
      {
        nomor: 75,
        pertanyaan: '',
        key: [
          {
            nilai: 2,
            value: ''
          },
          {
            nilai: 1,
            value: ''
          },
          {
            nilai: 0,
            value: ''
          }
        ]
      }
    ],
    [
      {
        nomor: 76,
        pertanyaan: '',
        key: [
          {
            nilai: 2,
            value: ''
          },
          {
            nilai: 1,
            value: ''
          },
          {
            nilai: 0,
            value: ''
          }
        ]
      }
    ]
  ]
};
