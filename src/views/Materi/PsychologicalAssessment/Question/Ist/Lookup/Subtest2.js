export default {
  durasi: null,
  soalName: '',
  soal: [
    [
      {
        nomor: 21,
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      },
      {
        nomor: 22,
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      },
      {
        nomor: 23,
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      },
      {
        nomor: 24,
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      },
      {
        nomor: 25,
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      }
    ],
    [
      {
        nomor: 26,
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      },
      {
        nomor: 27,
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      },
      {
        nomor: 28,
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      },
      {
        nomor: 29,
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      },
      {
        nomor: 30,
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      }
    ],
    [
      {
        nomor: 31,
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      },
      {
        nomor: 32,
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      },
      {
        nomor: 33,
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      },
      {
        nomor: 34,
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      },
      {
        nomor: 35,
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      }
    ],
    [
      {
        nomor: 36,
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      },
      {
        nomor: 37,
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      },
      {
        nomor: 38,
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      },
      {
        nomor: 39,
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: '',
            key: ''
          },
          {
            notasi: 'c',
            description: '',
            key: ''
          },
          {
            notasi: 'd',
            description: '',
            key: ''
          },
          {
            notasi: 'e',
            description: '',
            key: ''
          }
        ],
        key: ''
      },
      {
        nomor: 40,
        opsi: [
          {
            notasi: 'a',
            description: '',
            key: ''
          },
          {
            notasi: 'b',
            description: '',
            key: ''
          },
          {
            notasi: 'c',
            description: '',
            key: ''
          },
          {
            notasi: 'd',
            description: '',
            key: ''
          },
          {
            notasi: 'e',
            description: '',
            key: ''
          }
        ],
        key: ''
      }
    ]
  ]
};
