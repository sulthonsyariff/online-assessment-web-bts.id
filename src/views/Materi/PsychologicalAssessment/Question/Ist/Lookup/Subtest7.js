export default {
  durasi: '',
  ilustrasi: [
    {
      nomorAwal: 117,
      nomorAkhir: 128,
      opsi: [
        {
          notasi: 'a',
          ilustrasiId: 0
        },
        {
          notasi: 'b',
          ilustrasiId: 0
        },
        {
          notasi: 'c',
          ilustrasiId: 0
        },
        {
          notasi: 'd',
          ilustrasiId: 0
        },
        {
          notasi: 'e',
          ilustrasiId: 0
        }
      ]
    },
    {
      nomorAwal: 129,
      nomorAkhir: 136,
      opsi: [
        {
          notasi: 'a',
          ilustrasiId: 0
        },
        {
          notasi: 'b',
          ilustrasiId: 0
        },
        {
          notasi: 'c',
          ilustrasiId: 0
        },
        {
          notasi: 'd',
          ilustrasiId: 0
        },
        {
          notasi: 'e',
          ilustrasiId: 0
        }
      ]
    }
  ],
  soal: [
    [
      {
        nomor: 117,
        gambarId: 0,
        tipeGambar: ''
      },
      {
        nomor: 118,
        gambarId: 0,
        tipeGambar: ''
      },
      {
        nomor: 119,
        gambarId: 0,
        tipeGambar: ''
      },
      {
        nomor: 120,
        gambarId: 0,
        tipeGambar: ''
      },
      {
        nomor: 121,
        gambarId: 0,
        tipeGambar: ''
      }
    ],
    [
      {
        nomor: 122,
        gambarId: 0,
        tipeGambar: ''
      },
      {
        nomor: 123,
        gambarId: 0,
        tipeGambar: ''
      },
      {
        nomor: 124,
        gambarId: 0,
        tipeGambar: ''
      },
      {
        nomor: 125,
        gambarId: 0,
        tipeGambar: ''
      },
      {
        nomor: 126,
        gambarId: 0,
        tipeGambar: ''
      }
    ],
    [
      {
        nomor: 127,
        gambarId: 0,
        tipeGambar: ''
      },
      {
        nomor: 128,
        gambarId: 0,
        tipeGambar: ''
      },
      {
        nomor: 129,
        gambarId: 0,
        tipeGambar: ''
      },
      {
        nomor: 130,
        gambarId: 0,
        tipeGambar: ''
      },
      {
        nomor: 131,
        gambarId: 0,
        tipeGambar: ''
      }
    ],
    [
      {
        nomor: 132,
        gambarId: 0,
        tipeGambar: ''
      },
      {
        nomor: 133,
        gambarId: 0,
        tipeGambar: ''
      },
      {
        nomor: 134,
        gambarId: 0,
        tipeGambar: ''
      },
      {
        nomor: 135,
        gambarId: 0,
        tipeGambar: ''
      },
      {
        nomor: 136,
        gambarId: 0,
        tipeGambar: ''
      }
    ]
  ]
};
