export default {
  durasi: null,
  soalName: '',
  keterangan: [
    {
      nomor: 1,
      nama: '',
      opsi: [
        {
          value: ''
        },
        {
          value: ''
        },
        {
          value: ''
        },
        {
          value: ''
        },
        {
          value: ''
        }
      ]
    },
    {
      nomor: 2,
      nama: '',
      opsi: [
        {
          value: ''
        },
        {
          value: ''
        },
        {
          value: ''
        },
        {
          value: ''
        },
        {
          value: ''
        }
      ]
    },
    {
      nomor: 3,
      nama: '',
      opsi: [
        {
          value: ''
        },
        {
          value: ''
        },
        {
          value: ''
        },
        {
          value: ''
        },
        {
          value: ''
        }
      ]
    },
    {
      nomor: 4,
      nama: '',
      opsi: [
        {
          value: ''
        },
        {
          value: ''
        },
        {
          value: ''
        },
        {
          value: ''
        },
        {
          value: ''
        }
      ]
    },
    {
      nomor: 5,
      nama: '',
      opsi: [
        {
          value: ''
        },
        {
          value: ''
        },
        {
          value: ''
        },
        {
          value: ''
        },
        {
          value: ''
        }
      ]
    }
  ],
  soal: [
    [
      {
        nomor: 157,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      },
      {
        nomor: 158,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      },
      {
        nomor: 159,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      },
      {
        nomor: 160,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      },
      {
        nomor: 161,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      }
    ],
    [
      {
        nomor: 162,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      },
      {
        nomor: 163,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      },
      {
        nomor: 164,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      },
      {
        nomor: 165,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      },
      {
        nomor: 166,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      }
    ],
    [
      {
        nomor: 167,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      },
      {
        nomor: 168,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      },
      {
        nomor: 169,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      },
      {
        nomor: 170,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      },
      {
        nomor: 171,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      }
    ],
    [
      {
        nomor: 172,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      },
      {
        nomor: 173,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      },
      {
        nomor: 174,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      },
      {
        nomor: 175,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      },
      {
        nomor: 176,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      }
    ]
  ]
};
