export default {
  durasi: '',
  soalName: '',
  soal: [
    [
      {
        nomor: 1,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      },
      {
        nomor: 2,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      },
      {
        nomor: 3,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      },
      {
        nomor: 4,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      },
      {
        nomor: 5,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      }
    ],
    [
      {
        nomor: 6,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      },
      {
        nomor: 7,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      },
      {
        nomor: 8,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      },
      {
        nomor: 9,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      },
      {
        nomor: 10,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      }
    ],
    [
      {
        nomor: 11,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      },
      {
        nomor: 12,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      },
      {
        nomor: 13,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      },
      {
        nomor: 14,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      },
      {
        nomor: 15,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      }
    ],
    [
      {
        nomor: 16,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      },
      {
        nomor: 17,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      },
      {
        nomor: 18,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      },
      {
        nomor: 19,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      },
      {
        nomor: 20,
        pertanyaan: '',
        opsi: [
          {
            notasi: 'a',
            description: ''
          },
          {
            notasi: 'b',
            description: ''
          },
          {
            notasi: 'c',
            description: ''
          },
          {
            notasi: 'd',
            description: ''
          },
          {
            notasi: 'e',
            description: ''
          }
        ],
        key: ''
      }
    ]
  ]
};
