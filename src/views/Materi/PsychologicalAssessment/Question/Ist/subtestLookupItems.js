import Subtest1 from '@/views/Materi/PsychologicalAssessment/Question/Ist/Lookup/Subtest1';
import Subtest2 from '@/views/Materi/PsychologicalAssessment/Question/Ist/Lookup/Subtest2';
import Subtest3 from '@/views/Materi/PsychologicalAssessment/Question/Ist/Lookup/Subtest3';
import Subtest4 from '@/views/Materi/PsychologicalAssessment/Question/Ist/Lookup/Subtest4';
import Subtest5 from '@/views/Materi/PsychologicalAssessment/Question/Ist/Lookup/Subtest5';
import Subtest6 from '@/views/Materi/PsychologicalAssessment/Question/Ist/Lookup/Subtest6';
import Subtest7 from '@/views/Materi/PsychologicalAssessment/Question/Ist/Lookup/Subtest7';
import Subtest8 from '@/views/Materi/PsychologicalAssessment/Question/Ist/Lookup/Subtest8';
import Subtest9 from '@/views/Materi/PsychologicalAssessment/Question/Ist/Lookup/Subtest9';

export const subtest1 = Subtest1;
export const subtest2 = Subtest2;
export const subtest3 = Subtest3;
export const subtest4 = Subtest4;
export const subtest5 = Subtest5;
export const subtest6 = Subtest6;
export const subtest7 = Subtest7;
export const subtest8 = Subtest8;
export const subtest9 = Subtest9;
