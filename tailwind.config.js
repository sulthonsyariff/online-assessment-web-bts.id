module.exports = {
  purge: [],
  theme: {
    gradients: () => ({
      blues: ['#2071ee', '#44c4f1'],
      'blues-secondary': ['to left', '#44c4f1', '#2071ee']
    }),
    fontFamily: {
      montserrat: ['Montserrat'],
      poppins: ['Poppins']
    },
    extend: {
      colors: {
        'app-primary': '#f1b102',
        'app-secondary': '#44c4f1',
        'app-alt': '#99daff',
        'app-base': '#ffffff',
        'status-pending': '#ffa800',
        'status-approved': '#13ce66',
        'list-background': '#f5faff',
        'label-color': '#57585a',
        'shades-blue': '#1f75ff',
        'shades-blue-light': '#b5e4ff',
        'shades-blue-lighter': '#daeafb',
        'shades-black': '#232323',
        'shades-grey': '#6f6f6f'
      },
      screens: {
        print: { raw: 'print' }
      }
    },
    textColor: (theme) => ({
      ...theme('colors')
    }),
    backgroundColor: (theme) => ({
      ...theme('colors')
    })
  },
  variants: {
    backgroundColor: ['odd', 'hover'],
    gradients: ['responsive', 'hover']
  },
  plugins: [require('tailwindcss-plugins/gradients')]
};
