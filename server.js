const express = require('express');
const http = require('http');
const path = require('path');
const app = express();

const port = 8081; //port

app.use(express.static(__dirname + '/dist'));

// Catch all other routes and return the index file
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist/index.html'));
});

const server = http.createServer(app);

server.listen(port, () =>
  console.log(`Vue Server running on localhost: ${port}`)
);
