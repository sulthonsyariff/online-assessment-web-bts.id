// const BundleAnalyzerPlugin = require("webpack-bundle-analyzer")
//   .BundleAnalyzerPlugin;

module.exports = {
  publicPath: process.env.VUE_APP_ENVIRONMENT === 'production' ? '/cms/' : '/',
  configureWebpack: {
    optimization: {
      splitChunks: {
        minSize: 10000,
        maxSize: 200000
      }
    },
    plugins: [
      //   new BundleAnalyzerPlugin({
      //     analyzerMode: 'disabled'
      //   })
    ]
  },
  transpileDependencies: ['vuex-persist']
};
